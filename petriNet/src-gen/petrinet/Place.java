/**
 */
package petrinet;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link petrinet.Place#getNbJetons <em>Nb Jetons</em>}</li>
 *   <li>{@link petrinet.Place#getArctp <em>Arctp</em>}</li>
 *   <li>{@link petrinet.Place#getArcpt <em>Arcpt</em>}</li>
 * </ul>
 *
 * @see petrinet.PetriNetPackage#getPlace()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='nbJetonsValide'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot nbJetonsValide='self.nbJetons &gt;= 0'"
 * @generated
 */
public interface Place extends ElementPetri {
	/**
	 * Returns the value of the '<em><b>Nb Jetons</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nb Jetons</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nb Jetons</em>' attribute.
	 * @see #setNbJetons(int)
	 * @see petrinet.PetriNetPackage#getPlace_NbJetons()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getNbJetons();

	/**
	 * Sets the value of the '{@link petrinet.Place#getNbJetons <em>Nb Jetons</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nb Jetons</em>' attribute.
	 * @see #getNbJetons()
	 * @generated
	 */
	void setNbJetons(int value);

	/**
	 * Returns the value of the '<em><b>Arctp</b></em>' reference list.
	 * The list contents are of type {@link petrinet.ArcTP}.
	 * It is bidirectional and its opposite is '{@link petrinet.ArcTP#getPlace <em>Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arctp</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arctp</em>' reference list.
	 * @see petrinet.PetriNetPackage#getPlace_Arctp()
	 * @see petrinet.ArcTP#getPlace
	 * @model opposite="place"
	 * @generated
	 */
	EList<ArcTP> getArctp();

	/**
	 * Returns the value of the '<em><b>Arcpt</b></em>' reference list.
	 * The list contents are of type {@link petrinet.ArcPT}.
	 * It is bidirectional and its opposite is '{@link petrinet.ArcPT#getPlace <em>Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arcpt</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arcpt</em>' reference list.
	 * @see petrinet.PetriNetPackage#getPlace_Arcpt()
	 * @see petrinet.ArcPT#getPlace
	 * @model opposite="place"
	 * @generated
	 */
	EList<ArcPT> getArcpt();

} // Place
