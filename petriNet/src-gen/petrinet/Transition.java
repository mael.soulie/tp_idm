/**
 */
package petrinet;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link petrinet.Transition#getArctp <em>Arctp</em>}</li>
 *   <li>{@link petrinet.Transition#getArcpt <em>Arcpt</em>}</li>
 * </ul>
 *
 * @see petrinet.PetriNetPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends ElementPetri {
	/**
	 * Returns the value of the '<em><b>Arctp</b></em>' reference list.
	 * The list contents are of type {@link petrinet.ArcTP}.
	 * It is bidirectional and its opposite is '{@link petrinet.ArcTP#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arctp</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arctp</em>' reference list.
	 * @see petrinet.PetriNetPackage#getTransition_Arctp()
	 * @see petrinet.ArcTP#getTransition
	 * @model opposite="transition"
	 * @generated
	 */
	EList<ArcTP> getArctp();

	/**
	 * Returns the value of the '<em><b>Arcpt</b></em>' reference list.
	 * The list contents are of type {@link petrinet.ArcPT}.
	 * It is bidirectional and its opposite is '{@link petrinet.ArcPT#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arcpt</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arcpt</em>' reference list.
	 * @see petrinet.PetriNetPackage#getTransition_Arcpt()
	 * @see petrinet.ArcPT#getTransition
	 * @model opposite="transition"
	 * @generated
	 */
	EList<ArcPT> getArcpt();

} // Transition
