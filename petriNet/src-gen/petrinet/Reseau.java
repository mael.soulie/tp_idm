/**
 */
package petrinet;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reseau</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link petrinet.Reseau#getArc <em>Arc</em>}</li>
 *   <li>{@link petrinet.Reseau#getElementpetri <em>Elementpetri</em>}</li>
 *   <li>{@link petrinet.Reseau#getNom <em>Nom</em>}</li>
 * </ul>
 *
 * @see petrinet.PetriNetPackage#getReseau()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='elementNomDifferent'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot elementNomDifferent='elementpetri-&gt;forAll(a1 : ElementPetri, a2 : ElementPetri|a1 &lt;&gt; a2 implies a1.nom &lt;&gt; a2.nom)'"
 * @generated
 */
public interface Reseau extends EObject {
	/**
	 * Returns the value of the '<em><b>Arc</b></em>' containment reference list.
	 * The list contents are of type {@link petrinet.Arc}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arc</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arc</em>' containment reference list.
	 * @see petrinet.PetriNetPackage#getReseau_Arc()
	 * @model containment="true"
	 * @generated
	 */
	EList<Arc> getArc();

	/**
	 * Returns the value of the '<em><b>Elementpetri</b></em>' containment reference list.
	 * The list contents are of type {@link petrinet.ElementPetri}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elementpetri</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elementpetri</em>' containment reference list.
	 * @see petrinet.PetriNetPackage#getReseau_Elementpetri()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ElementPetri> getElementpetri();

	/**
	 * Returns the value of the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nom</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nom</em>' attribute.
	 * @see #setNom(String)
	 * @see petrinet.PetriNetPackage#getReseau_Nom()
	 * @model required="true"
	 * @generated
	 */
	String getNom();

	/**
	 * Sets the value of the '{@link petrinet.Reseau#getNom <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nom</em>' attribute.
	 * @see #getNom()
	 * @generated
	 */
	void setNom(String value);

} // Reseau
