/**
 */
package petrinet.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import petrinet.ArcPT;
import petrinet.ArcTP;
import petrinet.PetriNetPackage;
import petrinet.Place;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link petrinet.impl.PlaceImpl#getNbJetons <em>Nb Jetons</em>}</li>
 *   <li>{@link petrinet.impl.PlaceImpl#getArctp <em>Arctp</em>}</li>
 *   <li>{@link petrinet.impl.PlaceImpl#getArcpt <em>Arcpt</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PlaceImpl extends ElementPetriImpl implements Place {
	/**
	 * The default value of the '{@link #getNbJetons() <em>Nb Jetons</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbJetons()
	 * @generated
	 * @ordered
	 */
	protected static final int NB_JETONS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNbJetons() <em>Nb Jetons</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbJetons()
	 * @generated
	 * @ordered
	 */
	protected int nbJetons = NB_JETONS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getArctp() <em>Arctp</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArctp()
	 * @generated
	 * @ordered
	 */
	protected EList<ArcTP> arctp;

	/**
	 * The cached value of the '{@link #getArcpt() <em>Arcpt</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArcpt()
	 * @generated
	 * @ordered
	 */
	protected EList<ArcPT> arcpt;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PetriNetPackage.Literals.PLACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNbJetons() {
		return nbJetons;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNbJetons(int newNbJetons) {
		int oldNbJetons = nbJetons;
		nbJetons = newNbJetons;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PetriNetPackage.PLACE__NB_JETONS, oldNbJetons, nbJetons));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ArcTP> getArctp() {
		if (arctp == null) {
			arctp = new EObjectWithInverseResolvingEList<ArcTP>(ArcTP.class, this, PetriNetPackage.PLACE__ARCTP, PetriNetPackage.ARC_TP__PLACE);
		}
		return arctp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ArcPT> getArcpt() {
		if (arcpt == null) {
			arcpt = new EObjectWithInverseResolvingEList<ArcPT>(ArcPT.class, this, PetriNetPackage.PLACE__ARCPT, PetriNetPackage.ARC_PT__PLACE);
		}
		return arcpt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PetriNetPackage.PLACE__ARCTP:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getArctp()).basicAdd(otherEnd, msgs);
			case PetriNetPackage.PLACE__ARCPT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getArcpt()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PetriNetPackage.PLACE__ARCTP:
				return ((InternalEList<?>)getArctp()).basicRemove(otherEnd, msgs);
			case PetriNetPackage.PLACE__ARCPT:
				return ((InternalEList<?>)getArcpt()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PetriNetPackage.PLACE__NB_JETONS:
				return getNbJetons();
			case PetriNetPackage.PLACE__ARCTP:
				return getArctp();
			case PetriNetPackage.PLACE__ARCPT:
				return getArcpt();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PetriNetPackage.PLACE__NB_JETONS:
				setNbJetons((Integer)newValue);
				return;
			case PetriNetPackage.PLACE__ARCTP:
				getArctp().clear();
				getArctp().addAll((Collection<? extends ArcTP>)newValue);
				return;
			case PetriNetPackage.PLACE__ARCPT:
				getArcpt().clear();
				getArcpt().addAll((Collection<? extends ArcPT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PetriNetPackage.PLACE__NB_JETONS:
				setNbJetons(NB_JETONS_EDEFAULT);
				return;
			case PetriNetPackage.PLACE__ARCTP:
				getArctp().clear();
				return;
			case PetriNetPackage.PLACE__ARCPT:
				getArcpt().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PetriNetPackage.PLACE__NB_JETONS:
				return nbJetons != NB_JETONS_EDEFAULT;
			case PetriNetPackage.PLACE__ARCTP:
				return arctp != null && !arctp.isEmpty();
			case PetriNetPackage.PLACE__ARCPT:
				return arcpt != null && !arcpt.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nbJetons: ");
		result.append(nbJetons);
		result.append(')');
		return result.toString();
	}

} //PlaceImpl
