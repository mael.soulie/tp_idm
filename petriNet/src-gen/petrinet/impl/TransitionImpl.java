/**
 */
package petrinet.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import petrinet.ArcPT;
import petrinet.ArcTP;
import petrinet.PetriNetPackage;
import petrinet.Transition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link petrinet.impl.TransitionImpl#getArctp <em>Arctp</em>}</li>
 *   <li>{@link petrinet.impl.TransitionImpl#getArcpt <em>Arcpt</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransitionImpl extends ElementPetriImpl implements Transition {
	/**
	 * The cached value of the '{@link #getArctp() <em>Arctp</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArctp()
	 * @generated
	 * @ordered
	 */
	protected EList<ArcTP> arctp;

	/**
	 * The cached value of the '{@link #getArcpt() <em>Arcpt</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArcpt()
	 * @generated
	 * @ordered
	 */
	protected EList<ArcPT> arcpt;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PetriNetPackage.Literals.TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ArcTP> getArctp() {
		if (arctp == null) {
			arctp = new EObjectWithInverseResolvingEList<ArcTP>(ArcTP.class, this, PetriNetPackage.TRANSITION__ARCTP, PetriNetPackage.ARC_TP__TRANSITION);
		}
		return arctp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ArcPT> getArcpt() {
		if (arcpt == null) {
			arcpt = new EObjectWithInverseResolvingEList<ArcPT>(ArcPT.class, this, PetriNetPackage.TRANSITION__ARCPT, PetriNetPackage.ARC_PT__TRANSITION);
		}
		return arcpt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__ARCTP:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getArctp()).basicAdd(otherEnd, msgs);
			case PetriNetPackage.TRANSITION__ARCPT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getArcpt()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__ARCTP:
				return ((InternalEList<?>)getArctp()).basicRemove(otherEnd, msgs);
			case PetriNetPackage.TRANSITION__ARCPT:
				return ((InternalEList<?>)getArcpt()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__ARCTP:
				return getArctp();
			case PetriNetPackage.TRANSITION__ARCPT:
				return getArcpt();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__ARCTP:
				getArctp().clear();
				getArctp().addAll((Collection<? extends ArcTP>)newValue);
				return;
			case PetriNetPackage.TRANSITION__ARCPT:
				getArcpt().clear();
				getArcpt().addAll((Collection<? extends ArcPT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__ARCTP:
				getArctp().clear();
				return;
			case PetriNetPackage.TRANSITION__ARCPT:
				getArcpt().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__ARCTP:
				return arctp != null && !arctp.isEmpty();
			case PetriNetPackage.TRANSITION__ARCPT:
				return arcpt != null && !arcpt.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TransitionImpl
