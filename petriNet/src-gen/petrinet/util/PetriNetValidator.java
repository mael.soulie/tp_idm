/**
 */
package petrinet.util;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import petrinet.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see petrinet.PetriNetPackage
 * @generated
 */
public class PetriNetValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final PetriNetValidator INSTANCE = new PetriNetValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "petrinet";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PetriNetValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return PetriNetPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case PetriNetPackage.RESEAU:
				return validateReseau((Reseau)value, diagnostics, context);
			case PetriNetPackage.ELEMENT_PETRI:
				return validateElementPetri((ElementPetri)value, diagnostics, context);
			case PetriNetPackage.ARC:
				return validateArc((Arc)value, diagnostics, context);
			case PetriNetPackage.TRANSITION:
				return validateTransition((Transition)value, diagnostics, context);
			case PetriNetPackage.PLACE:
				return validatePlace((Place)value, diagnostics, context);
			case PetriNetPackage.ARC_PT:
				return validateArcPT((ArcPT)value, diagnostics, context);
			case PetriNetPackage.ARC_TP:
				return validateArcTP((ArcTP)value, diagnostics, context);
			case PetriNetPackage.ARC_KIND:
				return validateArcKind((ArcKind)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReseau(Reseau reseau, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(reseau, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(reseau, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(reseau, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(reseau, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(reseau, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(reseau, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(reseau, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(reseau, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(reseau, diagnostics, context);
		if (result || diagnostics != null) result &= validateReseau_elementNomDifferent(reseau, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the elementNomDifferent constraint of '<em>Reseau</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String RESEAU__ELEMENT_NOM_DIFFERENT__EEXPRESSION = "elementpetri->forAll(a1 : ElementPetri, a2 : ElementPetri|a1 <> a2 implies a1.nom <> a2.nom)";

	/**
	 * Validates the elementNomDifferent constraint of '<em>Reseau</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReseau_elementNomDifferent(Reseau reseau, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(PetriNetPackage.Literals.RESEAU,
				 reseau,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "elementNomDifferent",
				 RESEAU__ELEMENT_NOM_DIFFERENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateElementPetri(ElementPetri elementPetri, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(elementPetri, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(elementPetri, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(elementPetri, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(elementPetri, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(elementPetri, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(elementPetri, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(elementPetri, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(elementPetri, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(elementPetri, diagnostics, context);
		if (result || diagnostics != null) result &= validateElementPetri_noEmptyName(elementPetri, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the noEmptyName constraint of '<em>Element Petri</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ELEMENT_PETRI__NO_EMPTY_NAME__EEXPRESSION = " (not self.nom.oclIsUndefined()) and self.nom.size() > 0";

	/**
	 * Validates the noEmptyName constraint of '<em>Element Petri</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateElementPetri_noEmptyName(ElementPetri elementPetri, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(PetriNetPackage.Literals.ELEMENT_PETRI,
				 elementPetri,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "noEmptyName",
				 ELEMENT_PETRI__NO_EMPTY_NAME__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateArc(Arc arc, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(arc, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(arc, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(arc, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(arc, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(arc, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(arc, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(arc, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(arc, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(arc, diagnostics, context);
		if (result || diagnostics != null) result &= validateArc_poidsValide(arc, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the poidsValide constraint of '<em>Arc</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ARC__POIDS_VALIDE__EEXPRESSION = "self.poids > 0";

	/**
	 * Validates the poidsValide constraint of '<em>Arc</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateArc_poidsValide(Arc arc, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(PetriNetPackage.Literals.ARC,
				 arc,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "poidsValide",
				 ARC__POIDS_VALIDE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransition(Transition transition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(transition, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validateElementPetri_noEmptyName(transition, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePlace(Place place, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(place, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(place, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(place, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(place, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(place, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(place, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(place, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(place, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(place, diagnostics, context);
		if (result || diagnostics != null) result &= validateElementPetri_noEmptyName(place, diagnostics, context);
		if (result || diagnostics != null) result &= validatePlace_nbJetonsValide(place, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the nbJetonsValide constraint of '<em>Place</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PLACE__NB_JETONS_VALIDE__EEXPRESSION = "self.nbJetons >= 0";

	/**
	 * Validates the nbJetonsValide constraint of '<em>Place</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePlace_nbJetonsValide(Place place, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(PetriNetPackage.Literals.PLACE,
				 place,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "nbJetonsValide",
				 PLACE__NB_JETONS_VALIDE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateArcPT(ArcPT arcPT, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(arcPT, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(arcPT, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(arcPT, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(arcPT, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(arcPT, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(arcPT, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(arcPT, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(arcPT, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(arcPT, diagnostics, context);
		if (result || diagnostics != null) result &= validateArc_poidsValide(arcPT, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateArcTP(ArcTP arcTP, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(arcTP, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(arcTP, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(arcTP, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(arcTP, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(arcTP, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(arcTP, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(arcTP, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(arcTP, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(arcTP, diagnostics, context);
		if (result || diagnostics != null) result &= validateArc_poidsValide(arcTP, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateArcKind(ArcKind arcKind, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //PetriNetValidator
