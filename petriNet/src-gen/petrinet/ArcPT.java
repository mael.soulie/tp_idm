/**
 */
package petrinet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arc PT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link petrinet.ArcPT#getTransition <em>Transition</em>}</li>
 *   <li>{@link petrinet.ArcPT#getPlace <em>Place</em>}</li>
 *   <li>{@link petrinet.ArcPT#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see petrinet.PetriNetPackage#getArcPT()
 * @model
 * @generated
 */
public interface ArcPT extends Arc {
	/**
	 * Returns the value of the '<em><b>Transition</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link petrinet.Transition#getArcpt <em>Arcpt</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' reference.
	 * @see #setTransition(Transition)
	 * @see petrinet.PetriNetPackage#getArcPT_Transition()
	 * @see petrinet.Transition#getArcpt
	 * @model opposite="arcpt" required="true"
	 * @generated
	 */
	Transition getTransition();

	/**
	 * Sets the value of the '{@link petrinet.ArcPT#getTransition <em>Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transition</em>' reference.
	 * @see #getTransition()
	 * @generated
	 */
	void setTransition(Transition value);

	/**
	 * Returns the value of the '<em><b>Place</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link petrinet.Place#getArcpt <em>Arcpt</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Place</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Place</em>' reference.
	 * @see #setPlace(Place)
	 * @see petrinet.PetriNetPackage#getArcPT_Place()
	 * @see petrinet.Place#getArcpt
	 * @model opposite="arcpt" required="true"
	 * @generated
	 */
	Place getPlace();

	/**
	 * Sets the value of the '{@link petrinet.ArcPT#getPlace <em>Place</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Place</em>' reference.
	 * @see #getPlace()
	 * @generated
	 */
	void setPlace(Place value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The default value is <code>"REGULAR"</code>.
	 * The literals are from the enumeration {@link petrinet.ArcKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see petrinet.ArcKind
	 * @see #setType(ArcKind)
	 * @see petrinet.PetriNetPackage#getArcPT_Type()
	 * @model default="REGULAR" required="true"
	 * @generated
	 */
	ArcKind getType();

	/**
	 * Sets the value of the '{@link petrinet.ArcPT#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see petrinet.ArcKind
	 * @see #getType()
	 * @generated
	 */
	void setType(ArcKind value);

} // ArcPT
