package simplepdl.manip;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import simplepdl.AllocationRessource;
import simplepdl.Process;
import simplepdl.Ressource;
import simplepdl.SimplepdlPackage;
import simplepdl.WorkDefinition;
import simplepdl.WorkSequence;
import petrinet.*;

public class Transformer {

	private Resource simplePDLModel;
	private PetriNetFactory petriNetFactory;
	private Process process;
	private Reseau reseau;
	private Map<WorkDefinition, Map<String, ElementPetri>> elementsPetriTable;
	
	public Transformer(Resource simplePDLModel) {
		this.simplePDLModel = simplePDLModel;
		
		this.elementsPetriTable = new HashMap<WorkDefinition, Map<String, ElementPetri>>();

		// La fabrique pour fabriquer les éléments de SimplePDL
		this.petriNetFactory = PetriNetFactory.eINSTANCE;
		
		// Charge le process SimplePDL
		this.process = (Process) this.simplePDLModel.getContents().get(0);

		// Créer un élément Process
		this.reseau = petriNetFactory.createReseau();
		reseau.setNom(process.getName());
	}
	
	private void transformWorkDefinition(WorkDefinition wd) {
		
		Place p1 = this.petriNetFactory.createPlace();
		p1.setNom(wd.getName() + "_not_started");
		p1.setNbJetons(1);
		
		Place p2 = this.petriNetFactory.createPlace();
		p2.setNom(wd.getName() + "_running");
		p2.setNbJetons(0);

		Place p3 = this.petriNetFactory.createPlace();
		p3.setNom(wd.getName() + "_finished");
		p3.setNbJetons(0);

		Transition t1 = this.petriNetFactory.createTransition();
		t1.setNom(wd.getName() + "_start");
		
		Transition t2 = this.petriNetFactory.createTransition();
		t2.setNom(wd.getName() + "_finish");
		
		ArcPT arcPT1 = this.petriNetFactory.createArcPT();
		arcPT1.setPlace(p1);
		arcPT1.setTransition(t1);
		arcPT1.setPoids(1);
		
		ArcPT arcPT2 = this.petriNetFactory.createArcPT();
		arcPT2.setPlace(p2);
		arcPT2.setTransition(t2);
		arcPT2.setPoids(1);
		
		ArcTP arcTP1 = this.petriNetFactory.createArcTP();
		arcTP1.setPlace(p2);
		arcTP1.setTransition(t1);
		arcTP1.setPoids(1);

		ArcTP arcTP2 = this.petriNetFactory.createArcTP();
		arcTP2.setPlace(p3);
		arcTP2.setTransition(t2);
		arcTP2.setPoids(1);
		
		
		Map<String, ElementPetri> activity = new HashMap<String, ElementPetri>(); 
		activity.put(p1.getNom(), p1);
		activity.put(p2.getNom(), p2);
		activity.put(p3.getNom(), p3);
		activity.put(t1.getNom(), t1);
		activity.put(t2.getNom(), t2);
		
		this.elementsPetriTable.put(wd, activity);
		this.reseau.getArc().add(arcPT1);
		this.reseau.getArc().add(arcPT2);
		this.reseau.getArc().add(arcTP1);
		this.reseau.getArc().add(arcTP2);

	}
	
	public Reseau transform() {

		
		EList<WorkDefinition> workDefinitions = this.process.getProcessElement().stream().filter(WorkDefinition.class::isInstance)
				.map(WorkDefinition.class::cast)
				.collect(Collectors.toCollection(BasicEList::new));

		for (WorkDefinition wd : workDefinitions) {
			
			this.transformWorkDefinition(wd);
			
			for (Map.Entry<String, ElementPetri> entry : this.elementsPetriTable.get(wd).entrySet())
			{
				this.reseau.getElementpetri().add(entry.getValue());
			}

		}
		
		
		
		EList<WorkSequence> workSequences =  this.process.getProcessElement().stream().filter(WorkSequence.class::isInstance)
				.map(WorkSequence.class::cast)
				.collect(Collectors.toCollection(BasicEList::new));
		
		
		for (WorkSequence ws : workSequences) {
			
				switch (ws.getLinkType()) {
					case FINISH_TO_FINISH:
						this.transformf2f(ws.getPredecessor(), ws.getSuccessor());
						break;
					case FINISH_TO_START:
						this.transformf2s(ws.getPredecessor(), ws.getSuccessor());
						break;
					case START_TO_FINISH:
						this.transforms2f(ws.getPredecessor(), ws.getSuccessor());
						break;
					case START_TO_START:
						this.transforms2s(ws.getPredecessor(), ws.getSuccessor());
						break;
					default:
						break;
				};
		}
		
		
		// Transformation des ressources
		
		for (WorkDefinition wd : workDefinitions) {
			EList<AllocationRessource> al = wd.getAllocationressource();
			for (AllocationRessource a : al) {
				this.transformRessource(a.getRessource());
				this.transformAllocation(wd, a);
			}
			
		}
		
		
		
		return this.reseau;
	}
	
	private Place findPlace(String name) {
		
		return this.reseau.getElementpetri().stream()
				.filter(Place.class::isInstance).map(Place.class::cast)
				.filter(p -> p.getNom().equals(name))
				.findFirst().orElse(null);
		
	}
	
	private Transition findTransition(String name) {
		
		return this.reseau.getElementpetri().stream()
				.filter(Transition.class::isInstance).map(Transition.class::cast)
				.filter(p -> p.getNom().equals(name))
				.findFirst().orElse(null);
		
	}
	
	/**
	 * Méthode qui transforme une worksequence f2f en modèle de petri
	 * @param a l'activité a
	 * @param b l'activité b
	 */
	private void transformf2f(WorkDefinition a, WorkDefinition b) {
		Place p1 = this.findPlace(a.getName() + "_has_started");

		if (p1 == null) {
			p1 = this.petriNetFactory.createPlace();
			p1.setNom(a.getName() + "_has_started");
			p1.setNbJetons(0);
		}
		
		Place p2 = this.findPlace(b.getName() + "_has_started");

		if (p2 == null) {
			p2 = this.petriNetFactory.createPlace();
			p2.setNom(b.getName() + "_has_started");
			p2.setNbJetons(0);
		}
		
		ElementPetri bStart = this.elementsPetriTable.get(b).get(b.getName() + "_start");
		ElementPetri aStart = this.elementsPetriTable.get(a).get(a.getName() + "_start");

		ArcTP arcTP1 = this.petriNetFactory.createArcTP();
		arcTP1.setPlace(p1);
		arcTP1.setTransition((Transition)aStart);
		arcTP1.setPoids(1);
		
		ArcTP arcTP2 = this.petriNetFactory.createArcTP();
		arcTP2.setPlace(p2);
		arcTP2.setTransition((Transition)bStart);
		arcTP2.setPoids(1);
		
		this.reseau.getElementpetri().add(p1);
		this.reseau.getElementpetri().add(p2);
		this.reseau.getArc().add(arcTP1);
		this.reseau.getArc().add(arcTP2);
		
	}
	
	private void transforms2f(WorkDefinition a, WorkDefinition b) {
		Place p1 = this.findPlace(a.getName() + "_p0");
		
		if (p1 == null) {
			p1 = this.petriNetFactory.createPlace();
			p1.setNom(a.getName() + "_p0");
			p1.setNbJetons(0);
		}
		
		ElementPetri aStart = this.elementsPetriTable.get(a).get(a.getName() + "_start");
		ElementPetri bFinish = this.elementsPetriTable.get(b).get(b.getName() + "_finish");

		ArcTP arcTP1 = this.petriNetFactory.createArcTP();
		arcTP1.setPlace(p1);
		arcTP1.setTransition((Transition)aStart);
		arcTP1.setPoids(1);

		ArcPT arcPT1 = this.petriNetFactory.createArcPT();
		// read arc
		arcPT1.setPlace(p1);
		arcPT1.setType(ArcKind.READ_ARC);
		arcPT1.setTransition((Transition)bFinish);
		arcPT1.setPoids(1);
		
		this.reseau.getArc().add(arcTP1);
		this.reseau.getArc().add(arcPT1);
		this.reseau.getElementpetri().add(p1);

	}

	private void transforms2s(WorkDefinition a, WorkDefinition b) {
		Place p1 = this.findPlace(a.getName() + "_has_started");

		if (p1 == null) {
			p1 = this.petriNetFactory.createPlace();
			p1.setNom(a.getName() + "_has_started");
			p1.setNbJetons(0);
		}
				
		ElementPetri aStart = this.elementsPetriTable.get(a).get(a.getName() + "_start");
		ElementPetri bStart = this.elementsPetriTable.get(b).get(b.getName() + "_start");

		ArcTP arcTP1 = this.petriNetFactory.createArcTP();
		arcTP1.setPlace(p1);
		arcTP1.setTransition((Transition)aStart);
		arcTP1.setPoids(1);
		
		
		ArcPT arcPT1 = this.petriNetFactory.createArcPT();
		// read arc
		arcPT1.setType(ArcKind.READ_ARC);
		arcPT1.setPlace(p1);
		arcPT1.setTransition((Transition)bStart);
		arcPT1.setPoids(1);
		
		this.reseau.getArc().add(arcTP1);
		this.reseau.getArc().add(arcPT1);
		this.reseau.getElementpetri().add(p1);

	}

	private void transformf2s(WorkDefinition a, WorkDefinition b) {
		ElementPetri aFinished = this.elementsPetriTable.get(a).get(a.getName() + "_finished");
		ElementPetri bStart = this.elementsPetriTable.get(b).get(b.getName() + "_start");

		ArcPT arcPT1 = this.petriNetFactory.createArcPT();
		// read arc
		arcPT1.setType(ArcKind.READ_ARC);
		arcPT1.setPlace((Place)aFinished);
		arcPT1.setTransition((Transition)bStart);
		arcPT1.setPoids(1);
		
		this.reseau.getArc().add(arcPT1);
		
	}
	

   private void transformRessource(Ressource rs) {
        
        /* Création de la place associée à la Ressource */
		Place p_ressource = this.findPlace(rs.getName() + "_ressource");

		if (p_ressource == null) {
	        // La place pour le nombre de ressources disponibles
	        p_ressource = petriNetFactory.createPlace();
	        p_ressource.setNom(rs.getName() + "_ressource");
	        p_ressource.setNbJetons(rs.getInstance());
	        reseau.getElementpetri().add(p_ressource);
		}
    }
    
    private void transformAllocation(WorkDefinition wd, AllocationRessource al) {
    	
		Place p_ressource = this.findPlace(al.getRessource().getName() + "_ressource");
        
		if (p_ressource != null) {
	        Transition t_start = this.findTransition(wd.getName() + "_start");
	        Transition t_finish = this.findTransition(wd.getName() + "_finish");
	        
	        /* Création des arcs associées à l'Allocation */
	    	
	    	// Ressource -> Start
	    	ArcPT a_allocate = petriNetFactory.createArcPT();
	    	a_allocate.setPlace(p_ressource);
	    	a_allocate.setTransition(t_start);
	    	a_allocate.setPoids(al.getNumAllocated());
	        reseau.getArc().add(a_allocate);
	    	
	    	// Finish -> Ressource
	    	ArcTP a_unallocate = petriNetFactory.createArcTP();
	    	a_unallocate.setTransition(t_finish);
	    	a_unallocate.setPlace(p_ressource);
	    	a_unallocate.setPoids(al.getNumAllocated());
	        reseau.getArc().add(a_unallocate);
	        
		}
        
        
    }
	

    
    public static void main(String[] args) {
        // Créer le modèle en sortie
    	System.out.println("=== PDL2PETRI TRANSFORMATION ===");
       
    	String pdlFileName = "SimplePDLCreator_Created_Process";
    	
		// Charger le package SimplePDL afin de l'enregistrer dans le registre d'Eclipse.
		SimplepdlPackage packageInstance = SimplepdlPackage.eINSTANCE;

		// Enregistrer l'extension ".xmi" comme devant être ouverte à
		// l'aide d'un objet "XMIResourceFactoryImpl"
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("xmi", new XMIResourceFactoryImpl());

        // Créer un objet resourceSetImpl qui contiendra le modèle
        ResourceSet resSet = new ResourceSetImpl();

        // Charger le modèle en entrée
        URI modelURI = URI.createURI("models/" + pdlFileName + ".xmi");

        Resource resource = resSet.getResource(modelURI, true);

        // On applique la transformation
        Transformer t = new Transformer(resource);
        Reseau reseau = t.transform();

        // Créer le modèle en sortie
        URI pnURI = URI.createURI("models/" + pdlFileName + "_Petri.xmi");
        Resource pn = resSet.createResource(pnURI);
        
        // Remplir le modèle en sortie
        pn.getContents().add(reseau);

        try {
            pn.save(Collections.EMPTY_MAP);
            System.out.println("Transformation Ok!");

        } catch(Exception e) {
            e.printStackTrace();
        }
    }
	
	
}
