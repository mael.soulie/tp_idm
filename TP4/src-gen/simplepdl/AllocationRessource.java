/**
 */
package simplepdl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Allocation Ressource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link simplepdl.AllocationRessource#getRessource <em>Ressource</em>}</li>
 *   <li>{@link simplepdl.AllocationRessource#getNumAllocated <em>Num Allocated</em>}</li>
 *   <li>{@link simplepdl.AllocationRessource#getWorkdefinition <em>Workdefinition</em>}</li>
 * </ul>
 *
 * @see simplepdl.SimplepdlPackage#getAllocationRessource()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='numAllocatedPositive numAllocatedLessThanRessourceInstance'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot numAllocatedPositive='self.numAllocated &gt; 0' numAllocatedLessThanRessourceInstance='self.numAllocated &lt;= self.ressource.instance'"
 * @generated
 */
public interface AllocationRessource extends EObject {
	/**
	 * Returns the value of the '<em><b>Ressource</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link simplepdl.Ressource#getPredecessors <em>Predecessors</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ressource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ressource</em>' reference.
	 * @see #setRessource(Ressource)
	 * @see simplepdl.SimplepdlPackage#getAllocationRessource_Ressource()
	 * @see simplepdl.Ressource#getPredecessors
	 * @model opposite="predecessors" required="true"
	 * @generated
	 */
	Ressource getRessource();

	/**
	 * Sets the value of the '{@link simplepdl.AllocationRessource#getRessource <em>Ressource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ressource</em>' reference.
	 * @see #getRessource()
	 * @generated
	 */
	void setRessource(Ressource value);

	/**
	 * Returns the value of the '<em><b>Num Allocated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num Allocated</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num Allocated</em>' attribute.
	 * @see #setNumAllocated(int)
	 * @see simplepdl.SimplepdlPackage#getAllocationRessource_NumAllocated()
	 * @model required="true"
	 * @generated
	 */
	int getNumAllocated();

	/**
	 * Sets the value of the '{@link simplepdl.AllocationRessource#getNumAllocated <em>Num Allocated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num Allocated</em>' attribute.
	 * @see #getNumAllocated()
	 * @generated
	 */
	void setNumAllocated(int value);

	/**
	 * Returns the value of the '<em><b>Workdefinition</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link simplepdl.WorkDefinition#getAllocationressource <em>Allocationressource</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Workdefinition</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Workdefinition</em>' container reference.
	 * @see #setWorkdefinition(WorkDefinition)
	 * @see simplepdl.SimplepdlPackage#getAllocationRessource_Workdefinition()
	 * @see simplepdl.WorkDefinition#getAllocationressource
	 * @model opposite="allocationressource" required="true" transient="false"
	 * @generated
	 */
	WorkDefinition getWorkdefinition();

	/**
	 * Sets the value of the '{@link simplepdl.AllocationRessource#getWorkdefinition <em>Workdefinition</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Workdefinition</em>' container reference.
	 * @see #getWorkdefinition()
	 * @generated
	 */
	void setWorkdefinition(WorkDefinition value);

} // AllocationRessource
