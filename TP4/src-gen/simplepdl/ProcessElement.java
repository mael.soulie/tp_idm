/**
 */
package simplepdl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link simplepdl.ProcessElement#getProcess <em>Process</em>}</li>
 * </ul>
 *
 * @see simplepdl.SimplepdlPackage#getProcessElement()
 * @model abstract="true"
 * @generated
 */
public interface ProcessElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Process</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * TP3 Ex 1.3. qu 2  Une activité ne peut pas être réflexive :
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Process</em>' reference.
	 * @see simplepdl.SimplepdlPackage#getProcessElement_Process()
	 * @model resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='Process.allInstances()\n\t\t\t\t-&gt;select(p | p.processElement-&gt;includes(self))\n\t\t\t\t-&gt;asSequence()-&gt;first()'"
	 * @generated
	 */
	simplepdl.Process getProcess();

} // ProcessElement
