package fr.n7.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.n7.services.PDL3GrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPDL3Parser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Process'", "'{'", "'processElement'", "','", "'}'", "'WorkDefinition'", "'linksToPredecessors'", "'('", "')'", "'linksToSuccessors'", "'allocationressource'", "'WorkSequence'", "'linkType'", "'predecessor'", "'successor'", "'Guidance'", "'text'", "'elements'", "'Ressource'", "'instance'", "'predecessors'", "'AllocationRessource'", "'numAllocated'", "'ressource'", "'-'", "'startToStart'", "'finishToStart'", "'startToFinish'", "'finishToFinish'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPDL3Parser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPDL3Parser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPDL3Parser.tokenNames; }
    public String getGrammarFileName() { return "InternalPDL3.g"; }



     	private PDL3GrammarAccess grammarAccess;

        public InternalPDL3Parser(TokenStream input, PDL3GrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Process";
       	}

       	@Override
       	protected PDL3GrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleProcess"
    // InternalPDL3.g:65:1: entryRuleProcess returns [EObject current=null] : iv_ruleProcess= ruleProcess EOF ;
    public final EObject entryRuleProcess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcess = null;


        try {
            // InternalPDL3.g:65:48: (iv_ruleProcess= ruleProcess EOF )
            // InternalPDL3.g:66:2: iv_ruleProcess= ruleProcess EOF
            {
             newCompositeNode(grammarAccess.getProcessRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProcess=ruleProcess();

            state._fsp--;

             current =iv_ruleProcess; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcess"


    // $ANTLR start "ruleProcess"
    // InternalPDL3.g:72:1: ruleProcess returns [EObject current=null] : ( () otherlv_1= 'Process' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'processElement' otherlv_5= '{' ( (lv_processElement_6_0= ruleProcessElement ) ) (otherlv_7= ',' ( (lv_processElement_8_0= ruleProcessElement ) ) )* otherlv_9= '}' )? otherlv_10= '}' ) ;
    public final EObject ruleProcess() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_processElement_6_0 = null;

        EObject lv_processElement_8_0 = null;



        	enterRule();

        try {
            // InternalPDL3.g:78:2: ( ( () otherlv_1= 'Process' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'processElement' otherlv_5= '{' ( (lv_processElement_6_0= ruleProcessElement ) ) (otherlv_7= ',' ( (lv_processElement_8_0= ruleProcessElement ) ) )* otherlv_9= '}' )? otherlv_10= '}' ) )
            // InternalPDL3.g:79:2: ( () otherlv_1= 'Process' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'processElement' otherlv_5= '{' ( (lv_processElement_6_0= ruleProcessElement ) ) (otherlv_7= ',' ( (lv_processElement_8_0= ruleProcessElement ) ) )* otherlv_9= '}' )? otherlv_10= '}' )
            {
            // InternalPDL3.g:79:2: ( () otherlv_1= 'Process' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'processElement' otherlv_5= '{' ( (lv_processElement_6_0= ruleProcessElement ) ) (otherlv_7= ',' ( (lv_processElement_8_0= ruleProcessElement ) ) )* otherlv_9= '}' )? otherlv_10= '}' )
            // InternalPDL3.g:80:3: () otherlv_1= 'Process' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'processElement' otherlv_5= '{' ( (lv_processElement_6_0= ruleProcessElement ) ) (otherlv_7= ',' ( (lv_processElement_8_0= ruleProcessElement ) ) )* otherlv_9= '}' )? otherlv_10= '}'
            {
            // InternalPDL3.g:80:3: ()
            // InternalPDL3.g:81:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getProcessAccess().getProcessAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getProcessAccess().getProcessKeyword_1());
            		
            // InternalPDL3.g:91:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPDL3.g:92:4: (lv_name_2_0= ruleEString )
            {
            // InternalPDL3.g:92:4: (lv_name_2_0= ruleEString )
            // InternalPDL3.g:93:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getProcessAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getProcessRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.n7.PDL3.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getProcessAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPDL3.g:114:3: (otherlv_4= 'processElement' otherlv_5= '{' ( (lv_processElement_6_0= ruleProcessElement ) ) (otherlv_7= ',' ( (lv_processElement_8_0= ruleProcessElement ) ) )* otherlv_9= '}' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalPDL3.g:115:4: otherlv_4= 'processElement' otherlv_5= '{' ( (lv_processElement_6_0= ruleProcessElement ) ) (otherlv_7= ',' ( (lv_processElement_8_0= ruleProcessElement ) ) )* otherlv_9= '}'
                    {
                    otherlv_4=(Token)match(input,13,FOLLOW_4); 

                    				newLeafNode(otherlv_4, grammarAccess.getProcessAccess().getProcessElementKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,12,FOLLOW_6); 

                    				newLeafNode(otherlv_5, grammarAccess.getProcessAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalPDL3.g:123:4: ( (lv_processElement_6_0= ruleProcessElement ) )
                    // InternalPDL3.g:124:5: (lv_processElement_6_0= ruleProcessElement )
                    {
                    // InternalPDL3.g:124:5: (lv_processElement_6_0= ruleProcessElement )
                    // InternalPDL3.g:125:6: lv_processElement_6_0= ruleProcessElement
                    {

                    						newCompositeNode(grammarAccess.getProcessAccess().getProcessElementProcessElementParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_7);
                    lv_processElement_6_0=ruleProcessElement();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getProcessRule());
                    						}
                    						add(
                    							current,
                    							"processElement",
                    							lv_processElement_6_0,
                    							"fr.n7.PDL3.ProcessElement");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDL3.g:142:4: (otherlv_7= ',' ( (lv_processElement_8_0= ruleProcessElement ) ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==14) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalPDL3.g:143:5: otherlv_7= ',' ( (lv_processElement_8_0= ruleProcessElement ) )
                    	    {
                    	    otherlv_7=(Token)match(input,14,FOLLOW_6); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getProcessAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalPDL3.g:147:5: ( (lv_processElement_8_0= ruleProcessElement ) )
                    	    // InternalPDL3.g:148:6: (lv_processElement_8_0= ruleProcessElement )
                    	    {
                    	    // InternalPDL3.g:148:6: (lv_processElement_8_0= ruleProcessElement )
                    	    // InternalPDL3.g:149:7: lv_processElement_8_0= ruleProcessElement
                    	    {

                    	    							newCompositeNode(grammarAccess.getProcessAccess().getProcessElementProcessElementParserRuleCall_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_7);
                    	    lv_processElement_8_0=ruleProcessElement();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getProcessRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"processElement",
                    	    								lv_processElement_8_0,
                    	    								"fr.n7.PDL3.ProcessElement");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,15,FOLLOW_8); 

                    				newLeafNode(otherlv_9, grammarAccess.getProcessAccess().getRightCurlyBracketKeyword_4_4());
                    			

                    }
                    break;

            }

            otherlv_10=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getProcessAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcess"


    // $ANTLR start "entryRuleProcessElement"
    // InternalPDL3.g:180:1: entryRuleProcessElement returns [EObject current=null] : iv_ruleProcessElement= ruleProcessElement EOF ;
    public final EObject entryRuleProcessElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcessElement = null;


        try {
            // InternalPDL3.g:180:55: (iv_ruleProcessElement= ruleProcessElement EOF )
            // InternalPDL3.g:181:2: iv_ruleProcessElement= ruleProcessElement EOF
            {
             newCompositeNode(grammarAccess.getProcessElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProcessElement=ruleProcessElement();

            state._fsp--;

             current =iv_ruleProcessElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcessElement"


    // $ANTLR start "ruleProcessElement"
    // InternalPDL3.g:187:1: ruleProcessElement returns [EObject current=null] : (this_WorkDefinition_0= ruleWorkDefinition | this_WorkSequence_1= ruleWorkSequence | this_Guidance_2= ruleGuidance | this_Ressource_3= ruleRessource ) ;
    public final EObject ruleProcessElement() throws RecognitionException {
        EObject current = null;

        EObject this_WorkDefinition_0 = null;

        EObject this_WorkSequence_1 = null;

        EObject this_Guidance_2 = null;

        EObject this_Ressource_3 = null;



        	enterRule();

        try {
            // InternalPDL3.g:193:2: ( (this_WorkDefinition_0= ruleWorkDefinition | this_WorkSequence_1= ruleWorkSequence | this_Guidance_2= ruleGuidance | this_Ressource_3= ruleRessource ) )
            // InternalPDL3.g:194:2: (this_WorkDefinition_0= ruleWorkDefinition | this_WorkSequence_1= ruleWorkSequence | this_Guidance_2= ruleGuidance | this_Ressource_3= ruleRessource )
            {
            // InternalPDL3.g:194:2: (this_WorkDefinition_0= ruleWorkDefinition | this_WorkSequence_1= ruleWorkSequence | this_Guidance_2= ruleGuidance | this_Ressource_3= ruleRessource )
            int alt3=4;
            switch ( input.LA(1) ) {
            case 16:
                {
                alt3=1;
                }
                break;
            case 22:
                {
                alt3=2;
                }
                break;
            case 26:
                {
                alt3=3;
                }
                break;
            case 29:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalPDL3.g:195:3: this_WorkDefinition_0= ruleWorkDefinition
                    {

                    			newCompositeNode(grammarAccess.getProcessElementAccess().getWorkDefinitionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_WorkDefinition_0=ruleWorkDefinition();

                    state._fsp--;


                    			current = this_WorkDefinition_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPDL3.g:204:3: this_WorkSequence_1= ruleWorkSequence
                    {

                    			newCompositeNode(grammarAccess.getProcessElementAccess().getWorkSequenceParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_WorkSequence_1=ruleWorkSequence();

                    state._fsp--;


                    			current = this_WorkSequence_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalPDL3.g:213:3: this_Guidance_2= ruleGuidance
                    {

                    			newCompositeNode(grammarAccess.getProcessElementAccess().getGuidanceParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Guidance_2=ruleGuidance();

                    state._fsp--;


                    			current = this_Guidance_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalPDL3.g:222:3: this_Ressource_3= ruleRessource
                    {

                    			newCompositeNode(grammarAccess.getProcessElementAccess().getRessourceParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Ressource_3=ruleRessource();

                    state._fsp--;


                    			current = this_Ressource_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcessElement"


    // $ANTLR start "entryRuleEString"
    // InternalPDL3.g:234:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalPDL3.g:234:47: (iv_ruleEString= ruleEString EOF )
            // InternalPDL3.g:235:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalPDL3.g:241:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalPDL3.g:247:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalPDL3.g:248:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalPDL3.g:248:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_STRING) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_ID) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalPDL3.g:249:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalPDL3.g:257:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleWorkDefinition"
    // InternalPDL3.g:268:1: entryRuleWorkDefinition returns [EObject current=null] : iv_ruleWorkDefinition= ruleWorkDefinition EOF ;
    public final EObject entryRuleWorkDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWorkDefinition = null;


        try {
            // InternalPDL3.g:268:55: (iv_ruleWorkDefinition= ruleWorkDefinition EOF )
            // InternalPDL3.g:269:2: iv_ruleWorkDefinition= ruleWorkDefinition EOF
            {
             newCompositeNode(grammarAccess.getWorkDefinitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWorkDefinition=ruleWorkDefinition();

            state._fsp--;

             current =iv_ruleWorkDefinition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWorkDefinition"


    // $ANTLR start "ruleWorkDefinition"
    // InternalPDL3.g:275:1: ruleWorkDefinition returns [EObject current=null] : ( () otherlv_1= 'WorkDefinition' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'linksToPredecessors' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'linksToSuccessors' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )? (otherlv_16= 'allocationressource' otherlv_17= '{' ( (lv_allocationressource_18_0= ruleAllocationRessource ) ) (otherlv_19= ',' ( (lv_allocationressource_20_0= ruleAllocationRessource ) ) )* otherlv_21= '}' )? otherlv_22= '}' ) ;
    public final EObject ruleWorkDefinition() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_allocationressource_18_0 = null;

        EObject lv_allocationressource_20_0 = null;



        	enterRule();

        try {
            // InternalPDL3.g:281:2: ( ( () otherlv_1= 'WorkDefinition' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'linksToPredecessors' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'linksToSuccessors' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )? (otherlv_16= 'allocationressource' otherlv_17= '{' ( (lv_allocationressource_18_0= ruleAllocationRessource ) ) (otherlv_19= ',' ( (lv_allocationressource_20_0= ruleAllocationRessource ) ) )* otherlv_21= '}' )? otherlv_22= '}' ) )
            // InternalPDL3.g:282:2: ( () otherlv_1= 'WorkDefinition' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'linksToPredecessors' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'linksToSuccessors' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )? (otherlv_16= 'allocationressource' otherlv_17= '{' ( (lv_allocationressource_18_0= ruleAllocationRessource ) ) (otherlv_19= ',' ( (lv_allocationressource_20_0= ruleAllocationRessource ) ) )* otherlv_21= '}' )? otherlv_22= '}' )
            {
            // InternalPDL3.g:282:2: ( () otherlv_1= 'WorkDefinition' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'linksToPredecessors' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'linksToSuccessors' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )? (otherlv_16= 'allocationressource' otherlv_17= '{' ( (lv_allocationressource_18_0= ruleAllocationRessource ) ) (otherlv_19= ',' ( (lv_allocationressource_20_0= ruleAllocationRessource ) ) )* otherlv_21= '}' )? otherlv_22= '}' )
            // InternalPDL3.g:283:3: () otherlv_1= 'WorkDefinition' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'linksToPredecessors' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'linksToSuccessors' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )? (otherlv_16= 'allocationressource' otherlv_17= '{' ( (lv_allocationressource_18_0= ruleAllocationRessource ) ) (otherlv_19= ',' ( (lv_allocationressource_20_0= ruleAllocationRessource ) ) )* otherlv_21= '}' )? otherlv_22= '}'
            {
            // InternalPDL3.g:283:3: ()
            // InternalPDL3.g:284:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getWorkDefinitionAccess().getWorkDefinitionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,16,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getWorkDefinitionAccess().getWorkDefinitionKeyword_1());
            		
            // InternalPDL3.g:294:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPDL3.g:295:4: (lv_name_2_0= ruleEString )
            {
            // InternalPDL3.g:295:4: (lv_name_2_0= ruleEString )
            // InternalPDL3.g:296:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getWorkDefinitionAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkDefinitionRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.n7.PDL3.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_9); 

            			newLeafNode(otherlv_3, grammarAccess.getWorkDefinitionAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPDL3.g:317:3: (otherlv_4= 'linksToPredecessors' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==17) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalPDL3.g:318:4: otherlv_4= 'linksToPredecessors' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')'
                    {
                    otherlv_4=(Token)match(input,17,FOLLOW_10); 

                    				newLeafNode(otherlv_4, grammarAccess.getWorkDefinitionAccess().getLinksToPredecessorsKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,18,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getWorkDefinitionAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalPDL3.g:326:4: ( ( ruleEString ) )
                    // InternalPDL3.g:327:5: ( ruleEString )
                    {
                    // InternalPDL3.g:327:5: ( ruleEString )
                    // InternalPDL3.g:328:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getWorkDefinitionRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getWorkDefinitionAccess().getLinksToPredecessorsWorkSequenceCrossReference_4_2_0());
                    					
                    pushFollow(FOLLOW_11);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDL3.g:342:4: (otherlv_7= ',' ( ( ruleEString ) ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==14) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalPDL3.g:343:5: otherlv_7= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_7=(Token)match(input,14,FOLLOW_3); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getWorkDefinitionAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalPDL3.g:347:5: ( ( ruleEString ) )
                    	    // InternalPDL3.g:348:6: ( ruleEString )
                    	    {
                    	    // InternalPDL3.g:348:6: ( ruleEString )
                    	    // InternalPDL3.g:349:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getWorkDefinitionRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getWorkDefinitionAccess().getLinksToPredecessorsWorkSequenceCrossReference_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_11);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,19,FOLLOW_12); 

                    				newLeafNode(otherlv_9, grammarAccess.getWorkDefinitionAccess().getRightParenthesisKeyword_4_4());
                    			

                    }
                    break;

            }

            // InternalPDL3.g:369:3: (otherlv_10= 'linksToSuccessors' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==20) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalPDL3.g:370:4: otherlv_10= 'linksToSuccessors' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')'
                    {
                    otherlv_10=(Token)match(input,20,FOLLOW_10); 

                    				newLeafNode(otherlv_10, grammarAccess.getWorkDefinitionAccess().getLinksToSuccessorsKeyword_5_0());
                    			
                    otherlv_11=(Token)match(input,18,FOLLOW_3); 

                    				newLeafNode(otherlv_11, grammarAccess.getWorkDefinitionAccess().getLeftParenthesisKeyword_5_1());
                    			
                    // InternalPDL3.g:378:4: ( ( ruleEString ) )
                    // InternalPDL3.g:379:5: ( ruleEString )
                    {
                    // InternalPDL3.g:379:5: ( ruleEString )
                    // InternalPDL3.g:380:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getWorkDefinitionRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getWorkDefinitionAccess().getLinksToSuccessorsWorkSequenceCrossReference_5_2_0());
                    					
                    pushFollow(FOLLOW_11);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDL3.g:394:4: (otherlv_13= ',' ( ( ruleEString ) ) )*
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==14) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // InternalPDL3.g:395:5: otherlv_13= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_13=(Token)match(input,14,FOLLOW_3); 

                    	    					newLeafNode(otherlv_13, grammarAccess.getWorkDefinitionAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalPDL3.g:399:5: ( ( ruleEString ) )
                    	    // InternalPDL3.g:400:6: ( ruleEString )
                    	    {
                    	    // InternalPDL3.g:400:6: ( ruleEString )
                    	    // InternalPDL3.g:401:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getWorkDefinitionRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getWorkDefinitionAccess().getLinksToSuccessorsWorkSequenceCrossReference_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_11);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,19,FOLLOW_13); 

                    				newLeafNode(otherlv_15, grammarAccess.getWorkDefinitionAccess().getRightParenthesisKeyword_5_4());
                    			

                    }
                    break;

            }

            // InternalPDL3.g:421:3: (otherlv_16= 'allocationressource' otherlv_17= '{' ( (lv_allocationressource_18_0= ruleAllocationRessource ) ) (otherlv_19= ',' ( (lv_allocationressource_20_0= ruleAllocationRessource ) ) )* otherlv_21= '}' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==21) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalPDL3.g:422:4: otherlv_16= 'allocationressource' otherlv_17= '{' ( (lv_allocationressource_18_0= ruleAllocationRessource ) ) (otherlv_19= ',' ( (lv_allocationressource_20_0= ruleAllocationRessource ) ) )* otherlv_21= '}'
                    {
                    otherlv_16=(Token)match(input,21,FOLLOW_4); 

                    				newLeafNode(otherlv_16, grammarAccess.getWorkDefinitionAccess().getAllocationressourceKeyword_6_0());
                    			
                    otherlv_17=(Token)match(input,12,FOLLOW_14); 

                    				newLeafNode(otherlv_17, grammarAccess.getWorkDefinitionAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalPDL3.g:430:4: ( (lv_allocationressource_18_0= ruleAllocationRessource ) )
                    // InternalPDL3.g:431:5: (lv_allocationressource_18_0= ruleAllocationRessource )
                    {
                    // InternalPDL3.g:431:5: (lv_allocationressource_18_0= ruleAllocationRessource )
                    // InternalPDL3.g:432:6: lv_allocationressource_18_0= ruleAllocationRessource
                    {

                    						newCompositeNode(grammarAccess.getWorkDefinitionAccess().getAllocationressourceAllocationRessourceParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_7);
                    lv_allocationressource_18_0=ruleAllocationRessource();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkDefinitionRule());
                    						}
                    						add(
                    							current,
                    							"allocationressource",
                    							lv_allocationressource_18_0,
                    							"fr.n7.PDL3.AllocationRessource");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDL3.g:449:4: (otherlv_19= ',' ( (lv_allocationressource_20_0= ruleAllocationRessource ) ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==14) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalPDL3.g:450:5: otherlv_19= ',' ( (lv_allocationressource_20_0= ruleAllocationRessource ) )
                    	    {
                    	    otherlv_19=(Token)match(input,14,FOLLOW_14); 

                    	    					newLeafNode(otherlv_19, grammarAccess.getWorkDefinitionAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalPDL3.g:454:5: ( (lv_allocationressource_20_0= ruleAllocationRessource ) )
                    	    // InternalPDL3.g:455:6: (lv_allocationressource_20_0= ruleAllocationRessource )
                    	    {
                    	    // InternalPDL3.g:455:6: (lv_allocationressource_20_0= ruleAllocationRessource )
                    	    // InternalPDL3.g:456:7: lv_allocationressource_20_0= ruleAllocationRessource
                    	    {

                    	    							newCompositeNode(grammarAccess.getWorkDefinitionAccess().getAllocationressourceAllocationRessourceParserRuleCall_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_7);
                    	    lv_allocationressource_20_0=ruleAllocationRessource();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getWorkDefinitionRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"allocationressource",
                    	    								lv_allocationressource_20_0,
                    	    								"fr.n7.PDL3.AllocationRessource");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    otherlv_21=(Token)match(input,15,FOLLOW_8); 

                    				newLeafNode(otherlv_21, grammarAccess.getWorkDefinitionAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            otherlv_22=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_22, grammarAccess.getWorkDefinitionAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWorkDefinition"


    // $ANTLR start "entryRuleWorkSequence"
    // InternalPDL3.g:487:1: entryRuleWorkSequence returns [EObject current=null] : iv_ruleWorkSequence= ruleWorkSequence EOF ;
    public final EObject entryRuleWorkSequence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWorkSequence = null;


        try {
            // InternalPDL3.g:487:53: (iv_ruleWorkSequence= ruleWorkSequence EOF )
            // InternalPDL3.g:488:2: iv_ruleWorkSequence= ruleWorkSequence EOF
            {
             newCompositeNode(grammarAccess.getWorkSequenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWorkSequence=ruleWorkSequence();

            state._fsp--;

             current =iv_ruleWorkSequence; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWorkSequence"


    // $ANTLR start "ruleWorkSequence"
    // InternalPDL3.g:494:1: ruleWorkSequence returns [EObject current=null] : (otherlv_0= 'WorkSequence' otherlv_1= '{' otherlv_2= 'linkType' ( (lv_linkType_3_0= ruleWorkSequenceType ) ) otherlv_4= 'predecessor' ( ( ruleEString ) ) otherlv_6= 'successor' ( ( ruleEString ) ) otherlv_8= '}' ) ;
    public final EObject ruleWorkSequence() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Enumerator lv_linkType_3_0 = null;



        	enterRule();

        try {
            // InternalPDL3.g:500:2: ( (otherlv_0= 'WorkSequence' otherlv_1= '{' otherlv_2= 'linkType' ( (lv_linkType_3_0= ruleWorkSequenceType ) ) otherlv_4= 'predecessor' ( ( ruleEString ) ) otherlv_6= 'successor' ( ( ruleEString ) ) otherlv_8= '}' ) )
            // InternalPDL3.g:501:2: (otherlv_0= 'WorkSequence' otherlv_1= '{' otherlv_2= 'linkType' ( (lv_linkType_3_0= ruleWorkSequenceType ) ) otherlv_4= 'predecessor' ( ( ruleEString ) ) otherlv_6= 'successor' ( ( ruleEString ) ) otherlv_8= '}' )
            {
            // InternalPDL3.g:501:2: (otherlv_0= 'WorkSequence' otherlv_1= '{' otherlv_2= 'linkType' ( (lv_linkType_3_0= ruleWorkSequenceType ) ) otherlv_4= 'predecessor' ( ( ruleEString ) ) otherlv_6= 'successor' ( ( ruleEString ) ) otherlv_8= '}' )
            // InternalPDL3.g:502:3: otherlv_0= 'WorkSequence' otherlv_1= '{' otherlv_2= 'linkType' ( (lv_linkType_3_0= ruleWorkSequenceType ) ) otherlv_4= 'predecessor' ( ( ruleEString ) ) otherlv_6= 'successor' ( ( ruleEString ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getWorkSequenceAccess().getWorkSequenceKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_15); 

            			newLeafNode(otherlv_1, grammarAccess.getWorkSequenceAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,23,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getWorkSequenceAccess().getLinkTypeKeyword_2());
            		
            // InternalPDL3.g:514:3: ( (lv_linkType_3_0= ruleWorkSequenceType ) )
            // InternalPDL3.g:515:4: (lv_linkType_3_0= ruleWorkSequenceType )
            {
            // InternalPDL3.g:515:4: (lv_linkType_3_0= ruleWorkSequenceType )
            // InternalPDL3.g:516:5: lv_linkType_3_0= ruleWorkSequenceType
            {

            					newCompositeNode(grammarAccess.getWorkSequenceAccess().getLinkTypeWorkSequenceTypeEnumRuleCall_3_0());
            				
            pushFollow(FOLLOW_17);
            lv_linkType_3_0=ruleWorkSequenceType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkSequenceRule());
            					}
            					set(
            						current,
            						"linkType",
            						lv_linkType_3_0,
            						"fr.n7.PDL3.WorkSequenceType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,24,FOLLOW_3); 

            			newLeafNode(otherlv_4, grammarAccess.getWorkSequenceAccess().getPredecessorKeyword_4());
            		
            // InternalPDL3.g:537:3: ( ( ruleEString ) )
            // InternalPDL3.g:538:4: ( ruleEString )
            {
            // InternalPDL3.g:538:4: ( ruleEString )
            // InternalPDL3.g:539:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWorkSequenceRule());
            					}
            				

            					newCompositeNode(grammarAccess.getWorkSequenceAccess().getPredecessorWorkDefinitionCrossReference_5_0());
            				
            pushFollow(FOLLOW_18);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,25,FOLLOW_3); 

            			newLeafNode(otherlv_6, grammarAccess.getWorkSequenceAccess().getSuccessorKeyword_6());
            		
            // InternalPDL3.g:557:3: ( ( ruleEString ) )
            // InternalPDL3.g:558:4: ( ruleEString )
            {
            // InternalPDL3.g:558:4: ( ruleEString )
            // InternalPDL3.g:559:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWorkSequenceRule());
            					}
            				

            					newCompositeNode(grammarAccess.getWorkSequenceAccess().getSuccessorWorkDefinitionCrossReference_7_0());
            				
            pushFollow(FOLLOW_8);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getWorkSequenceAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWorkSequence"


    // $ANTLR start "entryRuleGuidance"
    // InternalPDL3.g:581:1: entryRuleGuidance returns [EObject current=null] : iv_ruleGuidance= ruleGuidance EOF ;
    public final EObject entryRuleGuidance() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuidance = null;


        try {
            // InternalPDL3.g:581:49: (iv_ruleGuidance= ruleGuidance EOF )
            // InternalPDL3.g:582:2: iv_ruleGuidance= ruleGuidance EOF
            {
             newCompositeNode(grammarAccess.getGuidanceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGuidance=ruleGuidance();

            state._fsp--;

             current =iv_ruleGuidance; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuidance"


    // $ANTLR start "ruleGuidance"
    // InternalPDL3.g:588:1: ruleGuidance returns [EObject current=null] : ( () otherlv_1= 'Guidance' otherlv_2= '{' (otherlv_3= 'text' ( (lv_text_4_0= ruleEString ) ) )? (otherlv_5= 'elements' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' ) ;
    public final EObject ruleGuidance() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_text_4_0 = null;



        	enterRule();

        try {
            // InternalPDL3.g:594:2: ( ( () otherlv_1= 'Guidance' otherlv_2= '{' (otherlv_3= 'text' ( (lv_text_4_0= ruleEString ) ) )? (otherlv_5= 'elements' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' ) )
            // InternalPDL3.g:595:2: ( () otherlv_1= 'Guidance' otherlv_2= '{' (otherlv_3= 'text' ( (lv_text_4_0= ruleEString ) ) )? (otherlv_5= 'elements' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' )
            {
            // InternalPDL3.g:595:2: ( () otherlv_1= 'Guidance' otherlv_2= '{' (otherlv_3= 'text' ( (lv_text_4_0= ruleEString ) ) )? (otherlv_5= 'elements' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' )
            // InternalPDL3.g:596:3: () otherlv_1= 'Guidance' otherlv_2= '{' (otherlv_3= 'text' ( (lv_text_4_0= ruleEString ) ) )? (otherlv_5= 'elements' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}'
            {
            // InternalPDL3.g:596:3: ()
            // InternalPDL3.g:597:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGuidanceAccess().getGuidanceAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,26,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getGuidanceAccess().getGuidanceKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_19); 

            			newLeafNode(otherlv_2, grammarAccess.getGuidanceAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPDL3.g:611:3: (otherlv_3= 'text' ( (lv_text_4_0= ruleEString ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==27) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalPDL3.g:612:4: otherlv_3= 'text' ( (lv_text_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,27,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getGuidanceAccess().getTextKeyword_3_0());
                    			
                    // InternalPDL3.g:616:4: ( (lv_text_4_0= ruleEString ) )
                    // InternalPDL3.g:617:5: (lv_text_4_0= ruleEString )
                    {
                    // InternalPDL3.g:617:5: (lv_text_4_0= ruleEString )
                    // InternalPDL3.g:618:6: lv_text_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getGuidanceAccess().getTextEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_20);
                    lv_text_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGuidanceRule());
                    						}
                    						set(
                    							current,
                    							"text",
                    							lv_text_4_0,
                    							"fr.n7.PDL3.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPDL3.g:636:3: (otherlv_5= 'elements' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==28) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalPDL3.g:637:4: otherlv_5= 'elements' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')'
                    {
                    otherlv_5=(Token)match(input,28,FOLLOW_10); 

                    				newLeafNode(otherlv_5, grammarAccess.getGuidanceAccess().getElementsKeyword_4_0());
                    			
                    otherlv_6=(Token)match(input,18,FOLLOW_3); 

                    				newLeafNode(otherlv_6, grammarAccess.getGuidanceAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalPDL3.g:645:4: ( ( ruleEString ) )
                    // InternalPDL3.g:646:5: ( ruleEString )
                    {
                    // InternalPDL3.g:646:5: ( ruleEString )
                    // InternalPDL3.g:647:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getGuidanceRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getGuidanceAccess().getElementsProcessElementCrossReference_4_2_0());
                    					
                    pushFollow(FOLLOW_11);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDL3.g:661:4: (otherlv_8= ',' ( ( ruleEString ) ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==14) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalPDL3.g:662:5: otherlv_8= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_8=(Token)match(input,14,FOLLOW_3); 

                    	    					newLeafNode(otherlv_8, grammarAccess.getGuidanceAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalPDL3.g:666:5: ( ( ruleEString ) )
                    	    // InternalPDL3.g:667:6: ( ruleEString )
                    	    {
                    	    // InternalPDL3.g:667:6: ( ruleEString )
                    	    // InternalPDL3.g:668:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getGuidanceRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getGuidanceAccess().getElementsProcessElementCrossReference_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_11);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    otherlv_10=(Token)match(input,19,FOLLOW_8); 

                    				newLeafNode(otherlv_10, grammarAccess.getGuidanceAccess().getRightParenthesisKeyword_4_4());
                    			

                    }
                    break;

            }

            otherlv_11=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getGuidanceAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuidance"


    // $ANTLR start "entryRuleRessource"
    // InternalPDL3.g:696:1: entryRuleRessource returns [EObject current=null] : iv_ruleRessource= ruleRessource EOF ;
    public final EObject entryRuleRessource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRessource = null;


        try {
            // InternalPDL3.g:696:50: (iv_ruleRessource= ruleRessource EOF )
            // InternalPDL3.g:697:2: iv_ruleRessource= ruleRessource EOF
            {
             newCompositeNode(grammarAccess.getRessourceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRessource=ruleRessource();

            state._fsp--;

             current =iv_ruleRessource; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRessource"


    // $ANTLR start "ruleRessource"
    // InternalPDL3.g:703:1: ruleRessource returns [EObject current=null] : (otherlv_0= 'Ressource' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'instance' ( (lv_instance_4_0= ruleEInt ) ) (otherlv_5= 'predecessors' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' ) ;
    public final EObject ruleRessource() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_instance_4_0 = null;



        	enterRule();

        try {
            // InternalPDL3.g:709:2: ( (otherlv_0= 'Ressource' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'instance' ( (lv_instance_4_0= ruleEInt ) ) (otherlv_5= 'predecessors' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' ) )
            // InternalPDL3.g:710:2: (otherlv_0= 'Ressource' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'instance' ( (lv_instance_4_0= ruleEInt ) ) (otherlv_5= 'predecessors' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' )
            {
            // InternalPDL3.g:710:2: (otherlv_0= 'Ressource' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'instance' ( (lv_instance_4_0= ruleEInt ) ) (otherlv_5= 'predecessors' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}' )
            // InternalPDL3.g:711:3: otherlv_0= 'Ressource' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'instance' ( (lv_instance_4_0= ruleEInt ) ) (otherlv_5= 'predecessors' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )? otherlv_11= '}'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getRessourceAccess().getRessourceKeyword_0());
            		
            // InternalPDL3.g:715:3: ( (lv_name_1_0= ruleEString ) )
            // InternalPDL3.g:716:4: (lv_name_1_0= ruleEString )
            {
            // InternalPDL3.g:716:4: (lv_name_1_0= ruleEString )
            // InternalPDL3.g:717:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getRessourceAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRessourceRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.n7.PDL3.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_21); 

            			newLeafNode(otherlv_2, grammarAccess.getRessourceAccess().getLeftCurlyBracketKeyword_2());
            		
            otherlv_3=(Token)match(input,30,FOLLOW_22); 

            			newLeafNode(otherlv_3, grammarAccess.getRessourceAccess().getInstanceKeyword_3());
            		
            // InternalPDL3.g:742:3: ( (lv_instance_4_0= ruleEInt ) )
            // InternalPDL3.g:743:4: (lv_instance_4_0= ruleEInt )
            {
            // InternalPDL3.g:743:4: (lv_instance_4_0= ruleEInt )
            // InternalPDL3.g:744:5: lv_instance_4_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getRessourceAccess().getInstanceEIntParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_23);
            lv_instance_4_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRessourceRule());
            					}
            					set(
            						current,
            						"instance",
            						lv_instance_4_0,
            						"fr.n7.PDL3.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPDL3.g:761:3: (otherlv_5= 'predecessors' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')' )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==31) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalPDL3.g:762:4: otherlv_5= 'predecessors' otherlv_6= '(' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* otherlv_10= ')'
                    {
                    otherlv_5=(Token)match(input,31,FOLLOW_10); 

                    				newLeafNode(otherlv_5, grammarAccess.getRessourceAccess().getPredecessorsKeyword_5_0());
                    			
                    otherlv_6=(Token)match(input,18,FOLLOW_3); 

                    				newLeafNode(otherlv_6, grammarAccess.getRessourceAccess().getLeftParenthesisKeyword_5_1());
                    			
                    // InternalPDL3.g:770:4: ( ( ruleEString ) )
                    // InternalPDL3.g:771:5: ( ruleEString )
                    {
                    // InternalPDL3.g:771:5: ( ruleEString )
                    // InternalPDL3.g:772:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRessourceRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getRessourceAccess().getPredecessorsAllocationRessourceCrossReference_5_2_0());
                    					
                    pushFollow(FOLLOW_11);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPDL3.g:786:4: (otherlv_8= ',' ( ( ruleEString ) ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==14) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalPDL3.g:787:5: otherlv_8= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_8=(Token)match(input,14,FOLLOW_3); 

                    	    					newLeafNode(otherlv_8, grammarAccess.getRessourceAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalPDL3.g:791:5: ( ( ruleEString ) )
                    	    // InternalPDL3.g:792:6: ( ruleEString )
                    	    {
                    	    // InternalPDL3.g:792:6: ( ruleEString )
                    	    // InternalPDL3.g:793:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getRessourceRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getRessourceAccess().getPredecessorsAllocationRessourceCrossReference_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_11);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);

                    otherlv_10=(Token)match(input,19,FOLLOW_8); 

                    				newLeafNode(otherlv_10, grammarAccess.getRessourceAccess().getRightParenthesisKeyword_5_4());
                    			

                    }
                    break;

            }

            otherlv_11=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getRessourceAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRessource"


    // $ANTLR start "entryRuleAllocationRessource"
    // InternalPDL3.g:821:1: entryRuleAllocationRessource returns [EObject current=null] : iv_ruleAllocationRessource= ruleAllocationRessource EOF ;
    public final EObject entryRuleAllocationRessource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAllocationRessource = null;


        try {
            // InternalPDL3.g:821:60: (iv_ruleAllocationRessource= ruleAllocationRessource EOF )
            // InternalPDL3.g:822:2: iv_ruleAllocationRessource= ruleAllocationRessource EOF
            {
             newCompositeNode(grammarAccess.getAllocationRessourceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAllocationRessource=ruleAllocationRessource();

            state._fsp--;

             current =iv_ruleAllocationRessource; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAllocationRessource"


    // $ANTLR start "ruleAllocationRessource"
    // InternalPDL3.g:828:1: ruleAllocationRessource returns [EObject current=null] : (otherlv_0= 'AllocationRessource' otherlv_1= '{' otherlv_2= 'numAllocated' ( (lv_numAllocated_3_0= ruleEInt ) ) otherlv_4= 'ressource' ( ( ruleEString ) ) otherlv_6= '}' ) ;
    public final EObject ruleAllocationRessource() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_numAllocated_3_0 = null;



        	enterRule();

        try {
            // InternalPDL3.g:834:2: ( (otherlv_0= 'AllocationRessource' otherlv_1= '{' otherlv_2= 'numAllocated' ( (lv_numAllocated_3_0= ruleEInt ) ) otherlv_4= 'ressource' ( ( ruleEString ) ) otherlv_6= '}' ) )
            // InternalPDL3.g:835:2: (otherlv_0= 'AllocationRessource' otherlv_1= '{' otherlv_2= 'numAllocated' ( (lv_numAllocated_3_0= ruleEInt ) ) otherlv_4= 'ressource' ( ( ruleEString ) ) otherlv_6= '}' )
            {
            // InternalPDL3.g:835:2: (otherlv_0= 'AllocationRessource' otherlv_1= '{' otherlv_2= 'numAllocated' ( (lv_numAllocated_3_0= ruleEInt ) ) otherlv_4= 'ressource' ( ( ruleEString ) ) otherlv_6= '}' )
            // InternalPDL3.g:836:3: otherlv_0= 'AllocationRessource' otherlv_1= '{' otherlv_2= 'numAllocated' ( (lv_numAllocated_3_0= ruleEInt ) ) otherlv_4= 'ressource' ( ( ruleEString ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,32,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getAllocationRessourceAccess().getAllocationRessourceKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_24); 

            			newLeafNode(otherlv_1, grammarAccess.getAllocationRessourceAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,33,FOLLOW_22); 

            			newLeafNode(otherlv_2, grammarAccess.getAllocationRessourceAccess().getNumAllocatedKeyword_2());
            		
            // InternalPDL3.g:848:3: ( (lv_numAllocated_3_0= ruleEInt ) )
            // InternalPDL3.g:849:4: (lv_numAllocated_3_0= ruleEInt )
            {
            // InternalPDL3.g:849:4: (lv_numAllocated_3_0= ruleEInt )
            // InternalPDL3.g:850:5: lv_numAllocated_3_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getAllocationRessourceAccess().getNumAllocatedEIntParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_25);
            lv_numAllocated_3_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAllocationRessourceRule());
            					}
            					set(
            						current,
            						"numAllocated",
            						lv_numAllocated_3_0,
            						"fr.n7.PDL3.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,34,FOLLOW_3); 

            			newLeafNode(otherlv_4, grammarAccess.getAllocationRessourceAccess().getRessourceKeyword_4());
            		
            // InternalPDL3.g:871:3: ( ( ruleEString ) )
            // InternalPDL3.g:872:4: ( ruleEString )
            {
            // InternalPDL3.g:872:4: ( ruleEString )
            // InternalPDL3.g:873:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAllocationRessourceRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAllocationRessourceAccess().getRessourceRessourceCrossReference_5_0());
            				
            pushFollow(FOLLOW_8);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getAllocationRessourceAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAllocationRessource"


    // $ANTLR start "entryRuleEInt"
    // InternalPDL3.g:895:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalPDL3.g:895:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalPDL3.g:896:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalPDL3.g:902:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalPDL3.g:908:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalPDL3.g:909:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalPDL3.g:909:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalPDL3.g:910:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalPDL3.g:910:3: (kw= '-' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==35) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalPDL3.g:911:4: kw= '-'
                    {
                    kw=(Token)match(input,35,FOLLOW_26); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEIntAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getEIntAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "ruleWorkSequenceType"
    // InternalPDL3.g:928:1: ruleWorkSequenceType returns [Enumerator current=null] : ( (enumLiteral_0= 'startToStart' ) | (enumLiteral_1= 'finishToStart' ) | (enumLiteral_2= 'startToFinish' ) | (enumLiteral_3= 'finishToFinish' ) ) ;
    public final Enumerator ruleWorkSequenceType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalPDL3.g:934:2: ( ( (enumLiteral_0= 'startToStart' ) | (enumLiteral_1= 'finishToStart' ) | (enumLiteral_2= 'startToFinish' ) | (enumLiteral_3= 'finishToFinish' ) ) )
            // InternalPDL3.g:935:2: ( (enumLiteral_0= 'startToStart' ) | (enumLiteral_1= 'finishToStart' ) | (enumLiteral_2= 'startToFinish' ) | (enumLiteral_3= 'finishToFinish' ) )
            {
            // InternalPDL3.g:935:2: ( (enumLiteral_0= 'startToStart' ) | (enumLiteral_1= 'finishToStart' ) | (enumLiteral_2= 'startToFinish' ) | (enumLiteral_3= 'finishToFinish' ) )
            int alt17=4;
            switch ( input.LA(1) ) {
            case 36:
                {
                alt17=1;
                }
                break;
            case 37:
                {
                alt17=2;
                }
                break;
            case 38:
                {
                alt17=3;
                }
                break;
            case 39:
                {
                alt17=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // InternalPDL3.g:936:3: (enumLiteral_0= 'startToStart' )
                    {
                    // InternalPDL3.g:936:3: (enumLiteral_0= 'startToStart' )
                    // InternalPDL3.g:937:4: enumLiteral_0= 'startToStart'
                    {
                    enumLiteral_0=(Token)match(input,36,FOLLOW_2); 

                    				current = grammarAccess.getWorkSequenceTypeAccess().getStartToStartEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getWorkSequenceTypeAccess().getStartToStartEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalPDL3.g:944:3: (enumLiteral_1= 'finishToStart' )
                    {
                    // InternalPDL3.g:944:3: (enumLiteral_1= 'finishToStart' )
                    // InternalPDL3.g:945:4: enumLiteral_1= 'finishToStart'
                    {
                    enumLiteral_1=(Token)match(input,37,FOLLOW_2); 

                    				current = grammarAccess.getWorkSequenceTypeAccess().getFinishToStartEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getWorkSequenceTypeAccess().getFinishToStartEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalPDL3.g:952:3: (enumLiteral_2= 'startToFinish' )
                    {
                    // InternalPDL3.g:952:3: (enumLiteral_2= 'startToFinish' )
                    // InternalPDL3.g:953:4: enumLiteral_2= 'startToFinish'
                    {
                    enumLiteral_2=(Token)match(input,38,FOLLOW_2); 

                    				current = grammarAccess.getWorkSequenceTypeAccess().getStartToFinishEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getWorkSequenceTypeAccess().getStartToFinishEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalPDL3.g:960:3: (enumLiteral_3= 'finishToFinish' )
                    {
                    // InternalPDL3.g:960:3: (enumLiteral_3= 'finishToFinish' )
                    // InternalPDL3.g:961:4: enumLiteral_3= 'finishToFinish'
                    {
                    enumLiteral_3=(Token)match(input,39,FOLLOW_2); 

                    				current = grammarAccess.getWorkSequenceTypeAccess().getFinishToFinishEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getWorkSequenceTypeAccess().getFinishToFinishEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWorkSequenceType"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000000A000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000024410000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000000000C000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000328000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000084000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000308000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000208000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000F000000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000018008000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000010008000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000800000040L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000080008000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000040L});

}