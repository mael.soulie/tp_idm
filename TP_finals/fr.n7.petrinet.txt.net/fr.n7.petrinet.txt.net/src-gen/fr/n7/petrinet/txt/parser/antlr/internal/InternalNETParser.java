package fr.n7.petrinet.txt.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.n7.petrinet.txt.services.NETGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalNETParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Reseau'", "'{'", "'nom'", "'arc'", "','", "'}'", "'elementpetri'", "'-'", "'ArcPT'", "'poids'", "'type'", "'transition'", "'place'", "'ArcTP'", "'Transition'", "'arctp'", "'('", "')'", "'arcpt'", "'Place'", "'nbJetons'", "'READ_ARC'", "'REGULAR'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalNETParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalNETParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalNETParser.tokenNames; }
    public String getGrammarFileName() { return "InternalNET.g"; }



     	private NETGrammarAccess grammarAccess;

        public InternalNETParser(TokenStream input, NETGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Reseau";
       	}

       	@Override
       	protected NETGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleReseau"
    // InternalNET.g:65:1: entryRuleReseau returns [EObject current=null] : iv_ruleReseau= ruleReseau EOF ;
    public final EObject entryRuleReseau() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReseau = null;


        try {
            // InternalNET.g:65:47: (iv_ruleReseau= ruleReseau EOF )
            // InternalNET.g:66:2: iv_ruleReseau= ruleReseau EOF
            {
             newCompositeNode(grammarAccess.getReseauRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReseau=ruleReseau();

            state._fsp--;

             current =iv_ruleReseau; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReseau"


    // $ANTLR start "ruleReseau"
    // InternalNET.g:72:1: ruleReseau returns [EObject current=null] : (otherlv_0= 'Reseau' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) (otherlv_4= 'arc' otherlv_5= '{' ( (lv_arc_6_0= ruleArc ) ) (otherlv_7= ',' ( (lv_arc_8_0= ruleArc ) ) )* otherlv_9= '}' )? otherlv_10= 'elementpetri' otherlv_11= '{' ( (lv_elementpetri_12_0= ruleElementPetri ) ) (otherlv_13= ',' ( (lv_elementpetri_14_0= ruleElementPetri ) ) )* otherlv_15= '}' otherlv_16= '}' ) ;
    public final EObject ruleReseau() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        AntlrDatatypeRuleToken lv_nom_3_0 = null;

        EObject lv_arc_6_0 = null;

        EObject lv_arc_8_0 = null;

        EObject lv_elementpetri_12_0 = null;

        EObject lv_elementpetri_14_0 = null;



        	enterRule();

        try {
            // InternalNET.g:78:2: ( (otherlv_0= 'Reseau' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) (otherlv_4= 'arc' otherlv_5= '{' ( (lv_arc_6_0= ruleArc ) ) (otherlv_7= ',' ( (lv_arc_8_0= ruleArc ) ) )* otherlv_9= '}' )? otherlv_10= 'elementpetri' otherlv_11= '{' ( (lv_elementpetri_12_0= ruleElementPetri ) ) (otherlv_13= ',' ( (lv_elementpetri_14_0= ruleElementPetri ) ) )* otherlv_15= '}' otherlv_16= '}' ) )
            // InternalNET.g:79:2: (otherlv_0= 'Reseau' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) (otherlv_4= 'arc' otherlv_5= '{' ( (lv_arc_6_0= ruleArc ) ) (otherlv_7= ',' ( (lv_arc_8_0= ruleArc ) ) )* otherlv_9= '}' )? otherlv_10= 'elementpetri' otherlv_11= '{' ( (lv_elementpetri_12_0= ruleElementPetri ) ) (otherlv_13= ',' ( (lv_elementpetri_14_0= ruleElementPetri ) ) )* otherlv_15= '}' otherlv_16= '}' )
            {
            // InternalNET.g:79:2: (otherlv_0= 'Reseau' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) (otherlv_4= 'arc' otherlv_5= '{' ( (lv_arc_6_0= ruleArc ) ) (otherlv_7= ',' ( (lv_arc_8_0= ruleArc ) ) )* otherlv_9= '}' )? otherlv_10= 'elementpetri' otherlv_11= '{' ( (lv_elementpetri_12_0= ruleElementPetri ) ) (otherlv_13= ',' ( (lv_elementpetri_14_0= ruleElementPetri ) ) )* otherlv_15= '}' otherlv_16= '}' )
            // InternalNET.g:80:3: otherlv_0= 'Reseau' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) (otherlv_4= 'arc' otherlv_5= '{' ( (lv_arc_6_0= ruleArc ) ) (otherlv_7= ',' ( (lv_arc_8_0= ruleArc ) ) )* otherlv_9= '}' )? otherlv_10= 'elementpetri' otherlv_11= '{' ( (lv_elementpetri_12_0= ruleElementPetri ) ) (otherlv_13= ',' ( (lv_elementpetri_14_0= ruleElementPetri ) ) )* otherlv_15= '}' otherlv_16= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getReseauAccess().getReseauKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getReseauAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getReseauAccess().getNomKeyword_2());
            		
            // InternalNET.g:92:3: ( (lv_nom_3_0= ruleEString ) )
            // InternalNET.g:93:4: (lv_nom_3_0= ruleEString )
            {
            // InternalNET.g:93:4: (lv_nom_3_0= ruleEString )
            // InternalNET.g:94:5: lv_nom_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getReseauAccess().getNomEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_6);
            lv_nom_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getReseauRule());
            					}
            					set(
            						current,
            						"nom",
            						lv_nom_3_0,
            						"fr.n7.petrinet.txt.NET.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalNET.g:111:3: (otherlv_4= 'arc' otherlv_5= '{' ( (lv_arc_6_0= ruleArc ) ) (otherlv_7= ',' ( (lv_arc_8_0= ruleArc ) ) )* otherlv_9= '}' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==14) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalNET.g:112:4: otherlv_4= 'arc' otherlv_5= '{' ( (lv_arc_6_0= ruleArc ) ) (otherlv_7= ',' ( (lv_arc_8_0= ruleArc ) ) )* otherlv_9= '}'
                    {
                    otherlv_4=(Token)match(input,14,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getReseauAccess().getArcKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,12,FOLLOW_7); 

                    				newLeafNode(otherlv_5, grammarAccess.getReseauAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalNET.g:120:4: ( (lv_arc_6_0= ruleArc ) )
                    // InternalNET.g:121:5: (lv_arc_6_0= ruleArc )
                    {
                    // InternalNET.g:121:5: (lv_arc_6_0= ruleArc )
                    // InternalNET.g:122:6: lv_arc_6_0= ruleArc
                    {

                    						newCompositeNode(grammarAccess.getReseauAccess().getArcArcParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_arc_6_0=ruleArc();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReseauRule());
                    						}
                    						add(
                    							current,
                    							"arc",
                    							lv_arc_6_0,
                    							"fr.n7.petrinet.txt.NET.Arc");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalNET.g:139:4: (otherlv_7= ',' ( (lv_arc_8_0= ruleArc ) ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==15) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalNET.g:140:5: otherlv_7= ',' ( (lv_arc_8_0= ruleArc ) )
                    	    {
                    	    otherlv_7=(Token)match(input,15,FOLLOW_7); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getReseauAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalNET.g:144:5: ( (lv_arc_8_0= ruleArc ) )
                    	    // InternalNET.g:145:6: (lv_arc_8_0= ruleArc )
                    	    {
                    	    // InternalNET.g:145:6: (lv_arc_8_0= ruleArc )
                    	    // InternalNET.g:146:7: lv_arc_8_0= ruleArc
                    	    {

                    	    							newCompositeNode(grammarAccess.getReseauAccess().getArcArcParserRuleCall_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_arc_8_0=ruleArc();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getReseauRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"arc",
                    	    								lv_arc_8_0,
                    	    								"fr.n7.petrinet.txt.NET.Arc");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,16,FOLLOW_9); 

                    				newLeafNode(otherlv_9, grammarAccess.getReseauAccess().getRightCurlyBracketKeyword_4_4());
                    			

                    }
                    break;

            }

            otherlv_10=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_10, grammarAccess.getReseauAccess().getElementpetriKeyword_5());
            		
            otherlv_11=(Token)match(input,12,FOLLOW_10); 

            			newLeafNode(otherlv_11, grammarAccess.getReseauAccess().getLeftCurlyBracketKeyword_6());
            		
            // InternalNET.g:177:3: ( (lv_elementpetri_12_0= ruleElementPetri ) )
            // InternalNET.g:178:4: (lv_elementpetri_12_0= ruleElementPetri )
            {
            // InternalNET.g:178:4: (lv_elementpetri_12_0= ruleElementPetri )
            // InternalNET.g:179:5: lv_elementpetri_12_0= ruleElementPetri
            {

            					newCompositeNode(grammarAccess.getReseauAccess().getElementpetriElementPetriParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_8);
            lv_elementpetri_12_0=ruleElementPetri();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getReseauRule());
            					}
            					add(
            						current,
            						"elementpetri",
            						lv_elementpetri_12_0,
            						"fr.n7.petrinet.txt.NET.ElementPetri");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalNET.g:196:3: (otherlv_13= ',' ( (lv_elementpetri_14_0= ruleElementPetri ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalNET.g:197:4: otherlv_13= ',' ( (lv_elementpetri_14_0= ruleElementPetri ) )
            	    {
            	    otherlv_13=(Token)match(input,15,FOLLOW_10); 

            	    				newLeafNode(otherlv_13, grammarAccess.getReseauAccess().getCommaKeyword_8_0());
            	    			
            	    // InternalNET.g:201:4: ( (lv_elementpetri_14_0= ruleElementPetri ) )
            	    // InternalNET.g:202:5: (lv_elementpetri_14_0= ruleElementPetri )
            	    {
            	    // InternalNET.g:202:5: (lv_elementpetri_14_0= ruleElementPetri )
            	    // InternalNET.g:203:6: lv_elementpetri_14_0= ruleElementPetri
            	    {

            	    						newCompositeNode(grammarAccess.getReseauAccess().getElementpetriElementPetriParserRuleCall_8_1_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_elementpetri_14_0=ruleElementPetri();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getReseauRule());
            	    						}
            	    						add(
            	    							current,
            	    							"elementpetri",
            	    							lv_elementpetri_14_0,
            	    							"fr.n7.petrinet.txt.NET.ElementPetri");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_15=(Token)match(input,16,FOLLOW_11); 

            			newLeafNode(otherlv_15, grammarAccess.getReseauAccess().getRightCurlyBracketKeyword_9());
            		
            otherlv_16=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_16, grammarAccess.getReseauAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReseau"


    // $ANTLR start "entryRuleArc"
    // InternalNET.g:233:1: entryRuleArc returns [EObject current=null] : iv_ruleArc= ruleArc EOF ;
    public final EObject entryRuleArc() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArc = null;


        try {
            // InternalNET.g:233:44: (iv_ruleArc= ruleArc EOF )
            // InternalNET.g:234:2: iv_ruleArc= ruleArc EOF
            {
             newCompositeNode(grammarAccess.getArcRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleArc=ruleArc();

            state._fsp--;

             current =iv_ruleArc; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArc"


    // $ANTLR start "ruleArc"
    // InternalNET.g:240:1: ruleArc returns [EObject current=null] : (this_ArcPT_0= ruleArcPT | this_ArcTP_1= ruleArcTP ) ;
    public final EObject ruleArc() throws RecognitionException {
        EObject current = null;

        EObject this_ArcPT_0 = null;

        EObject this_ArcTP_1 = null;



        	enterRule();

        try {
            // InternalNET.g:246:2: ( (this_ArcPT_0= ruleArcPT | this_ArcTP_1= ruleArcTP ) )
            // InternalNET.g:247:2: (this_ArcPT_0= ruleArcPT | this_ArcTP_1= ruleArcTP )
            {
            // InternalNET.g:247:2: (this_ArcPT_0= ruleArcPT | this_ArcTP_1= ruleArcTP )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==19) ) {
                alt4=1;
            }
            else if ( (LA4_0==24) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalNET.g:248:3: this_ArcPT_0= ruleArcPT
                    {

                    			newCompositeNode(grammarAccess.getArcAccess().getArcPTParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_ArcPT_0=ruleArcPT();

                    state._fsp--;


                    			current = this_ArcPT_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalNET.g:257:3: this_ArcTP_1= ruleArcTP
                    {

                    			newCompositeNode(grammarAccess.getArcAccess().getArcTPParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ArcTP_1=ruleArcTP();

                    state._fsp--;


                    			current = this_ArcTP_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArc"


    // $ANTLR start "entryRuleElementPetri"
    // InternalNET.g:269:1: entryRuleElementPetri returns [EObject current=null] : iv_ruleElementPetri= ruleElementPetri EOF ;
    public final EObject entryRuleElementPetri() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElementPetri = null;


        try {
            // InternalNET.g:269:53: (iv_ruleElementPetri= ruleElementPetri EOF )
            // InternalNET.g:270:2: iv_ruleElementPetri= ruleElementPetri EOF
            {
             newCompositeNode(grammarAccess.getElementPetriRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleElementPetri=ruleElementPetri();

            state._fsp--;

             current =iv_ruleElementPetri; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElementPetri"


    // $ANTLR start "ruleElementPetri"
    // InternalNET.g:276:1: ruleElementPetri returns [EObject current=null] : (this_Transition_0= ruleTransition | this_Place_1= rulePlace ) ;
    public final EObject ruleElementPetri() throws RecognitionException {
        EObject current = null;

        EObject this_Transition_0 = null;

        EObject this_Place_1 = null;



        	enterRule();

        try {
            // InternalNET.g:282:2: ( (this_Transition_0= ruleTransition | this_Place_1= rulePlace ) )
            // InternalNET.g:283:2: (this_Transition_0= ruleTransition | this_Place_1= rulePlace )
            {
            // InternalNET.g:283:2: (this_Transition_0= ruleTransition | this_Place_1= rulePlace )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==25) ) {
                alt5=1;
            }
            else if ( (LA5_0==30) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalNET.g:284:3: this_Transition_0= ruleTransition
                    {

                    			newCompositeNode(grammarAccess.getElementPetriAccess().getTransitionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Transition_0=ruleTransition();

                    state._fsp--;


                    			current = this_Transition_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalNET.g:293:3: this_Place_1= rulePlace
                    {

                    			newCompositeNode(grammarAccess.getElementPetriAccess().getPlaceParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Place_1=rulePlace();

                    state._fsp--;


                    			current = this_Place_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElementPetri"


    // $ANTLR start "entryRuleEString"
    // InternalNET.g:305:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalNET.g:305:47: (iv_ruleEString= ruleEString EOF )
            // InternalNET.g:306:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalNET.g:312:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalNET.g:318:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalNET.g:319:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalNET.g:319:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_STRING) ) {
                alt6=1;
            }
            else if ( (LA6_0==RULE_ID) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalNET.g:320:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalNET.g:328:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEInt"
    // InternalNET.g:339:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalNET.g:339:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalNET.g:340:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalNET.g:346:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalNET.g:352:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalNET.g:353:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalNET.g:353:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalNET.g:354:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalNET.g:354:3: (kw= '-' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==18) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalNET.g:355:4: kw= '-'
                    {
                    kw=(Token)match(input,18,FOLLOW_12); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEIntAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getEIntAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleArcPT"
    // InternalNET.g:372:1: entryRuleArcPT returns [EObject current=null] : iv_ruleArcPT= ruleArcPT EOF ;
    public final EObject entryRuleArcPT() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArcPT = null;


        try {
            // InternalNET.g:372:46: (iv_ruleArcPT= ruleArcPT EOF )
            // InternalNET.g:373:2: iv_ruleArcPT= ruleArcPT EOF
            {
             newCompositeNode(grammarAccess.getArcPTRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleArcPT=ruleArcPT();

            state._fsp--;

             current =iv_ruleArcPT; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArcPT"


    // $ANTLR start "ruleArcPT"
    // InternalNET.g:379:1: ruleArcPT returns [EObject current=null] : (otherlv_0= 'ArcPT' otherlv_1= '{' otherlv_2= 'poids' ( (lv_poids_3_0= ruleEInt ) ) otherlv_4= 'type' ( (lv_type_5_0= ruleArcKind ) ) otherlv_6= 'transition' ( ( ruleEString ) ) otherlv_8= 'place' ( ( ruleEString ) ) otherlv_10= '}' ) ;
    public final EObject ruleArcPT() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_poids_3_0 = null;

        Enumerator lv_type_5_0 = null;



        	enterRule();

        try {
            // InternalNET.g:385:2: ( (otherlv_0= 'ArcPT' otherlv_1= '{' otherlv_2= 'poids' ( (lv_poids_3_0= ruleEInt ) ) otherlv_4= 'type' ( (lv_type_5_0= ruleArcKind ) ) otherlv_6= 'transition' ( ( ruleEString ) ) otherlv_8= 'place' ( ( ruleEString ) ) otherlv_10= '}' ) )
            // InternalNET.g:386:2: (otherlv_0= 'ArcPT' otherlv_1= '{' otherlv_2= 'poids' ( (lv_poids_3_0= ruleEInt ) ) otherlv_4= 'type' ( (lv_type_5_0= ruleArcKind ) ) otherlv_6= 'transition' ( ( ruleEString ) ) otherlv_8= 'place' ( ( ruleEString ) ) otherlv_10= '}' )
            {
            // InternalNET.g:386:2: (otherlv_0= 'ArcPT' otherlv_1= '{' otherlv_2= 'poids' ( (lv_poids_3_0= ruleEInt ) ) otherlv_4= 'type' ( (lv_type_5_0= ruleArcKind ) ) otherlv_6= 'transition' ( ( ruleEString ) ) otherlv_8= 'place' ( ( ruleEString ) ) otherlv_10= '}' )
            // InternalNET.g:387:3: otherlv_0= 'ArcPT' otherlv_1= '{' otherlv_2= 'poids' ( (lv_poids_3_0= ruleEInt ) ) otherlv_4= 'type' ( (lv_type_5_0= ruleArcKind ) ) otherlv_6= 'transition' ( ( ruleEString ) ) otherlv_8= 'place' ( ( ruleEString ) ) otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getArcPTAccess().getArcPTKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getArcPTAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,20,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getArcPTAccess().getPoidsKeyword_2());
            		
            // InternalNET.g:399:3: ( (lv_poids_3_0= ruleEInt ) )
            // InternalNET.g:400:4: (lv_poids_3_0= ruleEInt )
            {
            // InternalNET.g:400:4: (lv_poids_3_0= ruleEInt )
            // InternalNET.g:401:5: lv_poids_3_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getArcPTAccess().getPoidsEIntParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_15);
            lv_poids_3_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getArcPTRule());
            					}
            					set(
            						current,
            						"poids",
            						lv_poids_3_0,
            						"fr.n7.petrinet.txt.NET.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,21,FOLLOW_16); 

            			newLeafNode(otherlv_4, grammarAccess.getArcPTAccess().getTypeKeyword_4());
            		
            // InternalNET.g:422:3: ( (lv_type_5_0= ruleArcKind ) )
            // InternalNET.g:423:4: (lv_type_5_0= ruleArcKind )
            {
            // InternalNET.g:423:4: (lv_type_5_0= ruleArcKind )
            // InternalNET.g:424:5: lv_type_5_0= ruleArcKind
            {

            					newCompositeNode(grammarAccess.getArcPTAccess().getTypeArcKindEnumRuleCall_5_0());
            				
            pushFollow(FOLLOW_17);
            lv_type_5_0=ruleArcKind();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getArcPTRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_5_0,
            						"fr.n7.petrinet.txt.NET.ArcKind");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,22,FOLLOW_5); 

            			newLeafNode(otherlv_6, grammarAccess.getArcPTAccess().getTransitionKeyword_6());
            		
            // InternalNET.g:445:3: ( ( ruleEString ) )
            // InternalNET.g:446:4: ( ruleEString )
            {
            // InternalNET.g:446:4: ( ruleEString )
            // InternalNET.g:447:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getArcPTRule());
            					}
            				

            					newCompositeNode(grammarAccess.getArcPTAccess().getTransitionTransitionCrossReference_7_0());
            				
            pushFollow(FOLLOW_18);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,23,FOLLOW_5); 

            			newLeafNode(otherlv_8, grammarAccess.getArcPTAccess().getPlaceKeyword_8());
            		
            // InternalNET.g:465:3: ( ( ruleEString ) )
            // InternalNET.g:466:4: ( ruleEString )
            {
            // InternalNET.g:466:4: ( ruleEString )
            // InternalNET.g:467:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getArcPTRule());
            					}
            				

            					newCompositeNode(grammarAccess.getArcPTAccess().getPlacePlaceCrossReference_9_0());
            				
            pushFollow(FOLLOW_11);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_10=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getArcPTAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArcPT"


    // $ANTLR start "entryRuleArcTP"
    // InternalNET.g:489:1: entryRuleArcTP returns [EObject current=null] : iv_ruleArcTP= ruleArcTP EOF ;
    public final EObject entryRuleArcTP() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArcTP = null;


        try {
            // InternalNET.g:489:46: (iv_ruleArcTP= ruleArcTP EOF )
            // InternalNET.g:490:2: iv_ruleArcTP= ruleArcTP EOF
            {
             newCompositeNode(grammarAccess.getArcTPRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleArcTP=ruleArcTP();

            state._fsp--;

             current =iv_ruleArcTP; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArcTP"


    // $ANTLR start "ruleArcTP"
    // InternalNET.g:496:1: ruleArcTP returns [EObject current=null] : (otherlv_0= 'ArcTP' otherlv_1= '{' otherlv_2= 'poids' ( (lv_poids_3_0= ruleEInt ) ) otherlv_4= 'transition' ( ( ruleEString ) ) otherlv_6= 'place' ( ( ruleEString ) ) otherlv_8= '}' ) ;
    public final EObject ruleArcTP() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_poids_3_0 = null;



        	enterRule();

        try {
            // InternalNET.g:502:2: ( (otherlv_0= 'ArcTP' otherlv_1= '{' otherlv_2= 'poids' ( (lv_poids_3_0= ruleEInt ) ) otherlv_4= 'transition' ( ( ruleEString ) ) otherlv_6= 'place' ( ( ruleEString ) ) otherlv_8= '}' ) )
            // InternalNET.g:503:2: (otherlv_0= 'ArcTP' otherlv_1= '{' otherlv_2= 'poids' ( (lv_poids_3_0= ruleEInt ) ) otherlv_4= 'transition' ( ( ruleEString ) ) otherlv_6= 'place' ( ( ruleEString ) ) otherlv_8= '}' )
            {
            // InternalNET.g:503:2: (otherlv_0= 'ArcTP' otherlv_1= '{' otherlv_2= 'poids' ( (lv_poids_3_0= ruleEInt ) ) otherlv_4= 'transition' ( ( ruleEString ) ) otherlv_6= 'place' ( ( ruleEString ) ) otherlv_8= '}' )
            // InternalNET.g:504:3: otherlv_0= 'ArcTP' otherlv_1= '{' otherlv_2= 'poids' ( (lv_poids_3_0= ruleEInt ) ) otherlv_4= 'transition' ( ( ruleEString ) ) otherlv_6= 'place' ( ( ruleEString ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getArcTPAccess().getArcTPKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getArcTPAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,20,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getArcTPAccess().getPoidsKeyword_2());
            		
            // InternalNET.g:516:3: ( (lv_poids_3_0= ruleEInt ) )
            // InternalNET.g:517:4: (lv_poids_3_0= ruleEInt )
            {
            // InternalNET.g:517:4: (lv_poids_3_0= ruleEInt )
            // InternalNET.g:518:5: lv_poids_3_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getArcTPAccess().getPoidsEIntParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_17);
            lv_poids_3_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getArcTPRule());
            					}
            					set(
            						current,
            						"poids",
            						lv_poids_3_0,
            						"fr.n7.petrinet.txt.NET.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,22,FOLLOW_5); 

            			newLeafNode(otherlv_4, grammarAccess.getArcTPAccess().getTransitionKeyword_4());
            		
            // InternalNET.g:539:3: ( ( ruleEString ) )
            // InternalNET.g:540:4: ( ruleEString )
            {
            // InternalNET.g:540:4: ( ruleEString )
            // InternalNET.g:541:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getArcTPRule());
            					}
            				

            					newCompositeNode(grammarAccess.getArcTPAccess().getTransitionTransitionCrossReference_5_0());
            				
            pushFollow(FOLLOW_18);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,23,FOLLOW_5); 

            			newLeafNode(otherlv_6, grammarAccess.getArcTPAccess().getPlaceKeyword_6());
            		
            // InternalNET.g:559:3: ( ( ruleEString ) )
            // InternalNET.g:560:4: ( ruleEString )
            {
            // InternalNET.g:560:4: ( ruleEString )
            // InternalNET.g:561:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getArcTPRule());
            					}
            				

            					newCompositeNode(grammarAccess.getArcTPAccess().getPlacePlaceCrossReference_7_0());
            				
            pushFollow(FOLLOW_11);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getArcTPAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArcTP"


    // $ANTLR start "entryRuleTransition"
    // InternalNET.g:583:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalNET.g:583:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalNET.g:584:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalNET.g:590:1: ruleTransition returns [EObject current=null] : (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) (otherlv_4= 'arctp' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'arcpt' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )? otherlv_16= '}' ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        AntlrDatatypeRuleToken lv_nom_3_0 = null;



        	enterRule();

        try {
            // InternalNET.g:596:2: ( (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) (otherlv_4= 'arctp' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'arcpt' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )? otherlv_16= '}' ) )
            // InternalNET.g:597:2: (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) (otherlv_4= 'arctp' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'arcpt' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )? otherlv_16= '}' )
            {
            // InternalNET.g:597:2: (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) (otherlv_4= 'arctp' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'arcpt' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )? otherlv_16= '}' )
            // InternalNET.g:598:3: otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) (otherlv_4= 'arctp' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'arcpt' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )? otherlv_16= '}'
            {
            otherlv_0=(Token)match(input,25,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getTransitionKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getTransitionAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getNomKeyword_2());
            		
            // InternalNET.g:610:3: ( (lv_nom_3_0= ruleEString ) )
            // InternalNET.g:611:4: (lv_nom_3_0= ruleEString )
            {
            // InternalNET.g:611:4: (lv_nom_3_0= ruleEString )
            // InternalNET.g:612:5: lv_nom_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getTransitionAccess().getNomEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_19);
            lv_nom_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTransitionRule());
            					}
            					set(
            						current,
            						"nom",
            						lv_nom_3_0,
            						"fr.n7.petrinet.txt.NET.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalNET.g:629:3: (otherlv_4= 'arctp' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==26) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalNET.g:630:4: otherlv_4= 'arctp' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')'
                    {
                    otherlv_4=(Token)match(input,26,FOLLOW_20); 

                    				newLeafNode(otherlv_4, grammarAccess.getTransitionAccess().getArctpKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,27,FOLLOW_5); 

                    				newLeafNode(otherlv_5, grammarAccess.getTransitionAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalNET.g:638:4: ( ( ruleEString ) )
                    // InternalNET.g:639:5: ( ruleEString )
                    {
                    // InternalNET.g:639:5: ( ruleEString )
                    // InternalNET.g:640:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTransitionRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getTransitionAccess().getArctpArcTPCrossReference_4_2_0());
                    					
                    pushFollow(FOLLOW_21);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalNET.g:654:4: (otherlv_7= ',' ( ( ruleEString ) ) )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==15) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalNET.g:655:5: otherlv_7= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_7=(Token)match(input,15,FOLLOW_5); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getTransitionAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalNET.g:659:5: ( ( ruleEString ) )
                    	    // InternalNET.g:660:6: ( ruleEString )
                    	    {
                    	    // InternalNET.g:660:6: ( ruleEString )
                    	    // InternalNET.g:661:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getTransitionRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getTransitionAccess().getArctpArcTPCrossReference_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_21);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,28,FOLLOW_22); 

                    				newLeafNode(otherlv_9, grammarAccess.getTransitionAccess().getRightParenthesisKeyword_4_4());
                    			

                    }
                    break;

            }

            // InternalNET.g:681:3: (otherlv_10= 'arcpt' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==29) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalNET.g:682:4: otherlv_10= 'arcpt' otherlv_11= '(' ( ( ruleEString ) ) (otherlv_13= ',' ( ( ruleEString ) ) )* otherlv_15= ')'
                    {
                    otherlv_10=(Token)match(input,29,FOLLOW_20); 

                    				newLeafNode(otherlv_10, grammarAccess.getTransitionAccess().getArcptKeyword_5_0());
                    			
                    otherlv_11=(Token)match(input,27,FOLLOW_5); 

                    				newLeafNode(otherlv_11, grammarAccess.getTransitionAccess().getLeftParenthesisKeyword_5_1());
                    			
                    // InternalNET.g:690:4: ( ( ruleEString ) )
                    // InternalNET.g:691:5: ( ruleEString )
                    {
                    // InternalNET.g:691:5: ( ruleEString )
                    // InternalNET.g:692:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTransitionRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getTransitionAccess().getArcptArcPTCrossReference_5_2_0());
                    					
                    pushFollow(FOLLOW_21);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalNET.g:706:4: (otherlv_13= ',' ( ( ruleEString ) ) )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==15) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // InternalNET.g:707:5: otherlv_13= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_13=(Token)match(input,15,FOLLOW_5); 

                    	    					newLeafNode(otherlv_13, grammarAccess.getTransitionAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalNET.g:711:5: ( ( ruleEString ) )
                    	    // InternalNET.g:712:6: ( ruleEString )
                    	    {
                    	    // InternalNET.g:712:6: ( ruleEString )
                    	    // InternalNET.g:713:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getTransitionRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getTransitionAccess().getArcptArcPTCrossReference_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_21);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,28,FOLLOW_11); 

                    				newLeafNode(otherlv_15, grammarAccess.getTransitionAccess().getRightParenthesisKeyword_5_4());
                    			

                    }
                    break;

            }

            otherlv_16=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_16, grammarAccess.getTransitionAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "entryRulePlace"
    // InternalNET.g:741:1: entryRulePlace returns [EObject current=null] : iv_rulePlace= rulePlace EOF ;
    public final EObject entryRulePlace() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlace = null;


        try {
            // InternalNET.g:741:46: (iv_rulePlace= rulePlace EOF )
            // InternalNET.g:742:2: iv_rulePlace= rulePlace EOF
            {
             newCompositeNode(grammarAccess.getPlaceRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePlace=rulePlace();

            state._fsp--;

             current =iv_rulePlace; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlace"


    // $ANTLR start "rulePlace"
    // InternalNET.g:748:1: rulePlace returns [EObject current=null] : (otherlv_0= 'Place' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) otherlv_4= 'nbJetons' ( (lv_nbJetons_5_0= ruleEInt ) ) (otherlv_6= 'arctp' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? (otherlv_12= 'arcpt' otherlv_13= '(' ( ( ruleEString ) ) (otherlv_15= ',' ( ( ruleEString ) ) )* otherlv_17= ')' )? otherlv_18= '}' ) ;
    public final EObject rulePlace() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        AntlrDatatypeRuleToken lv_nom_3_0 = null;

        AntlrDatatypeRuleToken lv_nbJetons_5_0 = null;



        	enterRule();

        try {
            // InternalNET.g:754:2: ( (otherlv_0= 'Place' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) otherlv_4= 'nbJetons' ( (lv_nbJetons_5_0= ruleEInt ) ) (otherlv_6= 'arctp' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? (otherlv_12= 'arcpt' otherlv_13= '(' ( ( ruleEString ) ) (otherlv_15= ',' ( ( ruleEString ) ) )* otherlv_17= ')' )? otherlv_18= '}' ) )
            // InternalNET.g:755:2: (otherlv_0= 'Place' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) otherlv_4= 'nbJetons' ( (lv_nbJetons_5_0= ruleEInt ) ) (otherlv_6= 'arctp' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? (otherlv_12= 'arcpt' otherlv_13= '(' ( ( ruleEString ) ) (otherlv_15= ',' ( ( ruleEString ) ) )* otherlv_17= ')' )? otherlv_18= '}' )
            {
            // InternalNET.g:755:2: (otherlv_0= 'Place' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) otherlv_4= 'nbJetons' ( (lv_nbJetons_5_0= ruleEInt ) ) (otherlv_6= 'arctp' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? (otherlv_12= 'arcpt' otherlv_13= '(' ( ( ruleEString ) ) (otherlv_15= ',' ( ( ruleEString ) ) )* otherlv_17= ')' )? otherlv_18= '}' )
            // InternalNET.g:756:3: otherlv_0= 'Place' otherlv_1= '{' otherlv_2= 'nom' ( (lv_nom_3_0= ruleEString ) ) otherlv_4= 'nbJetons' ( (lv_nbJetons_5_0= ruleEInt ) ) (otherlv_6= 'arctp' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? (otherlv_12= 'arcpt' otherlv_13= '(' ( ( ruleEString ) ) (otherlv_15= ',' ( ( ruleEString ) ) )* otherlv_17= ')' )? otherlv_18= '}'
            {
            otherlv_0=(Token)match(input,30,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getPlaceAccess().getPlaceKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getPlaceAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getPlaceAccess().getNomKeyword_2());
            		
            // InternalNET.g:768:3: ( (lv_nom_3_0= ruleEString ) )
            // InternalNET.g:769:4: (lv_nom_3_0= ruleEString )
            {
            // InternalNET.g:769:4: (lv_nom_3_0= ruleEString )
            // InternalNET.g:770:5: lv_nom_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPlaceAccess().getNomEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_23);
            lv_nom_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPlaceRule());
            					}
            					set(
            						current,
            						"nom",
            						lv_nom_3_0,
            						"fr.n7.petrinet.txt.NET.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,31,FOLLOW_14); 

            			newLeafNode(otherlv_4, grammarAccess.getPlaceAccess().getNbJetonsKeyword_4());
            		
            // InternalNET.g:791:3: ( (lv_nbJetons_5_0= ruleEInt ) )
            // InternalNET.g:792:4: (lv_nbJetons_5_0= ruleEInt )
            {
            // InternalNET.g:792:4: (lv_nbJetons_5_0= ruleEInt )
            // InternalNET.g:793:5: lv_nbJetons_5_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getPlaceAccess().getNbJetonsEIntParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_19);
            lv_nbJetons_5_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPlaceRule());
            					}
            					set(
            						current,
            						"nbJetons",
            						lv_nbJetons_5_0,
            						"fr.n7.petrinet.txt.NET.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalNET.g:810:3: (otherlv_6= 'arctp' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==26) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalNET.g:811:4: otherlv_6= 'arctp' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')'
                    {
                    otherlv_6=(Token)match(input,26,FOLLOW_20); 

                    				newLeafNode(otherlv_6, grammarAccess.getPlaceAccess().getArctpKeyword_6_0());
                    			
                    otherlv_7=(Token)match(input,27,FOLLOW_5); 

                    				newLeafNode(otherlv_7, grammarAccess.getPlaceAccess().getLeftParenthesisKeyword_6_1());
                    			
                    // InternalNET.g:819:4: ( ( ruleEString ) )
                    // InternalNET.g:820:5: ( ruleEString )
                    {
                    // InternalNET.g:820:5: ( ruleEString )
                    // InternalNET.g:821:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPlaceRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getPlaceAccess().getArctpArcTPCrossReference_6_2_0());
                    					
                    pushFollow(FOLLOW_21);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalNET.g:835:4: (otherlv_9= ',' ( ( ruleEString ) ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==15) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalNET.g:836:5: otherlv_9= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_9=(Token)match(input,15,FOLLOW_5); 

                    	    					newLeafNode(otherlv_9, grammarAccess.getPlaceAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalNET.g:840:5: ( ( ruleEString ) )
                    	    // InternalNET.g:841:6: ( ruleEString )
                    	    {
                    	    // InternalNET.g:841:6: ( ruleEString )
                    	    // InternalNET.g:842:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getPlaceRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getPlaceAccess().getArctpArcTPCrossReference_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_21);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,28,FOLLOW_22); 

                    				newLeafNode(otherlv_11, grammarAccess.getPlaceAccess().getRightParenthesisKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalNET.g:862:3: (otherlv_12= 'arcpt' otherlv_13= '(' ( ( ruleEString ) ) (otherlv_15= ',' ( ( ruleEString ) ) )* otherlv_17= ')' )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==29) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalNET.g:863:4: otherlv_12= 'arcpt' otherlv_13= '(' ( ( ruleEString ) ) (otherlv_15= ',' ( ( ruleEString ) ) )* otherlv_17= ')'
                    {
                    otherlv_12=(Token)match(input,29,FOLLOW_20); 

                    				newLeafNode(otherlv_12, grammarAccess.getPlaceAccess().getArcptKeyword_7_0());
                    			
                    otherlv_13=(Token)match(input,27,FOLLOW_5); 

                    				newLeafNode(otherlv_13, grammarAccess.getPlaceAccess().getLeftParenthesisKeyword_7_1());
                    			
                    // InternalNET.g:871:4: ( ( ruleEString ) )
                    // InternalNET.g:872:5: ( ruleEString )
                    {
                    // InternalNET.g:872:5: ( ruleEString )
                    // InternalNET.g:873:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPlaceRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getPlaceAccess().getArcptArcPTCrossReference_7_2_0());
                    					
                    pushFollow(FOLLOW_21);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalNET.g:887:4: (otherlv_15= ',' ( ( ruleEString ) ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==15) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalNET.g:888:5: otherlv_15= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_15=(Token)match(input,15,FOLLOW_5); 

                    	    					newLeafNode(otherlv_15, grammarAccess.getPlaceAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalNET.g:892:5: ( ( ruleEString ) )
                    	    // InternalNET.g:893:6: ( ruleEString )
                    	    {
                    	    // InternalNET.g:893:6: ( ruleEString )
                    	    // InternalNET.g:894:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getPlaceRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getPlaceAccess().getArcptArcPTCrossReference_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_21);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,28,FOLLOW_11); 

                    				newLeafNode(otherlv_17, grammarAccess.getPlaceAccess().getRightParenthesisKeyword_7_4());
                    			

                    }
                    break;

            }

            otherlv_18=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_18, grammarAccess.getPlaceAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlace"


    // $ANTLR start "ruleArcKind"
    // InternalNET.g:922:1: ruleArcKind returns [Enumerator current=null] : ( (enumLiteral_0= 'READ_ARC' ) | (enumLiteral_1= 'REGULAR' ) ) ;
    public final Enumerator ruleArcKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalNET.g:928:2: ( ( (enumLiteral_0= 'READ_ARC' ) | (enumLiteral_1= 'REGULAR' ) ) )
            // InternalNET.g:929:2: ( (enumLiteral_0= 'READ_ARC' ) | (enumLiteral_1= 'REGULAR' ) )
            {
            // InternalNET.g:929:2: ( (enumLiteral_0= 'READ_ARC' ) | (enumLiteral_1= 'REGULAR' ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==32) ) {
                alt16=1;
            }
            else if ( (LA16_0==33) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalNET.g:930:3: (enumLiteral_0= 'READ_ARC' )
                    {
                    // InternalNET.g:930:3: (enumLiteral_0= 'READ_ARC' )
                    // InternalNET.g:931:4: enumLiteral_0= 'READ_ARC'
                    {
                    enumLiteral_0=(Token)match(input,32,FOLLOW_2); 

                    				current = grammarAccess.getArcKindAccess().getREAD_ARCEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getArcKindAccess().getREAD_ARCEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalNET.g:938:3: (enumLiteral_1= 'REGULAR' )
                    {
                    // InternalNET.g:938:3: (enumLiteral_1= 'REGULAR' )
                    // InternalNET.g:939:4: enumLiteral_1= 'REGULAR'
                    {
                    enumLiteral_1=(Token)match(input,33,FOLLOW_2); 

                    				current = grammarAccess.getArcKindAccess().getREGULAREnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getArcKindAccess().getREGULAREnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArcKind"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000024000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000001080000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000042000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000040040L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000300000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000024010000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000010008000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000020010000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000080000000L});

}