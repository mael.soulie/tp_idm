/*
 * generated by Xtext 2.12.0
 */
package fr.n7.petrinet.txt.serializer;

import com.google.inject.Inject;
import fr.n7.petrinet.txt.services.NETGrammarAccess;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import petrinet.ArcPT;
import petrinet.ArcTP;
import petrinet.PetriNetPackage;
import petrinet.Place;
import petrinet.Reseau;
import petrinet.Transition;

@SuppressWarnings("all")
public class NETSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private NETGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == PetriNetPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case PetriNetPackage.ARC_PT:
				sequence_ArcPT(context, (ArcPT) semanticObject); 
				return; 
			case PetriNetPackage.ARC_TP:
				sequence_ArcTP(context, (ArcTP) semanticObject); 
				return; 
			case PetriNetPackage.PLACE:
				sequence_Place(context, (Place) semanticObject); 
				return; 
			case PetriNetPackage.RESEAU:
				sequence_Reseau(context, (Reseau) semanticObject); 
				return; 
			case PetriNetPackage.TRANSITION:
				sequence_Transition(context, (Transition) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     Arc returns ArcPT
	 *     ArcPT returns ArcPT
	 *
	 * Constraint:
	 *     (poids=EInt type=ArcKind transition=[Transition|EString] place=[Place|EString])
	 */
	protected void sequence_ArcPT(ISerializationContext context, ArcPT semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, PetriNetPackage.Literals.ARC__POIDS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PetriNetPackage.Literals.ARC__POIDS));
			if (transientValues.isValueTransient(semanticObject, PetriNetPackage.Literals.ARC_PT__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PetriNetPackage.Literals.ARC_PT__TYPE));
			if (transientValues.isValueTransient(semanticObject, PetriNetPackage.Literals.ARC_PT__TRANSITION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PetriNetPackage.Literals.ARC_PT__TRANSITION));
			if (transientValues.isValueTransient(semanticObject, PetriNetPackage.Literals.ARC_PT__PLACE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PetriNetPackage.Literals.ARC_PT__PLACE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getArcPTAccess().getPoidsEIntParserRuleCall_3_0(), semanticObject.getPoids());
		feeder.accept(grammarAccess.getArcPTAccess().getTypeArcKindEnumRuleCall_5_0(), semanticObject.getType());
		feeder.accept(grammarAccess.getArcPTAccess().getTransitionTransitionEStringParserRuleCall_7_0_1(), semanticObject.eGet(PetriNetPackage.Literals.ARC_PT__TRANSITION, false));
		feeder.accept(grammarAccess.getArcPTAccess().getPlacePlaceEStringParserRuleCall_9_0_1(), semanticObject.eGet(PetriNetPackage.Literals.ARC_PT__PLACE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Arc returns ArcTP
	 *     ArcTP returns ArcTP
	 *
	 * Constraint:
	 *     (poids=EInt transition=[Transition|EString] place=[Place|EString])
	 */
	protected void sequence_ArcTP(ISerializationContext context, ArcTP semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, PetriNetPackage.Literals.ARC__POIDS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PetriNetPackage.Literals.ARC__POIDS));
			if (transientValues.isValueTransient(semanticObject, PetriNetPackage.Literals.ARC_TP__TRANSITION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PetriNetPackage.Literals.ARC_TP__TRANSITION));
			if (transientValues.isValueTransient(semanticObject, PetriNetPackage.Literals.ARC_TP__PLACE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PetriNetPackage.Literals.ARC_TP__PLACE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getArcTPAccess().getPoidsEIntParserRuleCall_3_0(), semanticObject.getPoids());
		feeder.accept(grammarAccess.getArcTPAccess().getTransitionTransitionEStringParserRuleCall_5_0_1(), semanticObject.eGet(PetriNetPackage.Literals.ARC_TP__TRANSITION, false));
		feeder.accept(grammarAccess.getArcTPAccess().getPlacePlaceEStringParserRuleCall_7_0_1(), semanticObject.eGet(PetriNetPackage.Literals.ARC_TP__PLACE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     ElementPetri returns Place
	 *     Place returns Place
	 *
	 * Constraint:
	 *     (nom=EString nbJetons=EInt (arctp+=[ArcTP|EString] arctp+=[ArcTP|EString]*)? (arcpt+=[ArcPT|EString] arcpt+=[ArcPT|EString]*)?)
	 */
	protected void sequence_Place(ISerializationContext context, Place semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Reseau returns Reseau
	 *
	 * Constraint:
	 *     (nom=EString (arc+=Arc arc+=Arc*)? elementpetri+=ElementPetri elementpetri+=ElementPetri*)
	 */
	protected void sequence_Reseau(ISerializationContext context, Reseau semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ElementPetri returns Transition
	 *     Transition returns Transition
	 *
	 * Constraint:
	 *     (nom=EString (arctp+=[ArcTP|EString] arctp+=[ArcTP|EString]*)? (arcpt+=[ArcPT|EString] arcpt+=[ArcPT|EString]*)?)
	 */
	protected void sequence_Transition(ISerializationContext context, Transition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
}
