package fr.n7.petrinet.txt.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.n7.petrinet.txt.services.NETGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalNETParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'READ_ARC'", "'REGULAR'", "'Reseau'", "'{'", "'nom'", "'elementpetri'", "'}'", "'arc'", "','", "'-'", "'ArcPT'", "'poids'", "'type'", "'transition'", "'place'", "'ArcTP'", "'Transition'", "'arctp'", "'('", "')'", "'arcpt'", "'Place'", "'nbJetons'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalNETParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalNETParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalNETParser.tokenNames; }
    public String getGrammarFileName() { return "InternalNET.g"; }


    	private NETGrammarAccess grammarAccess;

    	public void setGrammarAccess(NETGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleReseau"
    // InternalNET.g:53:1: entryRuleReseau : ruleReseau EOF ;
    public final void entryRuleReseau() throws RecognitionException {
        try {
            // InternalNET.g:54:1: ( ruleReseau EOF )
            // InternalNET.g:55:1: ruleReseau EOF
            {
             before(grammarAccess.getReseauRule()); 
            pushFollow(FOLLOW_1);
            ruleReseau();

            state._fsp--;

             after(grammarAccess.getReseauRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReseau"


    // $ANTLR start "ruleReseau"
    // InternalNET.g:62:1: ruleReseau : ( ( rule__Reseau__Group__0 ) ) ;
    public final void ruleReseau() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:66:2: ( ( ( rule__Reseau__Group__0 ) ) )
            // InternalNET.g:67:2: ( ( rule__Reseau__Group__0 ) )
            {
            // InternalNET.g:67:2: ( ( rule__Reseau__Group__0 ) )
            // InternalNET.g:68:3: ( rule__Reseau__Group__0 )
            {
             before(grammarAccess.getReseauAccess().getGroup()); 
            // InternalNET.g:69:3: ( rule__Reseau__Group__0 )
            // InternalNET.g:69:4: rule__Reseau__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Reseau__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReseauAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReseau"


    // $ANTLR start "entryRuleArc"
    // InternalNET.g:78:1: entryRuleArc : ruleArc EOF ;
    public final void entryRuleArc() throws RecognitionException {
        try {
            // InternalNET.g:79:1: ( ruleArc EOF )
            // InternalNET.g:80:1: ruleArc EOF
            {
             before(grammarAccess.getArcRule()); 
            pushFollow(FOLLOW_1);
            ruleArc();

            state._fsp--;

             after(grammarAccess.getArcRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleArc"


    // $ANTLR start "ruleArc"
    // InternalNET.g:87:1: ruleArc : ( ( rule__Arc__Alternatives ) ) ;
    public final void ruleArc() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:91:2: ( ( ( rule__Arc__Alternatives ) ) )
            // InternalNET.g:92:2: ( ( rule__Arc__Alternatives ) )
            {
            // InternalNET.g:92:2: ( ( rule__Arc__Alternatives ) )
            // InternalNET.g:93:3: ( rule__Arc__Alternatives )
            {
             before(grammarAccess.getArcAccess().getAlternatives()); 
            // InternalNET.g:94:3: ( rule__Arc__Alternatives )
            // InternalNET.g:94:4: rule__Arc__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Arc__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getArcAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArc"


    // $ANTLR start "entryRuleElementPetri"
    // InternalNET.g:103:1: entryRuleElementPetri : ruleElementPetri EOF ;
    public final void entryRuleElementPetri() throws RecognitionException {
        try {
            // InternalNET.g:104:1: ( ruleElementPetri EOF )
            // InternalNET.g:105:1: ruleElementPetri EOF
            {
             before(grammarAccess.getElementPetriRule()); 
            pushFollow(FOLLOW_1);
            ruleElementPetri();

            state._fsp--;

             after(grammarAccess.getElementPetriRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleElementPetri"


    // $ANTLR start "ruleElementPetri"
    // InternalNET.g:112:1: ruleElementPetri : ( ( rule__ElementPetri__Alternatives ) ) ;
    public final void ruleElementPetri() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:116:2: ( ( ( rule__ElementPetri__Alternatives ) ) )
            // InternalNET.g:117:2: ( ( rule__ElementPetri__Alternatives ) )
            {
            // InternalNET.g:117:2: ( ( rule__ElementPetri__Alternatives ) )
            // InternalNET.g:118:3: ( rule__ElementPetri__Alternatives )
            {
             before(grammarAccess.getElementPetriAccess().getAlternatives()); 
            // InternalNET.g:119:3: ( rule__ElementPetri__Alternatives )
            // InternalNET.g:119:4: rule__ElementPetri__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ElementPetri__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getElementPetriAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleElementPetri"


    // $ANTLR start "entryRuleEString"
    // InternalNET.g:128:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalNET.g:129:1: ( ruleEString EOF )
            // InternalNET.g:130:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalNET.g:137:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:141:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalNET.g:142:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalNET.g:142:2: ( ( rule__EString__Alternatives ) )
            // InternalNET.g:143:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalNET.g:144:3: ( rule__EString__Alternatives )
            // InternalNET.g:144:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEInt"
    // InternalNET.g:153:1: entryRuleEInt : ruleEInt EOF ;
    public final void entryRuleEInt() throws RecognitionException {
        try {
            // InternalNET.g:154:1: ( ruleEInt EOF )
            // InternalNET.g:155:1: ruleEInt EOF
            {
             before(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getEIntRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalNET.g:162:1: ruleEInt : ( ( rule__EInt__Group__0 ) ) ;
    public final void ruleEInt() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:166:2: ( ( ( rule__EInt__Group__0 ) ) )
            // InternalNET.g:167:2: ( ( rule__EInt__Group__0 ) )
            {
            // InternalNET.g:167:2: ( ( rule__EInt__Group__0 ) )
            // InternalNET.g:168:3: ( rule__EInt__Group__0 )
            {
             before(grammarAccess.getEIntAccess().getGroup()); 
            // InternalNET.g:169:3: ( rule__EInt__Group__0 )
            // InternalNET.g:169:4: rule__EInt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EInt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEIntAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleArcPT"
    // InternalNET.g:178:1: entryRuleArcPT : ruleArcPT EOF ;
    public final void entryRuleArcPT() throws RecognitionException {
        try {
            // InternalNET.g:179:1: ( ruleArcPT EOF )
            // InternalNET.g:180:1: ruleArcPT EOF
            {
             before(grammarAccess.getArcPTRule()); 
            pushFollow(FOLLOW_1);
            ruleArcPT();

            state._fsp--;

             after(grammarAccess.getArcPTRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleArcPT"


    // $ANTLR start "ruleArcPT"
    // InternalNET.g:187:1: ruleArcPT : ( ( rule__ArcPT__Group__0 ) ) ;
    public final void ruleArcPT() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:191:2: ( ( ( rule__ArcPT__Group__0 ) ) )
            // InternalNET.g:192:2: ( ( rule__ArcPT__Group__0 ) )
            {
            // InternalNET.g:192:2: ( ( rule__ArcPT__Group__0 ) )
            // InternalNET.g:193:3: ( rule__ArcPT__Group__0 )
            {
             before(grammarAccess.getArcPTAccess().getGroup()); 
            // InternalNET.g:194:3: ( rule__ArcPT__Group__0 )
            // InternalNET.g:194:4: rule__ArcPT__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getArcPTAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArcPT"


    // $ANTLR start "entryRuleArcTP"
    // InternalNET.g:203:1: entryRuleArcTP : ruleArcTP EOF ;
    public final void entryRuleArcTP() throws RecognitionException {
        try {
            // InternalNET.g:204:1: ( ruleArcTP EOF )
            // InternalNET.g:205:1: ruleArcTP EOF
            {
             before(grammarAccess.getArcTPRule()); 
            pushFollow(FOLLOW_1);
            ruleArcTP();

            state._fsp--;

             after(grammarAccess.getArcTPRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleArcTP"


    // $ANTLR start "ruleArcTP"
    // InternalNET.g:212:1: ruleArcTP : ( ( rule__ArcTP__Group__0 ) ) ;
    public final void ruleArcTP() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:216:2: ( ( ( rule__ArcTP__Group__0 ) ) )
            // InternalNET.g:217:2: ( ( rule__ArcTP__Group__0 ) )
            {
            // InternalNET.g:217:2: ( ( rule__ArcTP__Group__0 ) )
            // InternalNET.g:218:3: ( rule__ArcTP__Group__0 )
            {
             before(grammarAccess.getArcTPAccess().getGroup()); 
            // InternalNET.g:219:3: ( rule__ArcTP__Group__0 )
            // InternalNET.g:219:4: rule__ArcTP__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ArcTP__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getArcTPAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArcTP"


    // $ANTLR start "entryRuleTransition"
    // InternalNET.g:228:1: entryRuleTransition : ruleTransition EOF ;
    public final void entryRuleTransition() throws RecognitionException {
        try {
            // InternalNET.g:229:1: ( ruleTransition EOF )
            // InternalNET.g:230:1: ruleTransition EOF
            {
             before(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getTransitionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalNET.g:237:1: ruleTransition : ( ( rule__Transition__Group__0 ) ) ;
    public final void ruleTransition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:241:2: ( ( ( rule__Transition__Group__0 ) ) )
            // InternalNET.g:242:2: ( ( rule__Transition__Group__0 ) )
            {
            // InternalNET.g:242:2: ( ( rule__Transition__Group__0 ) )
            // InternalNET.g:243:3: ( rule__Transition__Group__0 )
            {
             before(grammarAccess.getTransitionAccess().getGroup()); 
            // InternalNET.g:244:3: ( rule__Transition__Group__0 )
            // InternalNET.g:244:4: rule__Transition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "entryRulePlace"
    // InternalNET.g:253:1: entryRulePlace : rulePlace EOF ;
    public final void entryRulePlace() throws RecognitionException {
        try {
            // InternalNET.g:254:1: ( rulePlace EOF )
            // InternalNET.g:255:1: rulePlace EOF
            {
             before(grammarAccess.getPlaceRule()); 
            pushFollow(FOLLOW_1);
            rulePlace();

            state._fsp--;

             after(grammarAccess.getPlaceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePlace"


    // $ANTLR start "rulePlace"
    // InternalNET.g:262:1: rulePlace : ( ( rule__Place__Group__0 ) ) ;
    public final void rulePlace() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:266:2: ( ( ( rule__Place__Group__0 ) ) )
            // InternalNET.g:267:2: ( ( rule__Place__Group__0 ) )
            {
            // InternalNET.g:267:2: ( ( rule__Place__Group__0 ) )
            // InternalNET.g:268:3: ( rule__Place__Group__0 )
            {
             before(grammarAccess.getPlaceAccess().getGroup()); 
            // InternalNET.g:269:3: ( rule__Place__Group__0 )
            // InternalNET.g:269:4: rule__Place__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Place__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePlace"


    // $ANTLR start "ruleArcKind"
    // InternalNET.g:278:1: ruleArcKind : ( ( rule__ArcKind__Alternatives ) ) ;
    public final void ruleArcKind() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:282:1: ( ( ( rule__ArcKind__Alternatives ) ) )
            // InternalNET.g:283:2: ( ( rule__ArcKind__Alternatives ) )
            {
            // InternalNET.g:283:2: ( ( rule__ArcKind__Alternatives ) )
            // InternalNET.g:284:3: ( rule__ArcKind__Alternatives )
            {
             before(grammarAccess.getArcKindAccess().getAlternatives()); 
            // InternalNET.g:285:3: ( rule__ArcKind__Alternatives )
            // InternalNET.g:285:4: rule__ArcKind__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ArcKind__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getArcKindAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArcKind"


    // $ANTLR start "rule__Arc__Alternatives"
    // InternalNET.g:293:1: rule__Arc__Alternatives : ( ( ruleArcPT ) | ( ruleArcTP ) );
    public final void rule__Arc__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:297:1: ( ( ruleArcPT ) | ( ruleArcTP ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==21) ) {
                alt1=1;
            }
            else if ( (LA1_0==26) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalNET.g:298:2: ( ruleArcPT )
                    {
                    // InternalNET.g:298:2: ( ruleArcPT )
                    // InternalNET.g:299:3: ruleArcPT
                    {
                     before(grammarAccess.getArcAccess().getArcPTParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleArcPT();

                    state._fsp--;

                     after(grammarAccess.getArcAccess().getArcPTParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNET.g:304:2: ( ruleArcTP )
                    {
                    // InternalNET.g:304:2: ( ruleArcTP )
                    // InternalNET.g:305:3: ruleArcTP
                    {
                     before(grammarAccess.getArcAccess().getArcTPParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleArcTP();

                    state._fsp--;

                     after(grammarAccess.getArcAccess().getArcTPParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Arc__Alternatives"


    // $ANTLR start "rule__ElementPetri__Alternatives"
    // InternalNET.g:314:1: rule__ElementPetri__Alternatives : ( ( ruleTransition ) | ( rulePlace ) );
    public final void rule__ElementPetri__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:318:1: ( ( ruleTransition ) | ( rulePlace ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==27) ) {
                alt2=1;
            }
            else if ( (LA2_0==32) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalNET.g:319:2: ( ruleTransition )
                    {
                    // InternalNET.g:319:2: ( ruleTransition )
                    // InternalNET.g:320:3: ruleTransition
                    {
                     before(grammarAccess.getElementPetriAccess().getTransitionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleTransition();

                    state._fsp--;

                     after(grammarAccess.getElementPetriAccess().getTransitionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNET.g:325:2: ( rulePlace )
                    {
                    // InternalNET.g:325:2: ( rulePlace )
                    // InternalNET.g:326:3: rulePlace
                    {
                     before(grammarAccess.getElementPetriAccess().getPlaceParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    rulePlace();

                    state._fsp--;

                     after(grammarAccess.getElementPetriAccess().getPlaceParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ElementPetri__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalNET.g:335:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:339:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_STRING) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalNET.g:340:2: ( RULE_STRING )
                    {
                    // InternalNET.g:340:2: ( RULE_STRING )
                    // InternalNET.g:341:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNET.g:346:2: ( RULE_ID )
                    {
                    // InternalNET.g:346:2: ( RULE_ID )
                    // InternalNET.g:347:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__ArcKind__Alternatives"
    // InternalNET.g:356:1: rule__ArcKind__Alternatives : ( ( ( 'READ_ARC' ) ) | ( ( 'REGULAR' ) ) );
    public final void rule__ArcKind__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:360:1: ( ( ( 'READ_ARC' ) ) | ( ( 'REGULAR' ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==11) ) {
                alt4=1;
            }
            else if ( (LA4_0==12) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalNET.g:361:2: ( ( 'READ_ARC' ) )
                    {
                    // InternalNET.g:361:2: ( ( 'READ_ARC' ) )
                    // InternalNET.g:362:3: ( 'READ_ARC' )
                    {
                     before(grammarAccess.getArcKindAccess().getREAD_ARCEnumLiteralDeclaration_0()); 
                    // InternalNET.g:363:3: ( 'READ_ARC' )
                    // InternalNET.g:363:4: 'READ_ARC'
                    {
                    match(input,11,FOLLOW_2); 

                    }

                     after(grammarAccess.getArcKindAccess().getREAD_ARCEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalNET.g:367:2: ( ( 'REGULAR' ) )
                    {
                    // InternalNET.g:367:2: ( ( 'REGULAR' ) )
                    // InternalNET.g:368:3: ( 'REGULAR' )
                    {
                     before(grammarAccess.getArcKindAccess().getREGULAREnumLiteralDeclaration_1()); 
                    // InternalNET.g:369:3: ( 'REGULAR' )
                    // InternalNET.g:369:4: 'REGULAR'
                    {
                    match(input,12,FOLLOW_2); 

                    }

                     after(grammarAccess.getArcKindAccess().getREGULAREnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcKind__Alternatives"


    // $ANTLR start "rule__Reseau__Group__0"
    // InternalNET.g:377:1: rule__Reseau__Group__0 : rule__Reseau__Group__0__Impl rule__Reseau__Group__1 ;
    public final void rule__Reseau__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:381:1: ( rule__Reseau__Group__0__Impl rule__Reseau__Group__1 )
            // InternalNET.g:382:2: rule__Reseau__Group__0__Impl rule__Reseau__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Reseau__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__0"


    // $ANTLR start "rule__Reseau__Group__0__Impl"
    // InternalNET.g:389:1: rule__Reseau__Group__0__Impl : ( 'Reseau' ) ;
    public final void rule__Reseau__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:393:1: ( ( 'Reseau' ) )
            // InternalNET.g:394:1: ( 'Reseau' )
            {
            // InternalNET.g:394:1: ( 'Reseau' )
            // InternalNET.g:395:2: 'Reseau'
            {
             before(grammarAccess.getReseauAccess().getReseauKeyword_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getReseauKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__0__Impl"


    // $ANTLR start "rule__Reseau__Group__1"
    // InternalNET.g:404:1: rule__Reseau__Group__1 : rule__Reseau__Group__1__Impl rule__Reseau__Group__2 ;
    public final void rule__Reseau__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:408:1: ( rule__Reseau__Group__1__Impl rule__Reseau__Group__2 )
            // InternalNET.g:409:2: rule__Reseau__Group__1__Impl rule__Reseau__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Reseau__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__1"


    // $ANTLR start "rule__Reseau__Group__1__Impl"
    // InternalNET.g:416:1: rule__Reseau__Group__1__Impl : ( '{' ) ;
    public final void rule__Reseau__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:420:1: ( ( '{' ) )
            // InternalNET.g:421:1: ( '{' )
            {
            // InternalNET.g:421:1: ( '{' )
            // InternalNET.g:422:2: '{'
            {
             before(grammarAccess.getReseauAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__1__Impl"


    // $ANTLR start "rule__Reseau__Group__2"
    // InternalNET.g:431:1: rule__Reseau__Group__2 : rule__Reseau__Group__2__Impl rule__Reseau__Group__3 ;
    public final void rule__Reseau__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:435:1: ( rule__Reseau__Group__2__Impl rule__Reseau__Group__3 )
            // InternalNET.g:436:2: rule__Reseau__Group__2__Impl rule__Reseau__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Reseau__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__2"


    // $ANTLR start "rule__Reseau__Group__2__Impl"
    // InternalNET.g:443:1: rule__Reseau__Group__2__Impl : ( 'nom' ) ;
    public final void rule__Reseau__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:447:1: ( ( 'nom' ) )
            // InternalNET.g:448:1: ( 'nom' )
            {
            // InternalNET.g:448:1: ( 'nom' )
            // InternalNET.g:449:2: 'nom'
            {
             before(grammarAccess.getReseauAccess().getNomKeyword_2()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getNomKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__2__Impl"


    // $ANTLR start "rule__Reseau__Group__3"
    // InternalNET.g:458:1: rule__Reseau__Group__3 : rule__Reseau__Group__3__Impl rule__Reseau__Group__4 ;
    public final void rule__Reseau__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:462:1: ( rule__Reseau__Group__3__Impl rule__Reseau__Group__4 )
            // InternalNET.g:463:2: rule__Reseau__Group__3__Impl rule__Reseau__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Reseau__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__3"


    // $ANTLR start "rule__Reseau__Group__3__Impl"
    // InternalNET.g:470:1: rule__Reseau__Group__3__Impl : ( ( rule__Reseau__NomAssignment_3 ) ) ;
    public final void rule__Reseau__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:474:1: ( ( ( rule__Reseau__NomAssignment_3 ) ) )
            // InternalNET.g:475:1: ( ( rule__Reseau__NomAssignment_3 ) )
            {
            // InternalNET.g:475:1: ( ( rule__Reseau__NomAssignment_3 ) )
            // InternalNET.g:476:2: ( rule__Reseau__NomAssignment_3 )
            {
             before(grammarAccess.getReseauAccess().getNomAssignment_3()); 
            // InternalNET.g:477:2: ( rule__Reseau__NomAssignment_3 )
            // InternalNET.g:477:3: rule__Reseau__NomAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Reseau__NomAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getReseauAccess().getNomAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__3__Impl"


    // $ANTLR start "rule__Reseau__Group__4"
    // InternalNET.g:485:1: rule__Reseau__Group__4 : rule__Reseau__Group__4__Impl rule__Reseau__Group__5 ;
    public final void rule__Reseau__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:489:1: ( rule__Reseau__Group__4__Impl rule__Reseau__Group__5 )
            // InternalNET.g:490:2: rule__Reseau__Group__4__Impl rule__Reseau__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__Reseau__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__4"


    // $ANTLR start "rule__Reseau__Group__4__Impl"
    // InternalNET.g:497:1: rule__Reseau__Group__4__Impl : ( ( rule__Reseau__Group_4__0 )? ) ;
    public final void rule__Reseau__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:501:1: ( ( ( rule__Reseau__Group_4__0 )? ) )
            // InternalNET.g:502:1: ( ( rule__Reseau__Group_4__0 )? )
            {
            // InternalNET.g:502:1: ( ( rule__Reseau__Group_4__0 )? )
            // InternalNET.g:503:2: ( rule__Reseau__Group_4__0 )?
            {
             before(grammarAccess.getReseauAccess().getGroup_4()); 
            // InternalNET.g:504:2: ( rule__Reseau__Group_4__0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalNET.g:504:3: rule__Reseau__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Reseau__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReseauAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__4__Impl"


    // $ANTLR start "rule__Reseau__Group__5"
    // InternalNET.g:512:1: rule__Reseau__Group__5 : rule__Reseau__Group__5__Impl rule__Reseau__Group__6 ;
    public final void rule__Reseau__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:516:1: ( rule__Reseau__Group__5__Impl rule__Reseau__Group__6 )
            // InternalNET.g:517:2: rule__Reseau__Group__5__Impl rule__Reseau__Group__6
            {
            pushFollow(FOLLOW_3);
            rule__Reseau__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__5"


    // $ANTLR start "rule__Reseau__Group__5__Impl"
    // InternalNET.g:524:1: rule__Reseau__Group__5__Impl : ( 'elementpetri' ) ;
    public final void rule__Reseau__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:528:1: ( ( 'elementpetri' ) )
            // InternalNET.g:529:1: ( 'elementpetri' )
            {
            // InternalNET.g:529:1: ( 'elementpetri' )
            // InternalNET.g:530:2: 'elementpetri'
            {
             before(grammarAccess.getReseauAccess().getElementpetriKeyword_5()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getElementpetriKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__5__Impl"


    // $ANTLR start "rule__Reseau__Group__6"
    // InternalNET.g:539:1: rule__Reseau__Group__6 : rule__Reseau__Group__6__Impl rule__Reseau__Group__7 ;
    public final void rule__Reseau__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:543:1: ( rule__Reseau__Group__6__Impl rule__Reseau__Group__7 )
            // InternalNET.g:544:2: rule__Reseau__Group__6__Impl rule__Reseau__Group__7
            {
            pushFollow(FOLLOW_7);
            rule__Reseau__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__6"


    // $ANTLR start "rule__Reseau__Group__6__Impl"
    // InternalNET.g:551:1: rule__Reseau__Group__6__Impl : ( '{' ) ;
    public final void rule__Reseau__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:555:1: ( ( '{' ) )
            // InternalNET.g:556:1: ( '{' )
            {
            // InternalNET.g:556:1: ( '{' )
            // InternalNET.g:557:2: '{'
            {
             before(grammarAccess.getReseauAccess().getLeftCurlyBracketKeyword_6()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getLeftCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__6__Impl"


    // $ANTLR start "rule__Reseau__Group__7"
    // InternalNET.g:566:1: rule__Reseau__Group__7 : rule__Reseau__Group__7__Impl rule__Reseau__Group__8 ;
    public final void rule__Reseau__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:570:1: ( rule__Reseau__Group__7__Impl rule__Reseau__Group__8 )
            // InternalNET.g:571:2: rule__Reseau__Group__7__Impl rule__Reseau__Group__8
            {
            pushFollow(FOLLOW_8);
            rule__Reseau__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__7"


    // $ANTLR start "rule__Reseau__Group__7__Impl"
    // InternalNET.g:578:1: rule__Reseau__Group__7__Impl : ( ( rule__Reseau__ElementpetriAssignment_7 ) ) ;
    public final void rule__Reseau__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:582:1: ( ( ( rule__Reseau__ElementpetriAssignment_7 ) ) )
            // InternalNET.g:583:1: ( ( rule__Reseau__ElementpetriAssignment_7 ) )
            {
            // InternalNET.g:583:1: ( ( rule__Reseau__ElementpetriAssignment_7 ) )
            // InternalNET.g:584:2: ( rule__Reseau__ElementpetriAssignment_7 )
            {
             before(grammarAccess.getReseauAccess().getElementpetriAssignment_7()); 
            // InternalNET.g:585:2: ( rule__Reseau__ElementpetriAssignment_7 )
            // InternalNET.g:585:3: rule__Reseau__ElementpetriAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Reseau__ElementpetriAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getReseauAccess().getElementpetriAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__7__Impl"


    // $ANTLR start "rule__Reseau__Group__8"
    // InternalNET.g:593:1: rule__Reseau__Group__8 : rule__Reseau__Group__8__Impl rule__Reseau__Group__9 ;
    public final void rule__Reseau__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:597:1: ( rule__Reseau__Group__8__Impl rule__Reseau__Group__9 )
            // InternalNET.g:598:2: rule__Reseau__Group__8__Impl rule__Reseau__Group__9
            {
            pushFollow(FOLLOW_8);
            rule__Reseau__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__8"


    // $ANTLR start "rule__Reseau__Group__8__Impl"
    // InternalNET.g:605:1: rule__Reseau__Group__8__Impl : ( ( rule__Reseau__Group_8__0 )* ) ;
    public final void rule__Reseau__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:609:1: ( ( ( rule__Reseau__Group_8__0 )* ) )
            // InternalNET.g:610:1: ( ( rule__Reseau__Group_8__0 )* )
            {
            // InternalNET.g:610:1: ( ( rule__Reseau__Group_8__0 )* )
            // InternalNET.g:611:2: ( rule__Reseau__Group_8__0 )*
            {
             before(grammarAccess.getReseauAccess().getGroup_8()); 
            // InternalNET.g:612:2: ( rule__Reseau__Group_8__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==19) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalNET.g:612:3: rule__Reseau__Group_8__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Reseau__Group_8__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getReseauAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__8__Impl"


    // $ANTLR start "rule__Reseau__Group__9"
    // InternalNET.g:620:1: rule__Reseau__Group__9 : rule__Reseau__Group__9__Impl rule__Reseau__Group__10 ;
    public final void rule__Reseau__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:624:1: ( rule__Reseau__Group__9__Impl rule__Reseau__Group__10 )
            // InternalNET.g:625:2: rule__Reseau__Group__9__Impl rule__Reseau__Group__10
            {
            pushFollow(FOLLOW_10);
            rule__Reseau__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__9"


    // $ANTLR start "rule__Reseau__Group__9__Impl"
    // InternalNET.g:632:1: rule__Reseau__Group__9__Impl : ( '}' ) ;
    public final void rule__Reseau__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:636:1: ( ( '}' ) )
            // InternalNET.g:637:1: ( '}' )
            {
            // InternalNET.g:637:1: ( '}' )
            // InternalNET.g:638:2: '}'
            {
             before(grammarAccess.getReseauAccess().getRightCurlyBracketKeyword_9()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__9__Impl"


    // $ANTLR start "rule__Reseau__Group__10"
    // InternalNET.g:647:1: rule__Reseau__Group__10 : rule__Reseau__Group__10__Impl ;
    public final void rule__Reseau__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:651:1: ( rule__Reseau__Group__10__Impl )
            // InternalNET.g:652:2: rule__Reseau__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Reseau__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__10"


    // $ANTLR start "rule__Reseau__Group__10__Impl"
    // InternalNET.g:658:1: rule__Reseau__Group__10__Impl : ( '}' ) ;
    public final void rule__Reseau__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:662:1: ( ( '}' ) )
            // InternalNET.g:663:1: ( '}' )
            {
            // InternalNET.g:663:1: ( '}' )
            // InternalNET.g:664:2: '}'
            {
             before(grammarAccess.getReseauAccess().getRightCurlyBracketKeyword_10()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getRightCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group__10__Impl"


    // $ANTLR start "rule__Reseau__Group_4__0"
    // InternalNET.g:674:1: rule__Reseau__Group_4__0 : rule__Reseau__Group_4__0__Impl rule__Reseau__Group_4__1 ;
    public final void rule__Reseau__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:678:1: ( rule__Reseau__Group_4__0__Impl rule__Reseau__Group_4__1 )
            // InternalNET.g:679:2: rule__Reseau__Group_4__0__Impl rule__Reseau__Group_4__1
            {
            pushFollow(FOLLOW_3);
            rule__Reseau__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4__0"


    // $ANTLR start "rule__Reseau__Group_4__0__Impl"
    // InternalNET.g:686:1: rule__Reseau__Group_4__0__Impl : ( 'arc' ) ;
    public final void rule__Reseau__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:690:1: ( ( 'arc' ) )
            // InternalNET.g:691:1: ( 'arc' )
            {
            // InternalNET.g:691:1: ( 'arc' )
            // InternalNET.g:692:2: 'arc'
            {
             before(grammarAccess.getReseauAccess().getArcKeyword_4_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getArcKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4__0__Impl"


    // $ANTLR start "rule__Reseau__Group_4__1"
    // InternalNET.g:701:1: rule__Reseau__Group_4__1 : rule__Reseau__Group_4__1__Impl rule__Reseau__Group_4__2 ;
    public final void rule__Reseau__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:705:1: ( rule__Reseau__Group_4__1__Impl rule__Reseau__Group_4__2 )
            // InternalNET.g:706:2: rule__Reseau__Group_4__1__Impl rule__Reseau__Group_4__2
            {
            pushFollow(FOLLOW_11);
            rule__Reseau__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4__1"


    // $ANTLR start "rule__Reseau__Group_4__1__Impl"
    // InternalNET.g:713:1: rule__Reseau__Group_4__1__Impl : ( '{' ) ;
    public final void rule__Reseau__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:717:1: ( ( '{' ) )
            // InternalNET.g:718:1: ( '{' )
            {
            // InternalNET.g:718:1: ( '{' )
            // InternalNET.g:719:2: '{'
            {
             before(grammarAccess.getReseauAccess().getLeftCurlyBracketKeyword_4_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getLeftCurlyBracketKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4__1__Impl"


    // $ANTLR start "rule__Reseau__Group_4__2"
    // InternalNET.g:728:1: rule__Reseau__Group_4__2 : rule__Reseau__Group_4__2__Impl rule__Reseau__Group_4__3 ;
    public final void rule__Reseau__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:732:1: ( rule__Reseau__Group_4__2__Impl rule__Reseau__Group_4__3 )
            // InternalNET.g:733:2: rule__Reseau__Group_4__2__Impl rule__Reseau__Group_4__3
            {
            pushFollow(FOLLOW_8);
            rule__Reseau__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4__2"


    // $ANTLR start "rule__Reseau__Group_4__2__Impl"
    // InternalNET.g:740:1: rule__Reseau__Group_4__2__Impl : ( ( rule__Reseau__ArcAssignment_4_2 ) ) ;
    public final void rule__Reseau__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:744:1: ( ( ( rule__Reseau__ArcAssignment_4_2 ) ) )
            // InternalNET.g:745:1: ( ( rule__Reseau__ArcAssignment_4_2 ) )
            {
            // InternalNET.g:745:1: ( ( rule__Reseau__ArcAssignment_4_2 ) )
            // InternalNET.g:746:2: ( rule__Reseau__ArcAssignment_4_2 )
            {
             before(grammarAccess.getReseauAccess().getArcAssignment_4_2()); 
            // InternalNET.g:747:2: ( rule__Reseau__ArcAssignment_4_2 )
            // InternalNET.g:747:3: rule__Reseau__ArcAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__Reseau__ArcAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getReseauAccess().getArcAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4__2__Impl"


    // $ANTLR start "rule__Reseau__Group_4__3"
    // InternalNET.g:755:1: rule__Reseau__Group_4__3 : rule__Reseau__Group_4__3__Impl rule__Reseau__Group_4__4 ;
    public final void rule__Reseau__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:759:1: ( rule__Reseau__Group_4__3__Impl rule__Reseau__Group_4__4 )
            // InternalNET.g:760:2: rule__Reseau__Group_4__3__Impl rule__Reseau__Group_4__4
            {
            pushFollow(FOLLOW_8);
            rule__Reseau__Group_4__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group_4__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4__3"


    // $ANTLR start "rule__Reseau__Group_4__3__Impl"
    // InternalNET.g:767:1: rule__Reseau__Group_4__3__Impl : ( ( rule__Reseau__Group_4_3__0 )* ) ;
    public final void rule__Reseau__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:771:1: ( ( ( rule__Reseau__Group_4_3__0 )* ) )
            // InternalNET.g:772:1: ( ( rule__Reseau__Group_4_3__0 )* )
            {
            // InternalNET.g:772:1: ( ( rule__Reseau__Group_4_3__0 )* )
            // InternalNET.g:773:2: ( rule__Reseau__Group_4_3__0 )*
            {
             before(grammarAccess.getReseauAccess().getGroup_4_3()); 
            // InternalNET.g:774:2: ( rule__Reseau__Group_4_3__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==19) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalNET.g:774:3: rule__Reseau__Group_4_3__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Reseau__Group_4_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getReseauAccess().getGroup_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4__3__Impl"


    // $ANTLR start "rule__Reseau__Group_4__4"
    // InternalNET.g:782:1: rule__Reseau__Group_4__4 : rule__Reseau__Group_4__4__Impl ;
    public final void rule__Reseau__Group_4__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:786:1: ( rule__Reseau__Group_4__4__Impl )
            // InternalNET.g:787:2: rule__Reseau__Group_4__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Reseau__Group_4__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4__4"


    // $ANTLR start "rule__Reseau__Group_4__4__Impl"
    // InternalNET.g:793:1: rule__Reseau__Group_4__4__Impl : ( '}' ) ;
    public final void rule__Reseau__Group_4__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:797:1: ( ( '}' ) )
            // InternalNET.g:798:1: ( '}' )
            {
            // InternalNET.g:798:1: ( '}' )
            // InternalNET.g:799:2: '}'
            {
             before(grammarAccess.getReseauAccess().getRightCurlyBracketKeyword_4_4()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getRightCurlyBracketKeyword_4_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4__4__Impl"


    // $ANTLR start "rule__Reseau__Group_4_3__0"
    // InternalNET.g:809:1: rule__Reseau__Group_4_3__0 : rule__Reseau__Group_4_3__0__Impl rule__Reseau__Group_4_3__1 ;
    public final void rule__Reseau__Group_4_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:813:1: ( rule__Reseau__Group_4_3__0__Impl rule__Reseau__Group_4_3__1 )
            // InternalNET.g:814:2: rule__Reseau__Group_4_3__0__Impl rule__Reseau__Group_4_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Reseau__Group_4_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group_4_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4_3__0"


    // $ANTLR start "rule__Reseau__Group_4_3__0__Impl"
    // InternalNET.g:821:1: rule__Reseau__Group_4_3__0__Impl : ( ',' ) ;
    public final void rule__Reseau__Group_4_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:825:1: ( ( ',' ) )
            // InternalNET.g:826:1: ( ',' )
            {
            // InternalNET.g:826:1: ( ',' )
            // InternalNET.g:827:2: ','
            {
             before(grammarAccess.getReseauAccess().getCommaKeyword_4_3_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getCommaKeyword_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4_3__0__Impl"


    // $ANTLR start "rule__Reseau__Group_4_3__1"
    // InternalNET.g:836:1: rule__Reseau__Group_4_3__1 : rule__Reseau__Group_4_3__1__Impl ;
    public final void rule__Reseau__Group_4_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:840:1: ( rule__Reseau__Group_4_3__1__Impl )
            // InternalNET.g:841:2: rule__Reseau__Group_4_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Reseau__Group_4_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4_3__1"


    // $ANTLR start "rule__Reseau__Group_4_3__1__Impl"
    // InternalNET.g:847:1: rule__Reseau__Group_4_3__1__Impl : ( ( rule__Reseau__ArcAssignment_4_3_1 ) ) ;
    public final void rule__Reseau__Group_4_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:851:1: ( ( ( rule__Reseau__ArcAssignment_4_3_1 ) ) )
            // InternalNET.g:852:1: ( ( rule__Reseau__ArcAssignment_4_3_1 ) )
            {
            // InternalNET.g:852:1: ( ( rule__Reseau__ArcAssignment_4_3_1 ) )
            // InternalNET.g:853:2: ( rule__Reseau__ArcAssignment_4_3_1 )
            {
             before(grammarAccess.getReseauAccess().getArcAssignment_4_3_1()); 
            // InternalNET.g:854:2: ( rule__Reseau__ArcAssignment_4_3_1 )
            // InternalNET.g:854:3: rule__Reseau__ArcAssignment_4_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Reseau__ArcAssignment_4_3_1();

            state._fsp--;


            }

             after(grammarAccess.getReseauAccess().getArcAssignment_4_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_4_3__1__Impl"


    // $ANTLR start "rule__Reseau__Group_8__0"
    // InternalNET.g:863:1: rule__Reseau__Group_8__0 : rule__Reseau__Group_8__0__Impl rule__Reseau__Group_8__1 ;
    public final void rule__Reseau__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:867:1: ( rule__Reseau__Group_8__0__Impl rule__Reseau__Group_8__1 )
            // InternalNET.g:868:2: rule__Reseau__Group_8__0__Impl rule__Reseau__Group_8__1
            {
            pushFollow(FOLLOW_7);
            rule__Reseau__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Reseau__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_8__0"


    // $ANTLR start "rule__Reseau__Group_8__0__Impl"
    // InternalNET.g:875:1: rule__Reseau__Group_8__0__Impl : ( ',' ) ;
    public final void rule__Reseau__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:879:1: ( ( ',' ) )
            // InternalNET.g:880:1: ( ',' )
            {
            // InternalNET.g:880:1: ( ',' )
            // InternalNET.g:881:2: ','
            {
             before(grammarAccess.getReseauAccess().getCommaKeyword_8_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getReseauAccess().getCommaKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_8__0__Impl"


    // $ANTLR start "rule__Reseau__Group_8__1"
    // InternalNET.g:890:1: rule__Reseau__Group_8__1 : rule__Reseau__Group_8__1__Impl ;
    public final void rule__Reseau__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:894:1: ( rule__Reseau__Group_8__1__Impl )
            // InternalNET.g:895:2: rule__Reseau__Group_8__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Reseau__Group_8__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_8__1"


    // $ANTLR start "rule__Reseau__Group_8__1__Impl"
    // InternalNET.g:901:1: rule__Reseau__Group_8__1__Impl : ( ( rule__Reseau__ElementpetriAssignment_8_1 ) ) ;
    public final void rule__Reseau__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:905:1: ( ( ( rule__Reseau__ElementpetriAssignment_8_1 ) ) )
            // InternalNET.g:906:1: ( ( rule__Reseau__ElementpetriAssignment_8_1 ) )
            {
            // InternalNET.g:906:1: ( ( rule__Reseau__ElementpetriAssignment_8_1 ) )
            // InternalNET.g:907:2: ( rule__Reseau__ElementpetriAssignment_8_1 )
            {
             before(grammarAccess.getReseauAccess().getElementpetriAssignment_8_1()); 
            // InternalNET.g:908:2: ( rule__Reseau__ElementpetriAssignment_8_1 )
            // InternalNET.g:908:3: rule__Reseau__ElementpetriAssignment_8_1
            {
            pushFollow(FOLLOW_2);
            rule__Reseau__ElementpetriAssignment_8_1();

            state._fsp--;


            }

             after(grammarAccess.getReseauAccess().getElementpetriAssignment_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__Group_8__1__Impl"


    // $ANTLR start "rule__EInt__Group__0"
    // InternalNET.g:917:1: rule__EInt__Group__0 : rule__EInt__Group__0__Impl rule__EInt__Group__1 ;
    public final void rule__EInt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:921:1: ( rule__EInt__Group__0__Impl rule__EInt__Group__1 )
            // InternalNET.g:922:2: rule__EInt__Group__0__Impl rule__EInt__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__EInt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EInt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0"


    // $ANTLR start "rule__EInt__Group__0__Impl"
    // InternalNET.g:929:1: rule__EInt__Group__0__Impl : ( ( '-' )? ) ;
    public final void rule__EInt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:933:1: ( ( ( '-' )? ) )
            // InternalNET.g:934:1: ( ( '-' )? )
            {
            // InternalNET.g:934:1: ( ( '-' )? )
            // InternalNET.g:935:2: ( '-' )?
            {
             before(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 
            // InternalNET.g:936:2: ( '-' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==20) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalNET.g:936:3: '-'
                    {
                    match(input,20,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0__Impl"


    // $ANTLR start "rule__EInt__Group__1"
    // InternalNET.g:944:1: rule__EInt__Group__1 : rule__EInt__Group__1__Impl ;
    public final void rule__EInt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:948:1: ( rule__EInt__Group__1__Impl )
            // InternalNET.g:949:2: rule__EInt__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EInt__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1"


    // $ANTLR start "rule__EInt__Group__1__Impl"
    // InternalNET.g:955:1: rule__EInt__Group__1__Impl : ( RULE_INT ) ;
    public final void rule__EInt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:959:1: ( ( RULE_INT ) )
            // InternalNET.g:960:1: ( RULE_INT )
            {
            // InternalNET.g:960:1: ( RULE_INT )
            // InternalNET.g:961:2: RULE_INT
            {
             before(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1__Impl"


    // $ANTLR start "rule__ArcPT__Group__0"
    // InternalNET.g:971:1: rule__ArcPT__Group__0 : rule__ArcPT__Group__0__Impl rule__ArcPT__Group__1 ;
    public final void rule__ArcPT__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:975:1: ( rule__ArcPT__Group__0__Impl rule__ArcPT__Group__1 )
            // InternalNET.g:976:2: rule__ArcPT__Group__0__Impl rule__ArcPT__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__ArcPT__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__0"


    // $ANTLR start "rule__ArcPT__Group__0__Impl"
    // InternalNET.g:983:1: rule__ArcPT__Group__0__Impl : ( 'ArcPT' ) ;
    public final void rule__ArcPT__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:987:1: ( ( 'ArcPT' ) )
            // InternalNET.g:988:1: ( 'ArcPT' )
            {
            // InternalNET.g:988:1: ( 'ArcPT' )
            // InternalNET.g:989:2: 'ArcPT'
            {
             before(grammarAccess.getArcPTAccess().getArcPTKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getArcPTAccess().getArcPTKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__0__Impl"


    // $ANTLR start "rule__ArcPT__Group__1"
    // InternalNET.g:998:1: rule__ArcPT__Group__1 : rule__ArcPT__Group__1__Impl rule__ArcPT__Group__2 ;
    public final void rule__ArcPT__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1002:1: ( rule__ArcPT__Group__1__Impl rule__ArcPT__Group__2 )
            // InternalNET.g:1003:2: rule__ArcPT__Group__1__Impl rule__ArcPT__Group__2
            {
            pushFollow(FOLLOW_13);
            rule__ArcPT__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__1"


    // $ANTLR start "rule__ArcPT__Group__1__Impl"
    // InternalNET.g:1010:1: rule__ArcPT__Group__1__Impl : ( '{' ) ;
    public final void rule__ArcPT__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1014:1: ( ( '{' ) )
            // InternalNET.g:1015:1: ( '{' )
            {
            // InternalNET.g:1015:1: ( '{' )
            // InternalNET.g:1016:2: '{'
            {
             before(grammarAccess.getArcPTAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getArcPTAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__1__Impl"


    // $ANTLR start "rule__ArcPT__Group__2"
    // InternalNET.g:1025:1: rule__ArcPT__Group__2 : rule__ArcPT__Group__2__Impl rule__ArcPT__Group__3 ;
    public final void rule__ArcPT__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1029:1: ( rule__ArcPT__Group__2__Impl rule__ArcPT__Group__3 )
            // InternalNET.g:1030:2: rule__ArcPT__Group__2__Impl rule__ArcPT__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__ArcPT__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__2"


    // $ANTLR start "rule__ArcPT__Group__2__Impl"
    // InternalNET.g:1037:1: rule__ArcPT__Group__2__Impl : ( 'poids' ) ;
    public final void rule__ArcPT__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1041:1: ( ( 'poids' ) )
            // InternalNET.g:1042:1: ( 'poids' )
            {
            // InternalNET.g:1042:1: ( 'poids' )
            // InternalNET.g:1043:2: 'poids'
            {
             before(grammarAccess.getArcPTAccess().getPoidsKeyword_2()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getArcPTAccess().getPoidsKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__2__Impl"


    // $ANTLR start "rule__ArcPT__Group__3"
    // InternalNET.g:1052:1: rule__ArcPT__Group__3 : rule__ArcPT__Group__3__Impl rule__ArcPT__Group__4 ;
    public final void rule__ArcPT__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1056:1: ( rule__ArcPT__Group__3__Impl rule__ArcPT__Group__4 )
            // InternalNET.g:1057:2: rule__ArcPT__Group__3__Impl rule__ArcPT__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__ArcPT__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__3"


    // $ANTLR start "rule__ArcPT__Group__3__Impl"
    // InternalNET.g:1064:1: rule__ArcPT__Group__3__Impl : ( ( rule__ArcPT__PoidsAssignment_3 ) ) ;
    public final void rule__ArcPT__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1068:1: ( ( ( rule__ArcPT__PoidsAssignment_3 ) ) )
            // InternalNET.g:1069:1: ( ( rule__ArcPT__PoidsAssignment_3 ) )
            {
            // InternalNET.g:1069:1: ( ( rule__ArcPT__PoidsAssignment_3 ) )
            // InternalNET.g:1070:2: ( rule__ArcPT__PoidsAssignment_3 )
            {
             before(grammarAccess.getArcPTAccess().getPoidsAssignment_3()); 
            // InternalNET.g:1071:2: ( rule__ArcPT__PoidsAssignment_3 )
            // InternalNET.g:1071:3: rule__ArcPT__PoidsAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ArcPT__PoidsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getArcPTAccess().getPoidsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__3__Impl"


    // $ANTLR start "rule__ArcPT__Group__4"
    // InternalNET.g:1079:1: rule__ArcPT__Group__4 : rule__ArcPT__Group__4__Impl rule__ArcPT__Group__5 ;
    public final void rule__ArcPT__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1083:1: ( rule__ArcPT__Group__4__Impl rule__ArcPT__Group__5 )
            // InternalNET.g:1084:2: rule__ArcPT__Group__4__Impl rule__ArcPT__Group__5
            {
            pushFollow(FOLLOW_16);
            rule__ArcPT__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__4"


    // $ANTLR start "rule__ArcPT__Group__4__Impl"
    // InternalNET.g:1091:1: rule__ArcPT__Group__4__Impl : ( 'type' ) ;
    public final void rule__ArcPT__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1095:1: ( ( 'type' ) )
            // InternalNET.g:1096:1: ( 'type' )
            {
            // InternalNET.g:1096:1: ( 'type' )
            // InternalNET.g:1097:2: 'type'
            {
             before(grammarAccess.getArcPTAccess().getTypeKeyword_4()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getArcPTAccess().getTypeKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__4__Impl"


    // $ANTLR start "rule__ArcPT__Group__5"
    // InternalNET.g:1106:1: rule__ArcPT__Group__5 : rule__ArcPT__Group__5__Impl rule__ArcPT__Group__6 ;
    public final void rule__ArcPT__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1110:1: ( rule__ArcPT__Group__5__Impl rule__ArcPT__Group__6 )
            // InternalNET.g:1111:2: rule__ArcPT__Group__5__Impl rule__ArcPT__Group__6
            {
            pushFollow(FOLLOW_17);
            rule__ArcPT__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__5"


    // $ANTLR start "rule__ArcPT__Group__5__Impl"
    // InternalNET.g:1118:1: rule__ArcPT__Group__5__Impl : ( ( rule__ArcPT__TypeAssignment_5 ) ) ;
    public final void rule__ArcPT__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1122:1: ( ( ( rule__ArcPT__TypeAssignment_5 ) ) )
            // InternalNET.g:1123:1: ( ( rule__ArcPT__TypeAssignment_5 ) )
            {
            // InternalNET.g:1123:1: ( ( rule__ArcPT__TypeAssignment_5 ) )
            // InternalNET.g:1124:2: ( rule__ArcPT__TypeAssignment_5 )
            {
             before(grammarAccess.getArcPTAccess().getTypeAssignment_5()); 
            // InternalNET.g:1125:2: ( rule__ArcPT__TypeAssignment_5 )
            // InternalNET.g:1125:3: rule__ArcPT__TypeAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__ArcPT__TypeAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getArcPTAccess().getTypeAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__5__Impl"


    // $ANTLR start "rule__ArcPT__Group__6"
    // InternalNET.g:1133:1: rule__ArcPT__Group__6 : rule__ArcPT__Group__6__Impl rule__ArcPT__Group__7 ;
    public final void rule__ArcPT__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1137:1: ( rule__ArcPT__Group__6__Impl rule__ArcPT__Group__7 )
            // InternalNET.g:1138:2: rule__ArcPT__Group__6__Impl rule__ArcPT__Group__7
            {
            pushFollow(FOLLOW_5);
            rule__ArcPT__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__6"


    // $ANTLR start "rule__ArcPT__Group__6__Impl"
    // InternalNET.g:1145:1: rule__ArcPT__Group__6__Impl : ( 'transition' ) ;
    public final void rule__ArcPT__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1149:1: ( ( 'transition' ) )
            // InternalNET.g:1150:1: ( 'transition' )
            {
            // InternalNET.g:1150:1: ( 'transition' )
            // InternalNET.g:1151:2: 'transition'
            {
             before(grammarAccess.getArcPTAccess().getTransitionKeyword_6()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getArcPTAccess().getTransitionKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__6__Impl"


    // $ANTLR start "rule__ArcPT__Group__7"
    // InternalNET.g:1160:1: rule__ArcPT__Group__7 : rule__ArcPT__Group__7__Impl rule__ArcPT__Group__8 ;
    public final void rule__ArcPT__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1164:1: ( rule__ArcPT__Group__7__Impl rule__ArcPT__Group__8 )
            // InternalNET.g:1165:2: rule__ArcPT__Group__7__Impl rule__ArcPT__Group__8
            {
            pushFollow(FOLLOW_18);
            rule__ArcPT__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__7"


    // $ANTLR start "rule__ArcPT__Group__7__Impl"
    // InternalNET.g:1172:1: rule__ArcPT__Group__7__Impl : ( ( rule__ArcPT__TransitionAssignment_7 ) ) ;
    public final void rule__ArcPT__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1176:1: ( ( ( rule__ArcPT__TransitionAssignment_7 ) ) )
            // InternalNET.g:1177:1: ( ( rule__ArcPT__TransitionAssignment_7 ) )
            {
            // InternalNET.g:1177:1: ( ( rule__ArcPT__TransitionAssignment_7 ) )
            // InternalNET.g:1178:2: ( rule__ArcPT__TransitionAssignment_7 )
            {
             before(grammarAccess.getArcPTAccess().getTransitionAssignment_7()); 
            // InternalNET.g:1179:2: ( rule__ArcPT__TransitionAssignment_7 )
            // InternalNET.g:1179:3: rule__ArcPT__TransitionAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__ArcPT__TransitionAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getArcPTAccess().getTransitionAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__7__Impl"


    // $ANTLR start "rule__ArcPT__Group__8"
    // InternalNET.g:1187:1: rule__ArcPT__Group__8 : rule__ArcPT__Group__8__Impl rule__ArcPT__Group__9 ;
    public final void rule__ArcPT__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1191:1: ( rule__ArcPT__Group__8__Impl rule__ArcPT__Group__9 )
            // InternalNET.g:1192:2: rule__ArcPT__Group__8__Impl rule__ArcPT__Group__9
            {
            pushFollow(FOLLOW_5);
            rule__ArcPT__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__8"


    // $ANTLR start "rule__ArcPT__Group__8__Impl"
    // InternalNET.g:1199:1: rule__ArcPT__Group__8__Impl : ( 'place' ) ;
    public final void rule__ArcPT__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1203:1: ( ( 'place' ) )
            // InternalNET.g:1204:1: ( 'place' )
            {
            // InternalNET.g:1204:1: ( 'place' )
            // InternalNET.g:1205:2: 'place'
            {
             before(grammarAccess.getArcPTAccess().getPlaceKeyword_8()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getArcPTAccess().getPlaceKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__8__Impl"


    // $ANTLR start "rule__ArcPT__Group__9"
    // InternalNET.g:1214:1: rule__ArcPT__Group__9 : rule__ArcPT__Group__9__Impl rule__ArcPT__Group__10 ;
    public final void rule__ArcPT__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1218:1: ( rule__ArcPT__Group__9__Impl rule__ArcPT__Group__10 )
            // InternalNET.g:1219:2: rule__ArcPT__Group__9__Impl rule__ArcPT__Group__10
            {
            pushFollow(FOLLOW_10);
            rule__ArcPT__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__9"


    // $ANTLR start "rule__ArcPT__Group__9__Impl"
    // InternalNET.g:1226:1: rule__ArcPT__Group__9__Impl : ( ( rule__ArcPT__PlaceAssignment_9 ) ) ;
    public final void rule__ArcPT__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1230:1: ( ( ( rule__ArcPT__PlaceAssignment_9 ) ) )
            // InternalNET.g:1231:1: ( ( rule__ArcPT__PlaceAssignment_9 ) )
            {
            // InternalNET.g:1231:1: ( ( rule__ArcPT__PlaceAssignment_9 ) )
            // InternalNET.g:1232:2: ( rule__ArcPT__PlaceAssignment_9 )
            {
             before(grammarAccess.getArcPTAccess().getPlaceAssignment_9()); 
            // InternalNET.g:1233:2: ( rule__ArcPT__PlaceAssignment_9 )
            // InternalNET.g:1233:3: rule__ArcPT__PlaceAssignment_9
            {
            pushFollow(FOLLOW_2);
            rule__ArcPT__PlaceAssignment_9();

            state._fsp--;


            }

             after(grammarAccess.getArcPTAccess().getPlaceAssignment_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__9__Impl"


    // $ANTLR start "rule__ArcPT__Group__10"
    // InternalNET.g:1241:1: rule__ArcPT__Group__10 : rule__ArcPT__Group__10__Impl ;
    public final void rule__ArcPT__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1245:1: ( rule__ArcPT__Group__10__Impl )
            // InternalNET.g:1246:2: rule__ArcPT__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ArcPT__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__10"


    // $ANTLR start "rule__ArcPT__Group__10__Impl"
    // InternalNET.g:1252:1: rule__ArcPT__Group__10__Impl : ( '}' ) ;
    public final void rule__ArcPT__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1256:1: ( ( '}' ) )
            // InternalNET.g:1257:1: ( '}' )
            {
            // InternalNET.g:1257:1: ( '}' )
            // InternalNET.g:1258:2: '}'
            {
             before(grammarAccess.getArcPTAccess().getRightCurlyBracketKeyword_10()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getArcPTAccess().getRightCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__Group__10__Impl"


    // $ANTLR start "rule__ArcTP__Group__0"
    // InternalNET.g:1268:1: rule__ArcTP__Group__0 : rule__ArcTP__Group__0__Impl rule__ArcTP__Group__1 ;
    public final void rule__ArcTP__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1272:1: ( rule__ArcTP__Group__0__Impl rule__ArcTP__Group__1 )
            // InternalNET.g:1273:2: rule__ArcTP__Group__0__Impl rule__ArcTP__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__ArcTP__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcTP__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__0"


    // $ANTLR start "rule__ArcTP__Group__0__Impl"
    // InternalNET.g:1280:1: rule__ArcTP__Group__0__Impl : ( 'ArcTP' ) ;
    public final void rule__ArcTP__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1284:1: ( ( 'ArcTP' ) )
            // InternalNET.g:1285:1: ( 'ArcTP' )
            {
            // InternalNET.g:1285:1: ( 'ArcTP' )
            // InternalNET.g:1286:2: 'ArcTP'
            {
             before(grammarAccess.getArcTPAccess().getArcTPKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getArcTPAccess().getArcTPKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__0__Impl"


    // $ANTLR start "rule__ArcTP__Group__1"
    // InternalNET.g:1295:1: rule__ArcTP__Group__1 : rule__ArcTP__Group__1__Impl rule__ArcTP__Group__2 ;
    public final void rule__ArcTP__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1299:1: ( rule__ArcTP__Group__1__Impl rule__ArcTP__Group__2 )
            // InternalNET.g:1300:2: rule__ArcTP__Group__1__Impl rule__ArcTP__Group__2
            {
            pushFollow(FOLLOW_13);
            rule__ArcTP__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcTP__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__1"


    // $ANTLR start "rule__ArcTP__Group__1__Impl"
    // InternalNET.g:1307:1: rule__ArcTP__Group__1__Impl : ( '{' ) ;
    public final void rule__ArcTP__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1311:1: ( ( '{' ) )
            // InternalNET.g:1312:1: ( '{' )
            {
            // InternalNET.g:1312:1: ( '{' )
            // InternalNET.g:1313:2: '{'
            {
             before(grammarAccess.getArcTPAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getArcTPAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__1__Impl"


    // $ANTLR start "rule__ArcTP__Group__2"
    // InternalNET.g:1322:1: rule__ArcTP__Group__2 : rule__ArcTP__Group__2__Impl rule__ArcTP__Group__3 ;
    public final void rule__ArcTP__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1326:1: ( rule__ArcTP__Group__2__Impl rule__ArcTP__Group__3 )
            // InternalNET.g:1327:2: rule__ArcTP__Group__2__Impl rule__ArcTP__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__ArcTP__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcTP__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__2"


    // $ANTLR start "rule__ArcTP__Group__2__Impl"
    // InternalNET.g:1334:1: rule__ArcTP__Group__2__Impl : ( 'poids' ) ;
    public final void rule__ArcTP__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1338:1: ( ( 'poids' ) )
            // InternalNET.g:1339:1: ( 'poids' )
            {
            // InternalNET.g:1339:1: ( 'poids' )
            // InternalNET.g:1340:2: 'poids'
            {
             before(grammarAccess.getArcTPAccess().getPoidsKeyword_2()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getArcTPAccess().getPoidsKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__2__Impl"


    // $ANTLR start "rule__ArcTP__Group__3"
    // InternalNET.g:1349:1: rule__ArcTP__Group__3 : rule__ArcTP__Group__3__Impl rule__ArcTP__Group__4 ;
    public final void rule__ArcTP__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1353:1: ( rule__ArcTP__Group__3__Impl rule__ArcTP__Group__4 )
            // InternalNET.g:1354:2: rule__ArcTP__Group__3__Impl rule__ArcTP__Group__4
            {
            pushFollow(FOLLOW_17);
            rule__ArcTP__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcTP__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__3"


    // $ANTLR start "rule__ArcTP__Group__3__Impl"
    // InternalNET.g:1361:1: rule__ArcTP__Group__3__Impl : ( ( rule__ArcTP__PoidsAssignment_3 ) ) ;
    public final void rule__ArcTP__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1365:1: ( ( ( rule__ArcTP__PoidsAssignment_3 ) ) )
            // InternalNET.g:1366:1: ( ( rule__ArcTP__PoidsAssignment_3 ) )
            {
            // InternalNET.g:1366:1: ( ( rule__ArcTP__PoidsAssignment_3 ) )
            // InternalNET.g:1367:2: ( rule__ArcTP__PoidsAssignment_3 )
            {
             before(grammarAccess.getArcTPAccess().getPoidsAssignment_3()); 
            // InternalNET.g:1368:2: ( rule__ArcTP__PoidsAssignment_3 )
            // InternalNET.g:1368:3: rule__ArcTP__PoidsAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ArcTP__PoidsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getArcTPAccess().getPoidsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__3__Impl"


    // $ANTLR start "rule__ArcTP__Group__4"
    // InternalNET.g:1376:1: rule__ArcTP__Group__4 : rule__ArcTP__Group__4__Impl rule__ArcTP__Group__5 ;
    public final void rule__ArcTP__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1380:1: ( rule__ArcTP__Group__4__Impl rule__ArcTP__Group__5 )
            // InternalNET.g:1381:2: rule__ArcTP__Group__4__Impl rule__ArcTP__Group__5
            {
            pushFollow(FOLLOW_5);
            rule__ArcTP__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcTP__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__4"


    // $ANTLR start "rule__ArcTP__Group__4__Impl"
    // InternalNET.g:1388:1: rule__ArcTP__Group__4__Impl : ( 'transition' ) ;
    public final void rule__ArcTP__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1392:1: ( ( 'transition' ) )
            // InternalNET.g:1393:1: ( 'transition' )
            {
            // InternalNET.g:1393:1: ( 'transition' )
            // InternalNET.g:1394:2: 'transition'
            {
             before(grammarAccess.getArcTPAccess().getTransitionKeyword_4()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getArcTPAccess().getTransitionKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__4__Impl"


    // $ANTLR start "rule__ArcTP__Group__5"
    // InternalNET.g:1403:1: rule__ArcTP__Group__5 : rule__ArcTP__Group__5__Impl rule__ArcTP__Group__6 ;
    public final void rule__ArcTP__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1407:1: ( rule__ArcTP__Group__5__Impl rule__ArcTP__Group__6 )
            // InternalNET.g:1408:2: rule__ArcTP__Group__5__Impl rule__ArcTP__Group__6
            {
            pushFollow(FOLLOW_18);
            rule__ArcTP__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcTP__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__5"


    // $ANTLR start "rule__ArcTP__Group__5__Impl"
    // InternalNET.g:1415:1: rule__ArcTP__Group__5__Impl : ( ( rule__ArcTP__TransitionAssignment_5 ) ) ;
    public final void rule__ArcTP__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1419:1: ( ( ( rule__ArcTP__TransitionAssignment_5 ) ) )
            // InternalNET.g:1420:1: ( ( rule__ArcTP__TransitionAssignment_5 ) )
            {
            // InternalNET.g:1420:1: ( ( rule__ArcTP__TransitionAssignment_5 ) )
            // InternalNET.g:1421:2: ( rule__ArcTP__TransitionAssignment_5 )
            {
             before(grammarAccess.getArcTPAccess().getTransitionAssignment_5()); 
            // InternalNET.g:1422:2: ( rule__ArcTP__TransitionAssignment_5 )
            // InternalNET.g:1422:3: rule__ArcTP__TransitionAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__ArcTP__TransitionAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getArcTPAccess().getTransitionAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__5__Impl"


    // $ANTLR start "rule__ArcTP__Group__6"
    // InternalNET.g:1430:1: rule__ArcTP__Group__6 : rule__ArcTP__Group__6__Impl rule__ArcTP__Group__7 ;
    public final void rule__ArcTP__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1434:1: ( rule__ArcTP__Group__6__Impl rule__ArcTP__Group__7 )
            // InternalNET.g:1435:2: rule__ArcTP__Group__6__Impl rule__ArcTP__Group__7
            {
            pushFollow(FOLLOW_5);
            rule__ArcTP__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcTP__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__6"


    // $ANTLR start "rule__ArcTP__Group__6__Impl"
    // InternalNET.g:1442:1: rule__ArcTP__Group__6__Impl : ( 'place' ) ;
    public final void rule__ArcTP__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1446:1: ( ( 'place' ) )
            // InternalNET.g:1447:1: ( 'place' )
            {
            // InternalNET.g:1447:1: ( 'place' )
            // InternalNET.g:1448:2: 'place'
            {
             before(grammarAccess.getArcTPAccess().getPlaceKeyword_6()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getArcTPAccess().getPlaceKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__6__Impl"


    // $ANTLR start "rule__ArcTP__Group__7"
    // InternalNET.g:1457:1: rule__ArcTP__Group__7 : rule__ArcTP__Group__7__Impl rule__ArcTP__Group__8 ;
    public final void rule__ArcTP__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1461:1: ( rule__ArcTP__Group__7__Impl rule__ArcTP__Group__8 )
            // InternalNET.g:1462:2: rule__ArcTP__Group__7__Impl rule__ArcTP__Group__8
            {
            pushFollow(FOLLOW_10);
            rule__ArcTP__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ArcTP__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__7"


    // $ANTLR start "rule__ArcTP__Group__7__Impl"
    // InternalNET.g:1469:1: rule__ArcTP__Group__7__Impl : ( ( rule__ArcTP__PlaceAssignment_7 ) ) ;
    public final void rule__ArcTP__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1473:1: ( ( ( rule__ArcTP__PlaceAssignment_7 ) ) )
            // InternalNET.g:1474:1: ( ( rule__ArcTP__PlaceAssignment_7 ) )
            {
            // InternalNET.g:1474:1: ( ( rule__ArcTP__PlaceAssignment_7 ) )
            // InternalNET.g:1475:2: ( rule__ArcTP__PlaceAssignment_7 )
            {
             before(grammarAccess.getArcTPAccess().getPlaceAssignment_7()); 
            // InternalNET.g:1476:2: ( rule__ArcTP__PlaceAssignment_7 )
            // InternalNET.g:1476:3: rule__ArcTP__PlaceAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__ArcTP__PlaceAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getArcTPAccess().getPlaceAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__7__Impl"


    // $ANTLR start "rule__ArcTP__Group__8"
    // InternalNET.g:1484:1: rule__ArcTP__Group__8 : rule__ArcTP__Group__8__Impl ;
    public final void rule__ArcTP__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1488:1: ( rule__ArcTP__Group__8__Impl )
            // InternalNET.g:1489:2: rule__ArcTP__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ArcTP__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__8"


    // $ANTLR start "rule__ArcTP__Group__8__Impl"
    // InternalNET.g:1495:1: rule__ArcTP__Group__8__Impl : ( '}' ) ;
    public final void rule__ArcTP__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1499:1: ( ( '}' ) )
            // InternalNET.g:1500:1: ( '}' )
            {
            // InternalNET.g:1500:1: ( '}' )
            // InternalNET.g:1501:2: '}'
            {
             before(grammarAccess.getArcTPAccess().getRightCurlyBracketKeyword_8()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getArcTPAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__Group__8__Impl"


    // $ANTLR start "rule__Transition__Group__0"
    // InternalNET.g:1511:1: rule__Transition__Group__0 : rule__Transition__Group__0__Impl rule__Transition__Group__1 ;
    public final void rule__Transition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1515:1: ( rule__Transition__Group__0__Impl rule__Transition__Group__1 )
            // InternalNET.g:1516:2: rule__Transition__Group__0__Impl rule__Transition__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Transition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0"


    // $ANTLR start "rule__Transition__Group__0__Impl"
    // InternalNET.g:1523:1: rule__Transition__Group__0__Impl : ( 'Transition' ) ;
    public final void rule__Transition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1527:1: ( ( 'Transition' ) )
            // InternalNET.g:1528:1: ( 'Transition' )
            {
            // InternalNET.g:1528:1: ( 'Transition' )
            // InternalNET.g:1529:2: 'Transition'
            {
             before(grammarAccess.getTransitionAccess().getTransitionKeyword_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getTransitionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0__Impl"


    // $ANTLR start "rule__Transition__Group__1"
    // InternalNET.g:1538:1: rule__Transition__Group__1 : rule__Transition__Group__1__Impl rule__Transition__Group__2 ;
    public final void rule__Transition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1542:1: ( rule__Transition__Group__1__Impl rule__Transition__Group__2 )
            // InternalNET.g:1543:2: rule__Transition__Group__1__Impl rule__Transition__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Transition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1"


    // $ANTLR start "rule__Transition__Group__1__Impl"
    // InternalNET.g:1550:1: rule__Transition__Group__1__Impl : ( '{' ) ;
    public final void rule__Transition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1554:1: ( ( '{' ) )
            // InternalNET.g:1555:1: ( '{' )
            {
            // InternalNET.g:1555:1: ( '{' )
            // InternalNET.g:1556:2: '{'
            {
             before(grammarAccess.getTransitionAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1__Impl"


    // $ANTLR start "rule__Transition__Group__2"
    // InternalNET.g:1565:1: rule__Transition__Group__2 : rule__Transition__Group__2__Impl rule__Transition__Group__3 ;
    public final void rule__Transition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1569:1: ( rule__Transition__Group__2__Impl rule__Transition__Group__3 )
            // InternalNET.g:1570:2: rule__Transition__Group__2__Impl rule__Transition__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Transition__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2"


    // $ANTLR start "rule__Transition__Group__2__Impl"
    // InternalNET.g:1577:1: rule__Transition__Group__2__Impl : ( 'nom' ) ;
    public final void rule__Transition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1581:1: ( ( 'nom' ) )
            // InternalNET.g:1582:1: ( 'nom' )
            {
            // InternalNET.g:1582:1: ( 'nom' )
            // InternalNET.g:1583:2: 'nom'
            {
             before(grammarAccess.getTransitionAccess().getNomKeyword_2()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getNomKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2__Impl"


    // $ANTLR start "rule__Transition__Group__3"
    // InternalNET.g:1592:1: rule__Transition__Group__3 : rule__Transition__Group__3__Impl rule__Transition__Group__4 ;
    public final void rule__Transition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1596:1: ( rule__Transition__Group__3__Impl rule__Transition__Group__4 )
            // InternalNET.g:1597:2: rule__Transition__Group__3__Impl rule__Transition__Group__4
            {
            pushFollow(FOLLOW_19);
            rule__Transition__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3"


    // $ANTLR start "rule__Transition__Group__3__Impl"
    // InternalNET.g:1604:1: rule__Transition__Group__3__Impl : ( ( rule__Transition__NomAssignment_3 ) ) ;
    public final void rule__Transition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1608:1: ( ( ( rule__Transition__NomAssignment_3 ) ) )
            // InternalNET.g:1609:1: ( ( rule__Transition__NomAssignment_3 ) )
            {
            // InternalNET.g:1609:1: ( ( rule__Transition__NomAssignment_3 ) )
            // InternalNET.g:1610:2: ( rule__Transition__NomAssignment_3 )
            {
             before(grammarAccess.getTransitionAccess().getNomAssignment_3()); 
            // InternalNET.g:1611:2: ( rule__Transition__NomAssignment_3 )
            // InternalNET.g:1611:3: rule__Transition__NomAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Transition__NomAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getNomAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3__Impl"


    // $ANTLR start "rule__Transition__Group__4"
    // InternalNET.g:1619:1: rule__Transition__Group__4 : rule__Transition__Group__4__Impl rule__Transition__Group__5 ;
    public final void rule__Transition__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1623:1: ( rule__Transition__Group__4__Impl rule__Transition__Group__5 )
            // InternalNET.g:1624:2: rule__Transition__Group__4__Impl rule__Transition__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__Transition__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4"


    // $ANTLR start "rule__Transition__Group__4__Impl"
    // InternalNET.g:1631:1: rule__Transition__Group__4__Impl : ( ( rule__Transition__Group_4__0 )? ) ;
    public final void rule__Transition__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1635:1: ( ( ( rule__Transition__Group_4__0 )? ) )
            // InternalNET.g:1636:1: ( ( rule__Transition__Group_4__0 )? )
            {
            // InternalNET.g:1636:1: ( ( rule__Transition__Group_4__0 )? )
            // InternalNET.g:1637:2: ( rule__Transition__Group_4__0 )?
            {
             before(grammarAccess.getTransitionAccess().getGroup_4()); 
            // InternalNET.g:1638:2: ( rule__Transition__Group_4__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==28) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalNET.g:1638:3: rule__Transition__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Transition__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTransitionAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4__Impl"


    // $ANTLR start "rule__Transition__Group__5"
    // InternalNET.g:1646:1: rule__Transition__Group__5 : rule__Transition__Group__5__Impl rule__Transition__Group__6 ;
    public final void rule__Transition__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1650:1: ( rule__Transition__Group__5__Impl rule__Transition__Group__6 )
            // InternalNET.g:1651:2: rule__Transition__Group__5__Impl rule__Transition__Group__6
            {
            pushFollow(FOLLOW_19);
            rule__Transition__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__5"


    // $ANTLR start "rule__Transition__Group__5__Impl"
    // InternalNET.g:1658:1: rule__Transition__Group__5__Impl : ( ( rule__Transition__Group_5__0 )? ) ;
    public final void rule__Transition__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1662:1: ( ( ( rule__Transition__Group_5__0 )? ) )
            // InternalNET.g:1663:1: ( ( rule__Transition__Group_5__0 )? )
            {
            // InternalNET.g:1663:1: ( ( rule__Transition__Group_5__0 )? )
            // InternalNET.g:1664:2: ( rule__Transition__Group_5__0 )?
            {
             before(grammarAccess.getTransitionAccess().getGroup_5()); 
            // InternalNET.g:1665:2: ( rule__Transition__Group_5__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==31) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalNET.g:1665:3: rule__Transition__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Transition__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTransitionAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__5__Impl"


    // $ANTLR start "rule__Transition__Group__6"
    // InternalNET.g:1673:1: rule__Transition__Group__6 : rule__Transition__Group__6__Impl ;
    public final void rule__Transition__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1677:1: ( rule__Transition__Group__6__Impl )
            // InternalNET.g:1678:2: rule__Transition__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__6"


    // $ANTLR start "rule__Transition__Group__6__Impl"
    // InternalNET.g:1684:1: rule__Transition__Group__6__Impl : ( '}' ) ;
    public final void rule__Transition__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1688:1: ( ( '}' ) )
            // InternalNET.g:1689:1: ( '}' )
            {
            // InternalNET.g:1689:1: ( '}' )
            // InternalNET.g:1690:2: '}'
            {
             before(grammarAccess.getTransitionAccess().getRightCurlyBracketKeyword_6()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__6__Impl"


    // $ANTLR start "rule__Transition__Group_4__0"
    // InternalNET.g:1700:1: rule__Transition__Group_4__0 : rule__Transition__Group_4__0__Impl rule__Transition__Group_4__1 ;
    public final void rule__Transition__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1704:1: ( rule__Transition__Group_4__0__Impl rule__Transition__Group_4__1 )
            // InternalNET.g:1705:2: rule__Transition__Group_4__0__Impl rule__Transition__Group_4__1
            {
            pushFollow(FOLLOW_20);
            rule__Transition__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4__0"


    // $ANTLR start "rule__Transition__Group_4__0__Impl"
    // InternalNET.g:1712:1: rule__Transition__Group_4__0__Impl : ( 'arctp' ) ;
    public final void rule__Transition__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1716:1: ( ( 'arctp' ) )
            // InternalNET.g:1717:1: ( 'arctp' )
            {
            // InternalNET.g:1717:1: ( 'arctp' )
            // InternalNET.g:1718:2: 'arctp'
            {
             before(grammarAccess.getTransitionAccess().getArctpKeyword_4_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getArctpKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4__0__Impl"


    // $ANTLR start "rule__Transition__Group_4__1"
    // InternalNET.g:1727:1: rule__Transition__Group_4__1 : rule__Transition__Group_4__1__Impl rule__Transition__Group_4__2 ;
    public final void rule__Transition__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1731:1: ( rule__Transition__Group_4__1__Impl rule__Transition__Group_4__2 )
            // InternalNET.g:1732:2: rule__Transition__Group_4__1__Impl rule__Transition__Group_4__2
            {
            pushFollow(FOLLOW_5);
            rule__Transition__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4__1"


    // $ANTLR start "rule__Transition__Group_4__1__Impl"
    // InternalNET.g:1739:1: rule__Transition__Group_4__1__Impl : ( '(' ) ;
    public final void rule__Transition__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1743:1: ( ( '(' ) )
            // InternalNET.g:1744:1: ( '(' )
            {
            // InternalNET.g:1744:1: ( '(' )
            // InternalNET.g:1745:2: '('
            {
             before(grammarAccess.getTransitionAccess().getLeftParenthesisKeyword_4_1()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getLeftParenthesisKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4__1__Impl"


    // $ANTLR start "rule__Transition__Group_4__2"
    // InternalNET.g:1754:1: rule__Transition__Group_4__2 : rule__Transition__Group_4__2__Impl rule__Transition__Group_4__3 ;
    public final void rule__Transition__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1758:1: ( rule__Transition__Group_4__2__Impl rule__Transition__Group_4__3 )
            // InternalNET.g:1759:2: rule__Transition__Group_4__2__Impl rule__Transition__Group_4__3
            {
            pushFollow(FOLLOW_21);
            rule__Transition__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4__2"


    // $ANTLR start "rule__Transition__Group_4__2__Impl"
    // InternalNET.g:1766:1: rule__Transition__Group_4__2__Impl : ( ( rule__Transition__ArctpAssignment_4_2 ) ) ;
    public final void rule__Transition__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1770:1: ( ( ( rule__Transition__ArctpAssignment_4_2 ) ) )
            // InternalNET.g:1771:1: ( ( rule__Transition__ArctpAssignment_4_2 ) )
            {
            // InternalNET.g:1771:1: ( ( rule__Transition__ArctpAssignment_4_2 ) )
            // InternalNET.g:1772:2: ( rule__Transition__ArctpAssignment_4_2 )
            {
             before(grammarAccess.getTransitionAccess().getArctpAssignment_4_2()); 
            // InternalNET.g:1773:2: ( rule__Transition__ArctpAssignment_4_2 )
            // InternalNET.g:1773:3: rule__Transition__ArctpAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__Transition__ArctpAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getArctpAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4__2__Impl"


    // $ANTLR start "rule__Transition__Group_4__3"
    // InternalNET.g:1781:1: rule__Transition__Group_4__3 : rule__Transition__Group_4__3__Impl rule__Transition__Group_4__4 ;
    public final void rule__Transition__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1785:1: ( rule__Transition__Group_4__3__Impl rule__Transition__Group_4__4 )
            // InternalNET.g:1786:2: rule__Transition__Group_4__3__Impl rule__Transition__Group_4__4
            {
            pushFollow(FOLLOW_21);
            rule__Transition__Group_4__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_4__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4__3"


    // $ANTLR start "rule__Transition__Group_4__3__Impl"
    // InternalNET.g:1793:1: rule__Transition__Group_4__3__Impl : ( ( rule__Transition__Group_4_3__0 )* ) ;
    public final void rule__Transition__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1797:1: ( ( ( rule__Transition__Group_4_3__0 )* ) )
            // InternalNET.g:1798:1: ( ( rule__Transition__Group_4_3__0 )* )
            {
            // InternalNET.g:1798:1: ( ( rule__Transition__Group_4_3__0 )* )
            // InternalNET.g:1799:2: ( rule__Transition__Group_4_3__0 )*
            {
             before(grammarAccess.getTransitionAccess().getGroup_4_3()); 
            // InternalNET.g:1800:2: ( rule__Transition__Group_4_3__0 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==19) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalNET.g:1800:3: rule__Transition__Group_4_3__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Transition__Group_4_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getTransitionAccess().getGroup_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4__3__Impl"


    // $ANTLR start "rule__Transition__Group_4__4"
    // InternalNET.g:1808:1: rule__Transition__Group_4__4 : rule__Transition__Group_4__4__Impl ;
    public final void rule__Transition__Group_4__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1812:1: ( rule__Transition__Group_4__4__Impl )
            // InternalNET.g:1813:2: rule__Transition__Group_4__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group_4__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4__4"


    // $ANTLR start "rule__Transition__Group_4__4__Impl"
    // InternalNET.g:1819:1: rule__Transition__Group_4__4__Impl : ( ')' ) ;
    public final void rule__Transition__Group_4__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1823:1: ( ( ')' ) )
            // InternalNET.g:1824:1: ( ')' )
            {
            // InternalNET.g:1824:1: ( ')' )
            // InternalNET.g:1825:2: ')'
            {
             before(grammarAccess.getTransitionAccess().getRightParenthesisKeyword_4_4()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getRightParenthesisKeyword_4_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4__4__Impl"


    // $ANTLR start "rule__Transition__Group_4_3__0"
    // InternalNET.g:1835:1: rule__Transition__Group_4_3__0 : rule__Transition__Group_4_3__0__Impl rule__Transition__Group_4_3__1 ;
    public final void rule__Transition__Group_4_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1839:1: ( rule__Transition__Group_4_3__0__Impl rule__Transition__Group_4_3__1 )
            // InternalNET.g:1840:2: rule__Transition__Group_4_3__0__Impl rule__Transition__Group_4_3__1
            {
            pushFollow(FOLLOW_5);
            rule__Transition__Group_4_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_4_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4_3__0"


    // $ANTLR start "rule__Transition__Group_4_3__0__Impl"
    // InternalNET.g:1847:1: rule__Transition__Group_4_3__0__Impl : ( ',' ) ;
    public final void rule__Transition__Group_4_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1851:1: ( ( ',' ) )
            // InternalNET.g:1852:1: ( ',' )
            {
            // InternalNET.g:1852:1: ( ',' )
            // InternalNET.g:1853:2: ','
            {
             before(grammarAccess.getTransitionAccess().getCommaKeyword_4_3_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getCommaKeyword_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4_3__0__Impl"


    // $ANTLR start "rule__Transition__Group_4_3__1"
    // InternalNET.g:1862:1: rule__Transition__Group_4_3__1 : rule__Transition__Group_4_3__1__Impl ;
    public final void rule__Transition__Group_4_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1866:1: ( rule__Transition__Group_4_3__1__Impl )
            // InternalNET.g:1867:2: rule__Transition__Group_4_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group_4_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4_3__1"


    // $ANTLR start "rule__Transition__Group_4_3__1__Impl"
    // InternalNET.g:1873:1: rule__Transition__Group_4_3__1__Impl : ( ( rule__Transition__ArctpAssignment_4_3_1 ) ) ;
    public final void rule__Transition__Group_4_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1877:1: ( ( ( rule__Transition__ArctpAssignment_4_3_1 ) ) )
            // InternalNET.g:1878:1: ( ( rule__Transition__ArctpAssignment_4_3_1 ) )
            {
            // InternalNET.g:1878:1: ( ( rule__Transition__ArctpAssignment_4_3_1 ) )
            // InternalNET.g:1879:2: ( rule__Transition__ArctpAssignment_4_3_1 )
            {
             before(grammarAccess.getTransitionAccess().getArctpAssignment_4_3_1()); 
            // InternalNET.g:1880:2: ( rule__Transition__ArctpAssignment_4_3_1 )
            // InternalNET.g:1880:3: rule__Transition__ArctpAssignment_4_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Transition__ArctpAssignment_4_3_1();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getArctpAssignment_4_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_4_3__1__Impl"


    // $ANTLR start "rule__Transition__Group_5__0"
    // InternalNET.g:1889:1: rule__Transition__Group_5__0 : rule__Transition__Group_5__0__Impl rule__Transition__Group_5__1 ;
    public final void rule__Transition__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1893:1: ( rule__Transition__Group_5__0__Impl rule__Transition__Group_5__1 )
            // InternalNET.g:1894:2: rule__Transition__Group_5__0__Impl rule__Transition__Group_5__1
            {
            pushFollow(FOLLOW_20);
            rule__Transition__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__0"


    // $ANTLR start "rule__Transition__Group_5__0__Impl"
    // InternalNET.g:1901:1: rule__Transition__Group_5__0__Impl : ( 'arcpt' ) ;
    public final void rule__Transition__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1905:1: ( ( 'arcpt' ) )
            // InternalNET.g:1906:1: ( 'arcpt' )
            {
            // InternalNET.g:1906:1: ( 'arcpt' )
            // InternalNET.g:1907:2: 'arcpt'
            {
             before(grammarAccess.getTransitionAccess().getArcptKeyword_5_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getArcptKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__0__Impl"


    // $ANTLR start "rule__Transition__Group_5__1"
    // InternalNET.g:1916:1: rule__Transition__Group_5__1 : rule__Transition__Group_5__1__Impl rule__Transition__Group_5__2 ;
    public final void rule__Transition__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1920:1: ( rule__Transition__Group_5__1__Impl rule__Transition__Group_5__2 )
            // InternalNET.g:1921:2: rule__Transition__Group_5__1__Impl rule__Transition__Group_5__2
            {
            pushFollow(FOLLOW_5);
            rule__Transition__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__1"


    // $ANTLR start "rule__Transition__Group_5__1__Impl"
    // InternalNET.g:1928:1: rule__Transition__Group_5__1__Impl : ( '(' ) ;
    public final void rule__Transition__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1932:1: ( ( '(' ) )
            // InternalNET.g:1933:1: ( '(' )
            {
            // InternalNET.g:1933:1: ( '(' )
            // InternalNET.g:1934:2: '('
            {
             before(grammarAccess.getTransitionAccess().getLeftParenthesisKeyword_5_1()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getLeftParenthesisKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__1__Impl"


    // $ANTLR start "rule__Transition__Group_5__2"
    // InternalNET.g:1943:1: rule__Transition__Group_5__2 : rule__Transition__Group_5__2__Impl rule__Transition__Group_5__3 ;
    public final void rule__Transition__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1947:1: ( rule__Transition__Group_5__2__Impl rule__Transition__Group_5__3 )
            // InternalNET.g:1948:2: rule__Transition__Group_5__2__Impl rule__Transition__Group_5__3
            {
            pushFollow(FOLLOW_21);
            rule__Transition__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__2"


    // $ANTLR start "rule__Transition__Group_5__2__Impl"
    // InternalNET.g:1955:1: rule__Transition__Group_5__2__Impl : ( ( rule__Transition__ArcptAssignment_5_2 ) ) ;
    public final void rule__Transition__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1959:1: ( ( ( rule__Transition__ArcptAssignment_5_2 ) ) )
            // InternalNET.g:1960:1: ( ( rule__Transition__ArcptAssignment_5_2 ) )
            {
            // InternalNET.g:1960:1: ( ( rule__Transition__ArcptAssignment_5_2 ) )
            // InternalNET.g:1961:2: ( rule__Transition__ArcptAssignment_5_2 )
            {
             before(grammarAccess.getTransitionAccess().getArcptAssignment_5_2()); 
            // InternalNET.g:1962:2: ( rule__Transition__ArcptAssignment_5_2 )
            // InternalNET.g:1962:3: rule__Transition__ArcptAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__Transition__ArcptAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getArcptAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__2__Impl"


    // $ANTLR start "rule__Transition__Group_5__3"
    // InternalNET.g:1970:1: rule__Transition__Group_5__3 : rule__Transition__Group_5__3__Impl rule__Transition__Group_5__4 ;
    public final void rule__Transition__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1974:1: ( rule__Transition__Group_5__3__Impl rule__Transition__Group_5__4 )
            // InternalNET.g:1975:2: rule__Transition__Group_5__3__Impl rule__Transition__Group_5__4
            {
            pushFollow(FOLLOW_21);
            rule__Transition__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__3"


    // $ANTLR start "rule__Transition__Group_5__3__Impl"
    // InternalNET.g:1982:1: rule__Transition__Group_5__3__Impl : ( ( rule__Transition__Group_5_3__0 )* ) ;
    public final void rule__Transition__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:1986:1: ( ( ( rule__Transition__Group_5_3__0 )* ) )
            // InternalNET.g:1987:1: ( ( rule__Transition__Group_5_3__0 )* )
            {
            // InternalNET.g:1987:1: ( ( rule__Transition__Group_5_3__0 )* )
            // InternalNET.g:1988:2: ( rule__Transition__Group_5_3__0 )*
            {
             before(grammarAccess.getTransitionAccess().getGroup_5_3()); 
            // InternalNET.g:1989:2: ( rule__Transition__Group_5_3__0 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==19) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalNET.g:1989:3: rule__Transition__Group_5_3__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Transition__Group_5_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getTransitionAccess().getGroup_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__3__Impl"


    // $ANTLR start "rule__Transition__Group_5__4"
    // InternalNET.g:1997:1: rule__Transition__Group_5__4 : rule__Transition__Group_5__4__Impl ;
    public final void rule__Transition__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2001:1: ( rule__Transition__Group_5__4__Impl )
            // InternalNET.g:2002:2: rule__Transition__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__4"


    // $ANTLR start "rule__Transition__Group_5__4__Impl"
    // InternalNET.g:2008:1: rule__Transition__Group_5__4__Impl : ( ')' ) ;
    public final void rule__Transition__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2012:1: ( ( ')' ) )
            // InternalNET.g:2013:1: ( ')' )
            {
            // InternalNET.g:2013:1: ( ')' )
            // InternalNET.g:2014:2: ')'
            {
             before(grammarAccess.getTransitionAccess().getRightParenthesisKeyword_5_4()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getRightParenthesisKeyword_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__4__Impl"


    // $ANTLR start "rule__Transition__Group_5_3__0"
    // InternalNET.g:2024:1: rule__Transition__Group_5_3__0 : rule__Transition__Group_5_3__0__Impl rule__Transition__Group_5_3__1 ;
    public final void rule__Transition__Group_5_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2028:1: ( rule__Transition__Group_5_3__0__Impl rule__Transition__Group_5_3__1 )
            // InternalNET.g:2029:2: rule__Transition__Group_5_3__0__Impl rule__Transition__Group_5_3__1
            {
            pushFollow(FOLLOW_5);
            rule__Transition__Group_5_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group_5_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5_3__0"


    // $ANTLR start "rule__Transition__Group_5_3__0__Impl"
    // InternalNET.g:2036:1: rule__Transition__Group_5_3__0__Impl : ( ',' ) ;
    public final void rule__Transition__Group_5_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2040:1: ( ( ',' ) )
            // InternalNET.g:2041:1: ( ',' )
            {
            // InternalNET.g:2041:1: ( ',' )
            // InternalNET.g:2042:2: ','
            {
             before(grammarAccess.getTransitionAccess().getCommaKeyword_5_3_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getCommaKeyword_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5_3__0__Impl"


    // $ANTLR start "rule__Transition__Group_5_3__1"
    // InternalNET.g:2051:1: rule__Transition__Group_5_3__1 : rule__Transition__Group_5_3__1__Impl ;
    public final void rule__Transition__Group_5_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2055:1: ( rule__Transition__Group_5_3__1__Impl )
            // InternalNET.g:2056:2: rule__Transition__Group_5_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group_5_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5_3__1"


    // $ANTLR start "rule__Transition__Group_5_3__1__Impl"
    // InternalNET.g:2062:1: rule__Transition__Group_5_3__1__Impl : ( ( rule__Transition__ArcptAssignment_5_3_1 ) ) ;
    public final void rule__Transition__Group_5_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2066:1: ( ( ( rule__Transition__ArcptAssignment_5_3_1 ) ) )
            // InternalNET.g:2067:1: ( ( rule__Transition__ArcptAssignment_5_3_1 ) )
            {
            // InternalNET.g:2067:1: ( ( rule__Transition__ArcptAssignment_5_3_1 ) )
            // InternalNET.g:2068:2: ( rule__Transition__ArcptAssignment_5_3_1 )
            {
             before(grammarAccess.getTransitionAccess().getArcptAssignment_5_3_1()); 
            // InternalNET.g:2069:2: ( rule__Transition__ArcptAssignment_5_3_1 )
            // InternalNET.g:2069:3: rule__Transition__ArcptAssignment_5_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Transition__ArcptAssignment_5_3_1();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getArcptAssignment_5_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5_3__1__Impl"


    // $ANTLR start "rule__Place__Group__0"
    // InternalNET.g:2078:1: rule__Place__Group__0 : rule__Place__Group__0__Impl rule__Place__Group__1 ;
    public final void rule__Place__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2082:1: ( rule__Place__Group__0__Impl rule__Place__Group__1 )
            // InternalNET.g:2083:2: rule__Place__Group__0__Impl rule__Place__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Place__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__0"


    // $ANTLR start "rule__Place__Group__0__Impl"
    // InternalNET.g:2090:1: rule__Place__Group__0__Impl : ( 'Place' ) ;
    public final void rule__Place__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2094:1: ( ( 'Place' ) )
            // InternalNET.g:2095:1: ( 'Place' )
            {
            // InternalNET.g:2095:1: ( 'Place' )
            // InternalNET.g:2096:2: 'Place'
            {
             before(grammarAccess.getPlaceAccess().getPlaceKeyword_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getPlaceKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__0__Impl"


    // $ANTLR start "rule__Place__Group__1"
    // InternalNET.g:2105:1: rule__Place__Group__1 : rule__Place__Group__1__Impl rule__Place__Group__2 ;
    public final void rule__Place__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2109:1: ( rule__Place__Group__1__Impl rule__Place__Group__2 )
            // InternalNET.g:2110:2: rule__Place__Group__1__Impl rule__Place__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Place__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__1"


    // $ANTLR start "rule__Place__Group__1__Impl"
    // InternalNET.g:2117:1: rule__Place__Group__1__Impl : ( '{' ) ;
    public final void rule__Place__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2121:1: ( ( '{' ) )
            // InternalNET.g:2122:1: ( '{' )
            {
            // InternalNET.g:2122:1: ( '{' )
            // InternalNET.g:2123:2: '{'
            {
             before(grammarAccess.getPlaceAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__1__Impl"


    // $ANTLR start "rule__Place__Group__2"
    // InternalNET.g:2132:1: rule__Place__Group__2 : rule__Place__Group__2__Impl rule__Place__Group__3 ;
    public final void rule__Place__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2136:1: ( rule__Place__Group__2__Impl rule__Place__Group__3 )
            // InternalNET.g:2137:2: rule__Place__Group__2__Impl rule__Place__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Place__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__2"


    // $ANTLR start "rule__Place__Group__2__Impl"
    // InternalNET.g:2144:1: rule__Place__Group__2__Impl : ( 'nom' ) ;
    public final void rule__Place__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2148:1: ( ( 'nom' ) )
            // InternalNET.g:2149:1: ( 'nom' )
            {
            // InternalNET.g:2149:1: ( 'nom' )
            // InternalNET.g:2150:2: 'nom'
            {
             before(grammarAccess.getPlaceAccess().getNomKeyword_2()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getNomKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__2__Impl"


    // $ANTLR start "rule__Place__Group__3"
    // InternalNET.g:2159:1: rule__Place__Group__3 : rule__Place__Group__3__Impl rule__Place__Group__4 ;
    public final void rule__Place__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2163:1: ( rule__Place__Group__3__Impl rule__Place__Group__4 )
            // InternalNET.g:2164:2: rule__Place__Group__3__Impl rule__Place__Group__4
            {
            pushFollow(FOLLOW_22);
            rule__Place__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__3"


    // $ANTLR start "rule__Place__Group__3__Impl"
    // InternalNET.g:2171:1: rule__Place__Group__3__Impl : ( ( rule__Place__NomAssignment_3 ) ) ;
    public final void rule__Place__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2175:1: ( ( ( rule__Place__NomAssignment_3 ) ) )
            // InternalNET.g:2176:1: ( ( rule__Place__NomAssignment_3 ) )
            {
            // InternalNET.g:2176:1: ( ( rule__Place__NomAssignment_3 ) )
            // InternalNET.g:2177:2: ( rule__Place__NomAssignment_3 )
            {
             before(grammarAccess.getPlaceAccess().getNomAssignment_3()); 
            // InternalNET.g:2178:2: ( rule__Place__NomAssignment_3 )
            // InternalNET.g:2178:3: rule__Place__NomAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Place__NomAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getNomAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__3__Impl"


    // $ANTLR start "rule__Place__Group__4"
    // InternalNET.g:2186:1: rule__Place__Group__4 : rule__Place__Group__4__Impl rule__Place__Group__5 ;
    public final void rule__Place__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2190:1: ( rule__Place__Group__4__Impl rule__Place__Group__5 )
            // InternalNET.g:2191:2: rule__Place__Group__4__Impl rule__Place__Group__5
            {
            pushFollow(FOLLOW_14);
            rule__Place__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__4"


    // $ANTLR start "rule__Place__Group__4__Impl"
    // InternalNET.g:2198:1: rule__Place__Group__4__Impl : ( 'nbJetons' ) ;
    public final void rule__Place__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2202:1: ( ( 'nbJetons' ) )
            // InternalNET.g:2203:1: ( 'nbJetons' )
            {
            // InternalNET.g:2203:1: ( 'nbJetons' )
            // InternalNET.g:2204:2: 'nbJetons'
            {
             before(grammarAccess.getPlaceAccess().getNbJetonsKeyword_4()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getNbJetonsKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__4__Impl"


    // $ANTLR start "rule__Place__Group__5"
    // InternalNET.g:2213:1: rule__Place__Group__5 : rule__Place__Group__5__Impl rule__Place__Group__6 ;
    public final void rule__Place__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2217:1: ( rule__Place__Group__5__Impl rule__Place__Group__6 )
            // InternalNET.g:2218:2: rule__Place__Group__5__Impl rule__Place__Group__6
            {
            pushFollow(FOLLOW_19);
            rule__Place__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__5"


    // $ANTLR start "rule__Place__Group__5__Impl"
    // InternalNET.g:2225:1: rule__Place__Group__5__Impl : ( ( rule__Place__NbJetonsAssignment_5 ) ) ;
    public final void rule__Place__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2229:1: ( ( ( rule__Place__NbJetonsAssignment_5 ) ) )
            // InternalNET.g:2230:1: ( ( rule__Place__NbJetonsAssignment_5 ) )
            {
            // InternalNET.g:2230:1: ( ( rule__Place__NbJetonsAssignment_5 ) )
            // InternalNET.g:2231:2: ( rule__Place__NbJetonsAssignment_5 )
            {
             before(grammarAccess.getPlaceAccess().getNbJetonsAssignment_5()); 
            // InternalNET.g:2232:2: ( rule__Place__NbJetonsAssignment_5 )
            // InternalNET.g:2232:3: rule__Place__NbJetonsAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Place__NbJetonsAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getNbJetonsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__5__Impl"


    // $ANTLR start "rule__Place__Group__6"
    // InternalNET.g:2240:1: rule__Place__Group__6 : rule__Place__Group__6__Impl rule__Place__Group__7 ;
    public final void rule__Place__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2244:1: ( rule__Place__Group__6__Impl rule__Place__Group__7 )
            // InternalNET.g:2245:2: rule__Place__Group__6__Impl rule__Place__Group__7
            {
            pushFollow(FOLLOW_19);
            rule__Place__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__6"


    // $ANTLR start "rule__Place__Group__6__Impl"
    // InternalNET.g:2252:1: rule__Place__Group__6__Impl : ( ( rule__Place__Group_6__0 )? ) ;
    public final void rule__Place__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2256:1: ( ( ( rule__Place__Group_6__0 )? ) )
            // InternalNET.g:2257:1: ( ( rule__Place__Group_6__0 )? )
            {
            // InternalNET.g:2257:1: ( ( rule__Place__Group_6__0 )? )
            // InternalNET.g:2258:2: ( rule__Place__Group_6__0 )?
            {
             before(grammarAccess.getPlaceAccess().getGroup_6()); 
            // InternalNET.g:2259:2: ( rule__Place__Group_6__0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==28) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalNET.g:2259:3: rule__Place__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Place__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPlaceAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__6__Impl"


    // $ANTLR start "rule__Place__Group__7"
    // InternalNET.g:2267:1: rule__Place__Group__7 : rule__Place__Group__7__Impl rule__Place__Group__8 ;
    public final void rule__Place__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2271:1: ( rule__Place__Group__7__Impl rule__Place__Group__8 )
            // InternalNET.g:2272:2: rule__Place__Group__7__Impl rule__Place__Group__8
            {
            pushFollow(FOLLOW_19);
            rule__Place__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__7"


    // $ANTLR start "rule__Place__Group__7__Impl"
    // InternalNET.g:2279:1: rule__Place__Group__7__Impl : ( ( rule__Place__Group_7__0 )? ) ;
    public final void rule__Place__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2283:1: ( ( ( rule__Place__Group_7__0 )? ) )
            // InternalNET.g:2284:1: ( ( rule__Place__Group_7__0 )? )
            {
            // InternalNET.g:2284:1: ( ( rule__Place__Group_7__0 )? )
            // InternalNET.g:2285:2: ( rule__Place__Group_7__0 )?
            {
             before(grammarAccess.getPlaceAccess().getGroup_7()); 
            // InternalNET.g:2286:2: ( rule__Place__Group_7__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==31) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalNET.g:2286:3: rule__Place__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Place__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPlaceAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__7__Impl"


    // $ANTLR start "rule__Place__Group__8"
    // InternalNET.g:2294:1: rule__Place__Group__8 : rule__Place__Group__8__Impl ;
    public final void rule__Place__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2298:1: ( rule__Place__Group__8__Impl )
            // InternalNET.g:2299:2: rule__Place__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Place__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__8"


    // $ANTLR start "rule__Place__Group__8__Impl"
    // InternalNET.g:2305:1: rule__Place__Group__8__Impl : ( '}' ) ;
    public final void rule__Place__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2309:1: ( ( '}' ) )
            // InternalNET.g:2310:1: ( '}' )
            {
            // InternalNET.g:2310:1: ( '}' )
            // InternalNET.g:2311:2: '}'
            {
             before(grammarAccess.getPlaceAccess().getRightCurlyBracketKeyword_8()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__8__Impl"


    // $ANTLR start "rule__Place__Group_6__0"
    // InternalNET.g:2321:1: rule__Place__Group_6__0 : rule__Place__Group_6__0__Impl rule__Place__Group_6__1 ;
    public final void rule__Place__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2325:1: ( rule__Place__Group_6__0__Impl rule__Place__Group_6__1 )
            // InternalNET.g:2326:2: rule__Place__Group_6__0__Impl rule__Place__Group_6__1
            {
            pushFollow(FOLLOW_20);
            rule__Place__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6__0"


    // $ANTLR start "rule__Place__Group_6__0__Impl"
    // InternalNET.g:2333:1: rule__Place__Group_6__0__Impl : ( 'arctp' ) ;
    public final void rule__Place__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2337:1: ( ( 'arctp' ) )
            // InternalNET.g:2338:1: ( 'arctp' )
            {
            // InternalNET.g:2338:1: ( 'arctp' )
            // InternalNET.g:2339:2: 'arctp'
            {
             before(grammarAccess.getPlaceAccess().getArctpKeyword_6_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getArctpKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6__0__Impl"


    // $ANTLR start "rule__Place__Group_6__1"
    // InternalNET.g:2348:1: rule__Place__Group_6__1 : rule__Place__Group_6__1__Impl rule__Place__Group_6__2 ;
    public final void rule__Place__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2352:1: ( rule__Place__Group_6__1__Impl rule__Place__Group_6__2 )
            // InternalNET.g:2353:2: rule__Place__Group_6__1__Impl rule__Place__Group_6__2
            {
            pushFollow(FOLLOW_5);
            rule__Place__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6__1"


    // $ANTLR start "rule__Place__Group_6__1__Impl"
    // InternalNET.g:2360:1: rule__Place__Group_6__1__Impl : ( '(' ) ;
    public final void rule__Place__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2364:1: ( ( '(' ) )
            // InternalNET.g:2365:1: ( '(' )
            {
            // InternalNET.g:2365:1: ( '(' )
            // InternalNET.g:2366:2: '('
            {
             before(grammarAccess.getPlaceAccess().getLeftParenthesisKeyword_6_1()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getLeftParenthesisKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6__1__Impl"


    // $ANTLR start "rule__Place__Group_6__2"
    // InternalNET.g:2375:1: rule__Place__Group_6__2 : rule__Place__Group_6__2__Impl rule__Place__Group_6__3 ;
    public final void rule__Place__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2379:1: ( rule__Place__Group_6__2__Impl rule__Place__Group_6__3 )
            // InternalNET.g:2380:2: rule__Place__Group_6__2__Impl rule__Place__Group_6__3
            {
            pushFollow(FOLLOW_21);
            rule__Place__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6__2"


    // $ANTLR start "rule__Place__Group_6__2__Impl"
    // InternalNET.g:2387:1: rule__Place__Group_6__2__Impl : ( ( rule__Place__ArctpAssignment_6_2 ) ) ;
    public final void rule__Place__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2391:1: ( ( ( rule__Place__ArctpAssignment_6_2 ) ) )
            // InternalNET.g:2392:1: ( ( rule__Place__ArctpAssignment_6_2 ) )
            {
            // InternalNET.g:2392:1: ( ( rule__Place__ArctpAssignment_6_2 ) )
            // InternalNET.g:2393:2: ( rule__Place__ArctpAssignment_6_2 )
            {
             before(grammarAccess.getPlaceAccess().getArctpAssignment_6_2()); 
            // InternalNET.g:2394:2: ( rule__Place__ArctpAssignment_6_2 )
            // InternalNET.g:2394:3: rule__Place__ArctpAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__Place__ArctpAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getArctpAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6__2__Impl"


    // $ANTLR start "rule__Place__Group_6__3"
    // InternalNET.g:2402:1: rule__Place__Group_6__3 : rule__Place__Group_6__3__Impl rule__Place__Group_6__4 ;
    public final void rule__Place__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2406:1: ( rule__Place__Group_6__3__Impl rule__Place__Group_6__4 )
            // InternalNET.g:2407:2: rule__Place__Group_6__3__Impl rule__Place__Group_6__4
            {
            pushFollow(FOLLOW_21);
            rule__Place__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6__3"


    // $ANTLR start "rule__Place__Group_6__3__Impl"
    // InternalNET.g:2414:1: rule__Place__Group_6__3__Impl : ( ( rule__Place__Group_6_3__0 )* ) ;
    public final void rule__Place__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2418:1: ( ( ( rule__Place__Group_6_3__0 )* ) )
            // InternalNET.g:2419:1: ( ( rule__Place__Group_6_3__0 )* )
            {
            // InternalNET.g:2419:1: ( ( rule__Place__Group_6_3__0 )* )
            // InternalNET.g:2420:2: ( rule__Place__Group_6_3__0 )*
            {
             before(grammarAccess.getPlaceAccess().getGroup_6_3()); 
            // InternalNET.g:2421:2: ( rule__Place__Group_6_3__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==19) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalNET.g:2421:3: rule__Place__Group_6_3__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Place__Group_6_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getPlaceAccess().getGroup_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6__3__Impl"


    // $ANTLR start "rule__Place__Group_6__4"
    // InternalNET.g:2429:1: rule__Place__Group_6__4 : rule__Place__Group_6__4__Impl ;
    public final void rule__Place__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2433:1: ( rule__Place__Group_6__4__Impl )
            // InternalNET.g:2434:2: rule__Place__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Place__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6__4"


    // $ANTLR start "rule__Place__Group_6__4__Impl"
    // InternalNET.g:2440:1: rule__Place__Group_6__4__Impl : ( ')' ) ;
    public final void rule__Place__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2444:1: ( ( ')' ) )
            // InternalNET.g:2445:1: ( ')' )
            {
            // InternalNET.g:2445:1: ( ')' )
            // InternalNET.g:2446:2: ')'
            {
             before(grammarAccess.getPlaceAccess().getRightParenthesisKeyword_6_4()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getRightParenthesisKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6__4__Impl"


    // $ANTLR start "rule__Place__Group_6_3__0"
    // InternalNET.g:2456:1: rule__Place__Group_6_3__0 : rule__Place__Group_6_3__0__Impl rule__Place__Group_6_3__1 ;
    public final void rule__Place__Group_6_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2460:1: ( rule__Place__Group_6_3__0__Impl rule__Place__Group_6_3__1 )
            // InternalNET.g:2461:2: rule__Place__Group_6_3__0__Impl rule__Place__Group_6_3__1
            {
            pushFollow(FOLLOW_5);
            rule__Place__Group_6_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group_6_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6_3__0"


    // $ANTLR start "rule__Place__Group_6_3__0__Impl"
    // InternalNET.g:2468:1: rule__Place__Group_6_3__0__Impl : ( ',' ) ;
    public final void rule__Place__Group_6_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2472:1: ( ( ',' ) )
            // InternalNET.g:2473:1: ( ',' )
            {
            // InternalNET.g:2473:1: ( ',' )
            // InternalNET.g:2474:2: ','
            {
             before(grammarAccess.getPlaceAccess().getCommaKeyword_6_3_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getCommaKeyword_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6_3__0__Impl"


    // $ANTLR start "rule__Place__Group_6_3__1"
    // InternalNET.g:2483:1: rule__Place__Group_6_3__1 : rule__Place__Group_6_3__1__Impl ;
    public final void rule__Place__Group_6_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2487:1: ( rule__Place__Group_6_3__1__Impl )
            // InternalNET.g:2488:2: rule__Place__Group_6_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Place__Group_6_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6_3__1"


    // $ANTLR start "rule__Place__Group_6_3__1__Impl"
    // InternalNET.g:2494:1: rule__Place__Group_6_3__1__Impl : ( ( rule__Place__ArctpAssignment_6_3_1 ) ) ;
    public final void rule__Place__Group_6_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2498:1: ( ( ( rule__Place__ArctpAssignment_6_3_1 ) ) )
            // InternalNET.g:2499:1: ( ( rule__Place__ArctpAssignment_6_3_1 ) )
            {
            // InternalNET.g:2499:1: ( ( rule__Place__ArctpAssignment_6_3_1 ) )
            // InternalNET.g:2500:2: ( rule__Place__ArctpAssignment_6_3_1 )
            {
             before(grammarAccess.getPlaceAccess().getArctpAssignment_6_3_1()); 
            // InternalNET.g:2501:2: ( rule__Place__ArctpAssignment_6_3_1 )
            // InternalNET.g:2501:3: rule__Place__ArctpAssignment_6_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Place__ArctpAssignment_6_3_1();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getArctpAssignment_6_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_6_3__1__Impl"


    // $ANTLR start "rule__Place__Group_7__0"
    // InternalNET.g:2510:1: rule__Place__Group_7__0 : rule__Place__Group_7__0__Impl rule__Place__Group_7__1 ;
    public final void rule__Place__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2514:1: ( rule__Place__Group_7__0__Impl rule__Place__Group_7__1 )
            // InternalNET.g:2515:2: rule__Place__Group_7__0__Impl rule__Place__Group_7__1
            {
            pushFollow(FOLLOW_20);
            rule__Place__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7__0"


    // $ANTLR start "rule__Place__Group_7__0__Impl"
    // InternalNET.g:2522:1: rule__Place__Group_7__0__Impl : ( 'arcpt' ) ;
    public final void rule__Place__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2526:1: ( ( 'arcpt' ) )
            // InternalNET.g:2527:1: ( 'arcpt' )
            {
            // InternalNET.g:2527:1: ( 'arcpt' )
            // InternalNET.g:2528:2: 'arcpt'
            {
             before(grammarAccess.getPlaceAccess().getArcptKeyword_7_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getArcptKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7__0__Impl"


    // $ANTLR start "rule__Place__Group_7__1"
    // InternalNET.g:2537:1: rule__Place__Group_7__1 : rule__Place__Group_7__1__Impl rule__Place__Group_7__2 ;
    public final void rule__Place__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2541:1: ( rule__Place__Group_7__1__Impl rule__Place__Group_7__2 )
            // InternalNET.g:2542:2: rule__Place__Group_7__1__Impl rule__Place__Group_7__2
            {
            pushFollow(FOLLOW_5);
            rule__Place__Group_7__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group_7__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7__1"


    // $ANTLR start "rule__Place__Group_7__1__Impl"
    // InternalNET.g:2549:1: rule__Place__Group_7__1__Impl : ( '(' ) ;
    public final void rule__Place__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2553:1: ( ( '(' ) )
            // InternalNET.g:2554:1: ( '(' )
            {
            // InternalNET.g:2554:1: ( '(' )
            // InternalNET.g:2555:2: '('
            {
             before(grammarAccess.getPlaceAccess().getLeftParenthesisKeyword_7_1()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getLeftParenthesisKeyword_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7__1__Impl"


    // $ANTLR start "rule__Place__Group_7__2"
    // InternalNET.g:2564:1: rule__Place__Group_7__2 : rule__Place__Group_7__2__Impl rule__Place__Group_7__3 ;
    public final void rule__Place__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2568:1: ( rule__Place__Group_7__2__Impl rule__Place__Group_7__3 )
            // InternalNET.g:2569:2: rule__Place__Group_7__2__Impl rule__Place__Group_7__3
            {
            pushFollow(FOLLOW_21);
            rule__Place__Group_7__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group_7__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7__2"


    // $ANTLR start "rule__Place__Group_7__2__Impl"
    // InternalNET.g:2576:1: rule__Place__Group_7__2__Impl : ( ( rule__Place__ArcptAssignment_7_2 ) ) ;
    public final void rule__Place__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2580:1: ( ( ( rule__Place__ArcptAssignment_7_2 ) ) )
            // InternalNET.g:2581:1: ( ( rule__Place__ArcptAssignment_7_2 ) )
            {
            // InternalNET.g:2581:1: ( ( rule__Place__ArcptAssignment_7_2 ) )
            // InternalNET.g:2582:2: ( rule__Place__ArcptAssignment_7_2 )
            {
             before(grammarAccess.getPlaceAccess().getArcptAssignment_7_2()); 
            // InternalNET.g:2583:2: ( rule__Place__ArcptAssignment_7_2 )
            // InternalNET.g:2583:3: rule__Place__ArcptAssignment_7_2
            {
            pushFollow(FOLLOW_2);
            rule__Place__ArcptAssignment_7_2();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getArcptAssignment_7_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7__2__Impl"


    // $ANTLR start "rule__Place__Group_7__3"
    // InternalNET.g:2591:1: rule__Place__Group_7__3 : rule__Place__Group_7__3__Impl rule__Place__Group_7__4 ;
    public final void rule__Place__Group_7__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2595:1: ( rule__Place__Group_7__3__Impl rule__Place__Group_7__4 )
            // InternalNET.g:2596:2: rule__Place__Group_7__3__Impl rule__Place__Group_7__4
            {
            pushFollow(FOLLOW_21);
            rule__Place__Group_7__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group_7__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7__3"


    // $ANTLR start "rule__Place__Group_7__3__Impl"
    // InternalNET.g:2603:1: rule__Place__Group_7__3__Impl : ( ( rule__Place__Group_7_3__0 )* ) ;
    public final void rule__Place__Group_7__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2607:1: ( ( ( rule__Place__Group_7_3__0 )* ) )
            // InternalNET.g:2608:1: ( ( rule__Place__Group_7_3__0 )* )
            {
            // InternalNET.g:2608:1: ( ( rule__Place__Group_7_3__0 )* )
            // InternalNET.g:2609:2: ( rule__Place__Group_7_3__0 )*
            {
             before(grammarAccess.getPlaceAccess().getGroup_7_3()); 
            // InternalNET.g:2610:2: ( rule__Place__Group_7_3__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==19) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalNET.g:2610:3: rule__Place__Group_7_3__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Place__Group_7_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getPlaceAccess().getGroup_7_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7__3__Impl"


    // $ANTLR start "rule__Place__Group_7__4"
    // InternalNET.g:2618:1: rule__Place__Group_7__4 : rule__Place__Group_7__4__Impl ;
    public final void rule__Place__Group_7__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2622:1: ( rule__Place__Group_7__4__Impl )
            // InternalNET.g:2623:2: rule__Place__Group_7__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Place__Group_7__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7__4"


    // $ANTLR start "rule__Place__Group_7__4__Impl"
    // InternalNET.g:2629:1: rule__Place__Group_7__4__Impl : ( ')' ) ;
    public final void rule__Place__Group_7__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2633:1: ( ( ')' ) )
            // InternalNET.g:2634:1: ( ')' )
            {
            // InternalNET.g:2634:1: ( ')' )
            // InternalNET.g:2635:2: ')'
            {
             before(grammarAccess.getPlaceAccess().getRightParenthesisKeyword_7_4()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getRightParenthesisKeyword_7_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7__4__Impl"


    // $ANTLR start "rule__Place__Group_7_3__0"
    // InternalNET.g:2645:1: rule__Place__Group_7_3__0 : rule__Place__Group_7_3__0__Impl rule__Place__Group_7_3__1 ;
    public final void rule__Place__Group_7_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2649:1: ( rule__Place__Group_7_3__0__Impl rule__Place__Group_7_3__1 )
            // InternalNET.g:2650:2: rule__Place__Group_7_3__0__Impl rule__Place__Group_7_3__1
            {
            pushFollow(FOLLOW_5);
            rule__Place__Group_7_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Place__Group_7_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7_3__0"


    // $ANTLR start "rule__Place__Group_7_3__0__Impl"
    // InternalNET.g:2657:1: rule__Place__Group_7_3__0__Impl : ( ',' ) ;
    public final void rule__Place__Group_7_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2661:1: ( ( ',' ) )
            // InternalNET.g:2662:1: ( ',' )
            {
            // InternalNET.g:2662:1: ( ',' )
            // InternalNET.g:2663:2: ','
            {
             before(grammarAccess.getPlaceAccess().getCommaKeyword_7_3_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getPlaceAccess().getCommaKeyword_7_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7_3__0__Impl"


    // $ANTLR start "rule__Place__Group_7_3__1"
    // InternalNET.g:2672:1: rule__Place__Group_7_3__1 : rule__Place__Group_7_3__1__Impl ;
    public final void rule__Place__Group_7_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2676:1: ( rule__Place__Group_7_3__1__Impl )
            // InternalNET.g:2677:2: rule__Place__Group_7_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Place__Group_7_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7_3__1"


    // $ANTLR start "rule__Place__Group_7_3__1__Impl"
    // InternalNET.g:2683:1: rule__Place__Group_7_3__1__Impl : ( ( rule__Place__ArcptAssignment_7_3_1 ) ) ;
    public final void rule__Place__Group_7_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2687:1: ( ( ( rule__Place__ArcptAssignment_7_3_1 ) ) )
            // InternalNET.g:2688:1: ( ( rule__Place__ArcptAssignment_7_3_1 ) )
            {
            // InternalNET.g:2688:1: ( ( rule__Place__ArcptAssignment_7_3_1 ) )
            // InternalNET.g:2689:2: ( rule__Place__ArcptAssignment_7_3_1 )
            {
             before(grammarAccess.getPlaceAccess().getArcptAssignment_7_3_1()); 
            // InternalNET.g:2690:2: ( rule__Place__ArcptAssignment_7_3_1 )
            // InternalNET.g:2690:3: rule__Place__ArcptAssignment_7_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Place__ArcptAssignment_7_3_1();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getArcptAssignment_7_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group_7_3__1__Impl"


    // $ANTLR start "rule__Reseau__NomAssignment_3"
    // InternalNET.g:2699:1: rule__Reseau__NomAssignment_3 : ( ruleEString ) ;
    public final void rule__Reseau__NomAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2703:1: ( ( ruleEString ) )
            // InternalNET.g:2704:2: ( ruleEString )
            {
            // InternalNET.g:2704:2: ( ruleEString )
            // InternalNET.g:2705:3: ruleEString
            {
             before(grammarAccess.getReseauAccess().getNomEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReseauAccess().getNomEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__NomAssignment_3"


    // $ANTLR start "rule__Reseau__ArcAssignment_4_2"
    // InternalNET.g:2714:1: rule__Reseau__ArcAssignment_4_2 : ( ruleArc ) ;
    public final void rule__Reseau__ArcAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2718:1: ( ( ruleArc ) )
            // InternalNET.g:2719:2: ( ruleArc )
            {
            // InternalNET.g:2719:2: ( ruleArc )
            // InternalNET.g:2720:3: ruleArc
            {
             before(grammarAccess.getReseauAccess().getArcArcParserRuleCall_4_2_0()); 
            pushFollow(FOLLOW_2);
            ruleArc();

            state._fsp--;

             after(grammarAccess.getReseauAccess().getArcArcParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__ArcAssignment_4_2"


    // $ANTLR start "rule__Reseau__ArcAssignment_4_3_1"
    // InternalNET.g:2729:1: rule__Reseau__ArcAssignment_4_3_1 : ( ruleArc ) ;
    public final void rule__Reseau__ArcAssignment_4_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2733:1: ( ( ruleArc ) )
            // InternalNET.g:2734:2: ( ruleArc )
            {
            // InternalNET.g:2734:2: ( ruleArc )
            // InternalNET.g:2735:3: ruleArc
            {
             before(grammarAccess.getReseauAccess().getArcArcParserRuleCall_4_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleArc();

            state._fsp--;

             after(grammarAccess.getReseauAccess().getArcArcParserRuleCall_4_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__ArcAssignment_4_3_1"


    // $ANTLR start "rule__Reseau__ElementpetriAssignment_7"
    // InternalNET.g:2744:1: rule__Reseau__ElementpetriAssignment_7 : ( ruleElementPetri ) ;
    public final void rule__Reseau__ElementpetriAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2748:1: ( ( ruleElementPetri ) )
            // InternalNET.g:2749:2: ( ruleElementPetri )
            {
            // InternalNET.g:2749:2: ( ruleElementPetri )
            // InternalNET.g:2750:3: ruleElementPetri
            {
             before(grammarAccess.getReseauAccess().getElementpetriElementPetriParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleElementPetri();

            state._fsp--;

             after(grammarAccess.getReseauAccess().getElementpetriElementPetriParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__ElementpetriAssignment_7"


    // $ANTLR start "rule__Reseau__ElementpetriAssignment_8_1"
    // InternalNET.g:2759:1: rule__Reseau__ElementpetriAssignment_8_1 : ( ruleElementPetri ) ;
    public final void rule__Reseau__ElementpetriAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2763:1: ( ( ruleElementPetri ) )
            // InternalNET.g:2764:2: ( ruleElementPetri )
            {
            // InternalNET.g:2764:2: ( ruleElementPetri )
            // InternalNET.g:2765:3: ruleElementPetri
            {
             before(grammarAccess.getReseauAccess().getElementpetriElementPetriParserRuleCall_8_1_0()); 
            pushFollow(FOLLOW_2);
            ruleElementPetri();

            state._fsp--;

             after(grammarAccess.getReseauAccess().getElementpetriElementPetriParserRuleCall_8_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reseau__ElementpetriAssignment_8_1"


    // $ANTLR start "rule__ArcPT__PoidsAssignment_3"
    // InternalNET.g:2774:1: rule__ArcPT__PoidsAssignment_3 : ( ruleEInt ) ;
    public final void rule__ArcPT__PoidsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2778:1: ( ( ruleEInt ) )
            // InternalNET.g:2779:2: ( ruleEInt )
            {
            // InternalNET.g:2779:2: ( ruleEInt )
            // InternalNET.g:2780:3: ruleEInt
            {
             before(grammarAccess.getArcPTAccess().getPoidsEIntParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getArcPTAccess().getPoidsEIntParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__PoidsAssignment_3"


    // $ANTLR start "rule__ArcPT__TypeAssignment_5"
    // InternalNET.g:2789:1: rule__ArcPT__TypeAssignment_5 : ( ruleArcKind ) ;
    public final void rule__ArcPT__TypeAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2793:1: ( ( ruleArcKind ) )
            // InternalNET.g:2794:2: ( ruleArcKind )
            {
            // InternalNET.g:2794:2: ( ruleArcKind )
            // InternalNET.g:2795:3: ruleArcKind
            {
             before(grammarAccess.getArcPTAccess().getTypeArcKindEnumRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleArcKind();

            state._fsp--;

             after(grammarAccess.getArcPTAccess().getTypeArcKindEnumRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__TypeAssignment_5"


    // $ANTLR start "rule__ArcPT__TransitionAssignment_7"
    // InternalNET.g:2804:1: rule__ArcPT__TransitionAssignment_7 : ( ( ruleEString ) ) ;
    public final void rule__ArcPT__TransitionAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2808:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:2809:2: ( ( ruleEString ) )
            {
            // InternalNET.g:2809:2: ( ( ruleEString ) )
            // InternalNET.g:2810:3: ( ruleEString )
            {
             before(grammarAccess.getArcPTAccess().getTransitionTransitionCrossReference_7_0()); 
            // InternalNET.g:2811:3: ( ruleEString )
            // InternalNET.g:2812:4: ruleEString
            {
             before(grammarAccess.getArcPTAccess().getTransitionTransitionEStringParserRuleCall_7_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getArcPTAccess().getTransitionTransitionEStringParserRuleCall_7_0_1()); 

            }

             after(grammarAccess.getArcPTAccess().getTransitionTransitionCrossReference_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__TransitionAssignment_7"


    // $ANTLR start "rule__ArcPT__PlaceAssignment_9"
    // InternalNET.g:2823:1: rule__ArcPT__PlaceAssignment_9 : ( ( ruleEString ) ) ;
    public final void rule__ArcPT__PlaceAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2827:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:2828:2: ( ( ruleEString ) )
            {
            // InternalNET.g:2828:2: ( ( ruleEString ) )
            // InternalNET.g:2829:3: ( ruleEString )
            {
             before(grammarAccess.getArcPTAccess().getPlacePlaceCrossReference_9_0()); 
            // InternalNET.g:2830:3: ( ruleEString )
            // InternalNET.g:2831:4: ruleEString
            {
             before(grammarAccess.getArcPTAccess().getPlacePlaceEStringParserRuleCall_9_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getArcPTAccess().getPlacePlaceEStringParserRuleCall_9_0_1()); 

            }

             after(grammarAccess.getArcPTAccess().getPlacePlaceCrossReference_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcPT__PlaceAssignment_9"


    // $ANTLR start "rule__ArcTP__PoidsAssignment_3"
    // InternalNET.g:2842:1: rule__ArcTP__PoidsAssignment_3 : ( ruleEInt ) ;
    public final void rule__ArcTP__PoidsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2846:1: ( ( ruleEInt ) )
            // InternalNET.g:2847:2: ( ruleEInt )
            {
            // InternalNET.g:2847:2: ( ruleEInt )
            // InternalNET.g:2848:3: ruleEInt
            {
             before(grammarAccess.getArcTPAccess().getPoidsEIntParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getArcTPAccess().getPoidsEIntParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__PoidsAssignment_3"


    // $ANTLR start "rule__ArcTP__TransitionAssignment_5"
    // InternalNET.g:2857:1: rule__ArcTP__TransitionAssignment_5 : ( ( ruleEString ) ) ;
    public final void rule__ArcTP__TransitionAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2861:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:2862:2: ( ( ruleEString ) )
            {
            // InternalNET.g:2862:2: ( ( ruleEString ) )
            // InternalNET.g:2863:3: ( ruleEString )
            {
             before(grammarAccess.getArcTPAccess().getTransitionTransitionCrossReference_5_0()); 
            // InternalNET.g:2864:3: ( ruleEString )
            // InternalNET.g:2865:4: ruleEString
            {
             before(grammarAccess.getArcTPAccess().getTransitionTransitionEStringParserRuleCall_5_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getArcTPAccess().getTransitionTransitionEStringParserRuleCall_5_0_1()); 

            }

             after(grammarAccess.getArcTPAccess().getTransitionTransitionCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__TransitionAssignment_5"


    // $ANTLR start "rule__ArcTP__PlaceAssignment_7"
    // InternalNET.g:2876:1: rule__ArcTP__PlaceAssignment_7 : ( ( ruleEString ) ) ;
    public final void rule__ArcTP__PlaceAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2880:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:2881:2: ( ( ruleEString ) )
            {
            // InternalNET.g:2881:2: ( ( ruleEString ) )
            // InternalNET.g:2882:3: ( ruleEString )
            {
             before(grammarAccess.getArcTPAccess().getPlacePlaceCrossReference_7_0()); 
            // InternalNET.g:2883:3: ( ruleEString )
            // InternalNET.g:2884:4: ruleEString
            {
             before(grammarAccess.getArcTPAccess().getPlacePlaceEStringParserRuleCall_7_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getArcTPAccess().getPlacePlaceEStringParserRuleCall_7_0_1()); 

            }

             after(grammarAccess.getArcTPAccess().getPlacePlaceCrossReference_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArcTP__PlaceAssignment_7"


    // $ANTLR start "rule__Transition__NomAssignment_3"
    // InternalNET.g:2895:1: rule__Transition__NomAssignment_3 : ( ruleEString ) ;
    public final void rule__Transition__NomAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2899:1: ( ( ruleEString ) )
            // InternalNET.g:2900:2: ( ruleEString )
            {
            // InternalNET.g:2900:2: ( ruleEString )
            // InternalNET.g:2901:3: ruleEString
            {
             before(grammarAccess.getTransitionAccess().getNomEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTransitionAccess().getNomEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__NomAssignment_3"


    // $ANTLR start "rule__Transition__ArctpAssignment_4_2"
    // InternalNET.g:2910:1: rule__Transition__ArctpAssignment_4_2 : ( ( ruleEString ) ) ;
    public final void rule__Transition__ArctpAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2914:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:2915:2: ( ( ruleEString ) )
            {
            // InternalNET.g:2915:2: ( ( ruleEString ) )
            // InternalNET.g:2916:3: ( ruleEString )
            {
             before(grammarAccess.getTransitionAccess().getArctpArcTPCrossReference_4_2_0()); 
            // InternalNET.g:2917:3: ( ruleEString )
            // InternalNET.g:2918:4: ruleEString
            {
             before(grammarAccess.getTransitionAccess().getArctpArcTPEStringParserRuleCall_4_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTransitionAccess().getArctpArcTPEStringParserRuleCall_4_2_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getArctpArcTPCrossReference_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__ArctpAssignment_4_2"


    // $ANTLR start "rule__Transition__ArctpAssignment_4_3_1"
    // InternalNET.g:2929:1: rule__Transition__ArctpAssignment_4_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Transition__ArctpAssignment_4_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2933:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:2934:2: ( ( ruleEString ) )
            {
            // InternalNET.g:2934:2: ( ( ruleEString ) )
            // InternalNET.g:2935:3: ( ruleEString )
            {
             before(grammarAccess.getTransitionAccess().getArctpArcTPCrossReference_4_3_1_0()); 
            // InternalNET.g:2936:3: ( ruleEString )
            // InternalNET.g:2937:4: ruleEString
            {
             before(grammarAccess.getTransitionAccess().getArctpArcTPEStringParserRuleCall_4_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTransitionAccess().getArctpArcTPEStringParserRuleCall_4_3_1_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getArctpArcTPCrossReference_4_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__ArctpAssignment_4_3_1"


    // $ANTLR start "rule__Transition__ArcptAssignment_5_2"
    // InternalNET.g:2948:1: rule__Transition__ArcptAssignment_5_2 : ( ( ruleEString ) ) ;
    public final void rule__Transition__ArcptAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2952:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:2953:2: ( ( ruleEString ) )
            {
            // InternalNET.g:2953:2: ( ( ruleEString ) )
            // InternalNET.g:2954:3: ( ruleEString )
            {
             before(grammarAccess.getTransitionAccess().getArcptArcPTCrossReference_5_2_0()); 
            // InternalNET.g:2955:3: ( ruleEString )
            // InternalNET.g:2956:4: ruleEString
            {
             before(grammarAccess.getTransitionAccess().getArcptArcPTEStringParserRuleCall_5_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTransitionAccess().getArcptArcPTEStringParserRuleCall_5_2_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getArcptArcPTCrossReference_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__ArcptAssignment_5_2"


    // $ANTLR start "rule__Transition__ArcptAssignment_5_3_1"
    // InternalNET.g:2967:1: rule__Transition__ArcptAssignment_5_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Transition__ArcptAssignment_5_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2971:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:2972:2: ( ( ruleEString ) )
            {
            // InternalNET.g:2972:2: ( ( ruleEString ) )
            // InternalNET.g:2973:3: ( ruleEString )
            {
             before(grammarAccess.getTransitionAccess().getArcptArcPTCrossReference_5_3_1_0()); 
            // InternalNET.g:2974:3: ( ruleEString )
            // InternalNET.g:2975:4: ruleEString
            {
             before(grammarAccess.getTransitionAccess().getArcptArcPTEStringParserRuleCall_5_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTransitionAccess().getArcptArcPTEStringParserRuleCall_5_3_1_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getArcptArcPTCrossReference_5_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__ArcptAssignment_5_3_1"


    // $ANTLR start "rule__Place__NomAssignment_3"
    // InternalNET.g:2986:1: rule__Place__NomAssignment_3 : ( ruleEString ) ;
    public final void rule__Place__NomAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:2990:1: ( ( ruleEString ) )
            // InternalNET.g:2991:2: ( ruleEString )
            {
            // InternalNET.g:2991:2: ( ruleEString )
            // InternalNET.g:2992:3: ruleEString
            {
             before(grammarAccess.getPlaceAccess().getNomEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPlaceAccess().getNomEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__NomAssignment_3"


    // $ANTLR start "rule__Place__NbJetonsAssignment_5"
    // InternalNET.g:3001:1: rule__Place__NbJetonsAssignment_5 : ( ruleEInt ) ;
    public final void rule__Place__NbJetonsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:3005:1: ( ( ruleEInt ) )
            // InternalNET.g:3006:2: ( ruleEInt )
            {
            // InternalNET.g:3006:2: ( ruleEInt )
            // InternalNET.g:3007:3: ruleEInt
            {
             before(grammarAccess.getPlaceAccess().getNbJetonsEIntParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getPlaceAccess().getNbJetonsEIntParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__NbJetonsAssignment_5"


    // $ANTLR start "rule__Place__ArctpAssignment_6_2"
    // InternalNET.g:3016:1: rule__Place__ArctpAssignment_6_2 : ( ( ruleEString ) ) ;
    public final void rule__Place__ArctpAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:3020:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:3021:2: ( ( ruleEString ) )
            {
            // InternalNET.g:3021:2: ( ( ruleEString ) )
            // InternalNET.g:3022:3: ( ruleEString )
            {
             before(grammarAccess.getPlaceAccess().getArctpArcTPCrossReference_6_2_0()); 
            // InternalNET.g:3023:3: ( ruleEString )
            // InternalNET.g:3024:4: ruleEString
            {
             before(grammarAccess.getPlaceAccess().getArctpArcTPEStringParserRuleCall_6_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPlaceAccess().getArctpArcTPEStringParserRuleCall_6_2_0_1()); 

            }

             after(grammarAccess.getPlaceAccess().getArctpArcTPCrossReference_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__ArctpAssignment_6_2"


    // $ANTLR start "rule__Place__ArctpAssignment_6_3_1"
    // InternalNET.g:3035:1: rule__Place__ArctpAssignment_6_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Place__ArctpAssignment_6_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:3039:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:3040:2: ( ( ruleEString ) )
            {
            // InternalNET.g:3040:2: ( ( ruleEString ) )
            // InternalNET.g:3041:3: ( ruleEString )
            {
             before(grammarAccess.getPlaceAccess().getArctpArcTPCrossReference_6_3_1_0()); 
            // InternalNET.g:3042:3: ( ruleEString )
            // InternalNET.g:3043:4: ruleEString
            {
             before(grammarAccess.getPlaceAccess().getArctpArcTPEStringParserRuleCall_6_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPlaceAccess().getArctpArcTPEStringParserRuleCall_6_3_1_0_1()); 

            }

             after(grammarAccess.getPlaceAccess().getArctpArcTPCrossReference_6_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__ArctpAssignment_6_3_1"


    // $ANTLR start "rule__Place__ArcptAssignment_7_2"
    // InternalNET.g:3054:1: rule__Place__ArcptAssignment_7_2 : ( ( ruleEString ) ) ;
    public final void rule__Place__ArcptAssignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:3058:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:3059:2: ( ( ruleEString ) )
            {
            // InternalNET.g:3059:2: ( ( ruleEString ) )
            // InternalNET.g:3060:3: ( ruleEString )
            {
             before(grammarAccess.getPlaceAccess().getArcptArcPTCrossReference_7_2_0()); 
            // InternalNET.g:3061:3: ( ruleEString )
            // InternalNET.g:3062:4: ruleEString
            {
             before(grammarAccess.getPlaceAccess().getArcptArcPTEStringParserRuleCall_7_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPlaceAccess().getArcptArcPTEStringParserRuleCall_7_2_0_1()); 

            }

             after(grammarAccess.getPlaceAccess().getArcptArcPTCrossReference_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__ArcptAssignment_7_2"


    // $ANTLR start "rule__Place__ArcptAssignment_7_3_1"
    // InternalNET.g:3073:1: rule__Place__ArcptAssignment_7_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Place__ArcptAssignment_7_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalNET.g:3077:1: ( ( ( ruleEString ) ) )
            // InternalNET.g:3078:2: ( ( ruleEString ) )
            {
            // InternalNET.g:3078:2: ( ( ruleEString ) )
            // InternalNET.g:3079:3: ( ruleEString )
            {
             before(grammarAccess.getPlaceAccess().getArcptArcPTCrossReference_7_3_1_0()); 
            // InternalNET.g:3080:3: ( ruleEString )
            // InternalNET.g:3081:4: ruleEString
            {
             before(grammarAccess.getPlaceAccess().getArcptArcPTEStringParserRuleCall_7_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPlaceAccess().getArcptArcPTEStringParserRuleCall_7_3_1_0_1()); 

            }

             after(grammarAccess.getPlaceAccess().getArcptArcPTCrossReference_7_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__ArcptAssignment_7_3_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000050000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000108000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x00000000000A0000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000004200000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000100040L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000090020000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000040080000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000200000000L});

}